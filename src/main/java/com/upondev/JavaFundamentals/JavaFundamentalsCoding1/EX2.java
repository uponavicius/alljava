package com.upondev.JavaFundamentals.JavaFundamentalsCoding1;

import java.text.DecimalFormat;

public class EX2 {
    public static void main(String[] args) {
        //Enter any value with several digits after the decimal point and assign it to variable of type double. Display the given value rounded to two decimal places.
        double digit = 12.1112212122;
        double digit2 = 13.555555;

        DecimalFormat df = new DecimalFormat("#.##");
        System.out.println(df.format(digit));
        System.out.println(df.format(digit2));

    }
}
