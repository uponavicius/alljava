package com.upondev.JavaFundamentals.JavaFundamentalsCoding1;

import java.text.DecimalFormat;


public class EX4 {
    public static void main(String[] args) {
        //Enter two values of type int. Display their division casted to the double type and rounded to the third decimal point.
        int v1 = 13;
        int v2 = 9;

        //Cast integer  to double
        double rez = (double) v1/v2;
        double rez2 = v1/v2;

        //Construct to double using cast
        Double rslt = new Double((double) v2 / v1);

        DecimalFormat df = new DecimalFormat("#.##");
        System.out.println(rez);
        System.out.println(df.format(rez));
        System.out.println(rez2);


    }
}
