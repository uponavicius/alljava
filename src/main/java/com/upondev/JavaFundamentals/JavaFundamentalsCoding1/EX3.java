package com.upondev.JavaFundamentals.JavaFundamentalsCoding1;

public class EX3 {
    public static void main(String[] args) {
        //Display any three strings of characters on one line so that they are aligned to the right edge of the 15-character blocks. How to align strings to the left edge?
        //Sauliaus PVZ pasinagrineti
        String string = "My name is Vladas";
        for (int i = 0; i < string.length(); i = i + 3) {
            if (string.length() - i - 3 < 0) {
                System.out.printf("%12s", string.substring(i));
            } else {
                System.out.printf("%12s", string.substring(i, i + 3));
            }
        }
    }
}
