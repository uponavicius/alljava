package com.upondev.JavaFundamentals.JavaFundamentalsCoding1;

import java.util.Scanner;

public class EX6 {
    public static void main(String[] args) {
        //Create three variables, one for each type: float, byte and char. Enter values corresponding to
        //those types using Scanner. What values are you able to enter for each type?
        Scanner reader = new Scanner(System.in);
        System.out.println("Iveskite raide");
        char oneChar = reader.next().charAt(0);
        System.out.println("raide: " + oneChar);

        System.out.println("iveskite byte");
        byte oneByte = reader.nextByte();
        System.out.println("Byte: " + oneByte);

        System.out.println("iveskite float");
        float oneFloat = reader.nextFloat();
        System.out.println("Float: " + oneFloat);






    }
}
