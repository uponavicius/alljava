package com.upondev.JavaFundamentals.JavaFundamentalsCoding1;

public class EX1 {
    public static void main(String[] args) {
        //1. Use System.out.print method to print the same statement in separate lines. Example:
        //Hello, World!
        //Hello, World!


        System.out.println("2 Hello, World!\n2 Hello, World!");

        //3
        for (int i=0; i<2; i++){
            System.out.println("3 Hello, World!");
        }

        //4
        int i=0;
        do {
            System.out.println("4 Hello, World!");
            i++;
        } while (i<=1);

        //5
        i = 0;
        while (i<=1){
            System.out.println("5 Hello, World!");
            i++;
        }



    }
}
