package com.upondev.JavaFundamentals.Bank;

import java.util.Scanner;


public class ActionsWithBankAccount {
    public static void main(String[] args) {
        Scanner actionIn = new Scanner(System.in);
        String operation = "";
        BankAccount actiontsWithBankAccunt = new BankAccount("Vladas Ups",
                "LT00 0000 0000 0000 0000", 1000);

        while (!operation.equals("exit")) {
            System.out.println("Įnešti pinigus, rašykite:       +");
            System.out.println("Pinigų išėmimas, rašykite:      -");
            System.out.println("Pamatyti balansą, rašykite:     *");
            System.out.println("Sąskaitos numeris, rašykite:    s");
            System.out.println("Išeiti iš banko, rašykite:      exit");
            operation = actionIn.next(); //imti ne netxln, o next.
            if (operation.equals("exit")) {
                continue; //jei ivede exit, tai kad nebetesti toliau patikros.
            }



            if (operation.equals("*")){
                System.out.println("Jūsų sąskaitos balansas");
                System.out.println(actiontsWithBankAccunt.getAccountBalance());
            } else if (operation.equals("s")){
                System.out.println("Jūsų sąskaita");
                System.out.println(actiontsWithBankAccunt.getBankAccountNumber());
            } else if (operation.equals("+")){
                System.out.println("Įveskite pingų sumą kurią norite įdėti į sąskaita");
                double valuePlus = actionIn.nextDouble();
                actiontsWithBankAccunt.moneyPlus(valuePlus);
            } else if (operation.equals("-")){
                System.out.println("Įveskite pinigų sumą kurią norite išimti iš sąskaitos");
                double valueMinus = actionIn.nextDouble();
                    if (valueMinus>actiontsWithBankAccunt.getAccountBalance()){
                        System.out.println("Jūsų pageidaujama suma per didelė");
                    } else if (valueMinus<=actiontsWithBankAccunt.getAccountBalance()){
                        actiontsWithBankAccunt.moneyMinus(valueMinus);
                    }
            }


        }
        System.out.println("Atsijunėte nuo banko");
        System.out.println("Jei skaičiai nedžiugina, eik dirbti :D");
        System.out.println("Geros dienos!");
    }
}
