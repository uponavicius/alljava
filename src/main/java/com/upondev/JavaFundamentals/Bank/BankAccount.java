package com.upondev.JavaFundamentals.Bank;

public class BankAccount {
    protected static String accountHolderName;
    protected static String bankAccountNumber;
    private double accountBalance;


    protected BankAccount(String accountHolderName, String bankAccount, double accountBalance) {
        this.accountHolderName = accountHolderName;
        this.bankAccountNumber = bankAccount;
        this.accountBalance = accountBalance;
    }

    //----METODAI----

    protected double moneyPlus(double plus) {
        this.accountBalance = this.accountBalance + plus;
        System.out.println("Į sąskaitą įdėjote: " + plus + " €");
        System.out.println("Jūsų sąskaitoje: " + this.accountBalance + " €");
        return this.accountBalance;
    }

    protected double moneyMinus(double minus) {
        this.accountBalance = this.accountBalance - minus;
        System.out.println("Iš sąskaitos išėmete : " + minus + " €");
        System.out.println("Jūsų sąskaitoje liko: " + this.accountBalance + " €");
        return this.accountBalance;
    }


    public double getAccountBalance() {
        return this.accountBalance;
    }

    public String getAccountHolderName(){
        return this.accountHolderName;
    }

    public String getBankAccountNumber(){
        return this.bankAccountNumber;
    }
}