package com.upondev.JavaFundamentals.javaFundDay3;

public class NewClassExample {
    public int publicIntVladas = 0; //matis visi
    int defaultIntVladas = 0; //Leidzia matyti pakete ir visoms kitoms klases kurios neturi kitu (public, protected ar private)
    protected int protectedIntVladas = 0; //leisime naudoti tik tiems kurie yra pakete ir tam kam leidziame.
    private  int privateIntVladas = 0; //Tik tas matis, kad yra klaseje. net ir kalse klaseje nematis

}
