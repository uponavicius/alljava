package com.upondev.JavaFundamentals.javaFundDay3;

public class Namas {
    public double kambariai;
    public int aukstai;

    public Namas() {} //defaultinis konstruktorius,
                    // jei mes patys neparasome konstruktoriaus

    public Namas(int auk, int kam){
        aukstai = auk;
        kambariai = kam;

    }
    public Namas(int auk){
        aukstai = auk;
    }
    public Namas(double kam){
        kambariai = kam;
    }

}
