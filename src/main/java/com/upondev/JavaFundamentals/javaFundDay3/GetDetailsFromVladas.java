package com.upondev.JavaFundamentals.javaFundDay3;

public class GetDetailsFromVladas {
    public static void main(String[] args) { //metodas, jis visada bus
        Vladas vladas = new Vladas();
        vladas.koksManoVardasTikSpausdinti();
        String vardas = vladas.koksManoVardasGrazinti();
        String pataisytasVardas = vardas.replace('v', 'V');
        vladas.name = pataisytasVardas;
        vladas.koksManoVardasTikSpausdinti();

        System.out.println("Man " + vladas.age + " metai");


    }
}
