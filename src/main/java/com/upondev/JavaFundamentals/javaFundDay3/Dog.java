package com.upondev.JavaFundamentals.javaFundDay3;
//1.	Create a class called „dog"
//a.	Create a declaration for all required fields, like “name”, age ”,...
//b.	Create default constructor that will be able to set instantiate those values
//c.	Create a method, that will print default sound that dog can emit
//d.	*Sound should depend on the dogs age
//e.	Print that sound to the console. Change dogs age, print it once more

public class Dog {
    public String dogName;
    public int dogAge;

    public Dog (String name, int age) {
        dogName = name;
        dogAge = age;
    }

    public void sound (){
        System.out.println("au au au");
    }

    public void soundOnAge (){
        if ( dogAge== 1){
            System.out.println("au");
        } else if (dogAge==2){
            System.out.println("au au");
        }else if (dogAge==3){
            System.out.println("au au au");
        }else if (dogAge==4){
            System.out.println("au au au au");
        }
    }





}
