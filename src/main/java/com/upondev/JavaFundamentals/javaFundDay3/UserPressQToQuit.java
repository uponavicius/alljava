package com.upondev.JavaFundamentals.javaFundDay3;

import java.util.Scanner;

public class UserPressQToQuit {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String userEnter = "";
        while (!userEnter.equals("q")){
            userEnter = scanner.nextLine();
        }
        System.out.println("Exit");

    }
}
