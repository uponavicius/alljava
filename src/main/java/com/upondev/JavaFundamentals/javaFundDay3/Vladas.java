package com.upondev.JavaFundamentals.javaFundDay3;

public class Vladas {
    public String  name = "vladas";
    public int age = 34;

    public Vladas () {}

    public void koksManoVardasTikSpausdinti() {
        System.out.println("Mano vardas " + name);
    }
    public String koksManoVardasGrazinti(){
        return name;
    }

    private int kiekManMetu(){
        return age;
    }
}
