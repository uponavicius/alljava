package com.upondev.JavaFundamentals.javaFundDay4;

public class AboutRoom {
    public static void main(String[] args) {
        Room diningRoom = new Room(10, 10, 10); //is klases Room padarom new Room objekta, kuris seka konstruktoriu
        diningRoom.surface();
        int volume = diningRoom.volume();
        System.out.println();
        System.out.println("Volume is " + volume);

    }
}
