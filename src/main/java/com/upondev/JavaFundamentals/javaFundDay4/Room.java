package com.upondev.JavaFundamentals.javaFundDay4;

// public static int windows;
// jei nori pasiekti static kintakaji is kitos klases, tai nereikai sukurti naujo objekto.
public class Room {
    public int lenght; //instance klases kintamasis
    public int width;
    public int heigt;
    public static int pvm; //static kintamasis, jis nebepriklauso klasei.
    // naudojamas pvz konfiguracianiams failams nurodyti. ji matys visiskai visi.
    // tikslas kad matyti visi. SOURSE lygiio.
    // zodis final neleidzia keisti reiksmes niekam.

    public Room (int width, int heigt, int lenght){ //konstruktoriuje void nereikia. kintamieji skliaustuose yra vadinami local kintamieji
        this.lenght = lenght;
        this.width = width;
        this.heigt = heigt;

    }

    public void surface (){ //metodas
        int surface = lenght * width; //surface yra bloko kintamasis
        System.out.println("Plotas " + surface);
    }
    public int volume (){ //metodas
        int volume = heigt * lenght * heigt;
        return volume;

    }

    public static void setPVM(String country){
        if (country.equals("LIT")){
            pvm = 21;
        }else if (country.equals("LAT")){
            pvm = 25;
        }
    }


}
