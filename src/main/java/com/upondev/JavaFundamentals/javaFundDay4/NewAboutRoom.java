package com.upondev.JavaFundamentals.javaFundDay4;

public class NewAboutRoom {
    public static void main(String[] args) {
        NewRoom svetaine = new NewRoom();
        svetaine.surface(10, 5);

        int volume = svetaine.volume(10, 5, 10);
        System.out.println("Plotas " + volume);

        Room.setPVM("LIT");
    }
}
