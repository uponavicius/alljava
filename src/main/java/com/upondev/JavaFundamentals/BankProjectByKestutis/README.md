# Bankomato projektas

Užduotis:

Patobulinti bankomto klasę.
1.	Banko sąsaita turi būti kuriama Banko klasėje, tai reiškia teks sukurti naują Bank klasę.
2.	Bank klasė, gali sukurti Visa ir/arba MasterCard, priklausomai nuo klineto pasirinkimo, taigi reikės bent poros metodų Bank klasėje.
3.	Kadangi yra galimybė sukurti dviejų rušių sąskaitas, sąskaita turėtų turėti savo klasę su kintamaisiais, kaip sąsk numeris, kam ji priklauso, balansu ir pin kodu.
a.	Sąskaitos klasė, turėtų turėti set / get balanso metodus.
b.	Sąskaitos klasė, turėtų turėti get pin kodo metodą.
4.	Išėmus/pridėjus iš bankomato pinigų, bakomato klasė turėtų gauti iš sąskaitos klasės balansą (get) jį sumažinti / padidnti ir išsaugoti (set).

Tai yra klasės patobulinimas, tad kas turėjo papildomų metodų, kaip sąskaitos balansas padauginti iš pin kodo, galite viską palikti. Patobulinkite savo projektą, sukuriant naujas klases ir metodus, ir juos panaudokite.

Projekto apibendrinimas:
1.	Klasė, kurioje yra main metodas, turi sukurti naują Bank klasę ir pakviesti jos Visa ir/arba MasterCard metodą, priklausomai nuo vartotojo pasirinkimo.
2.	Bank klasė, pagal pasirinkimą, sukurti atitinkamą sąskaitą.
3.	Main klasė, turi papršyti vartotojo su kokia sąskaitos norima dirbti.
4.	Pagal sąskaitos pasirinkimą, patikrinti tos sąskaitos (getPinCode) pin kodą.
5.	Visi likusieji metodai, iš to ką darėme klasėje, tik pritaikyti pasirinktai sąskaitai (Visa ar MasterCard)
