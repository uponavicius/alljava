package com.upondev.JavaFundamentals.BankProjectByKestutis;

import java.util.Scanner;

public class BankMachine {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("To create new account please enter");
        System.out.println("Name ");
        String accountName = scanner.next();

        System.out.println("IBAN ");
        String iban = scanner.next();

        System.out.println("Starting Balance ");
        double balance = scanner.nextDouble();

        System.out.println("Enter Pin Code to create account");
        int pinCode = scanner.nextInt();

        BankAccount bankAccount = new BankAccount(accountName, iban, balance, pinCode);
        System.out.println("Please wait");
        bankAccount.tryVarAgrs(1 , 2 , 3, 4, 5);
        bankAccount.tryVarAgrs(1 , 2 , 3, 4, 5, 6, 7, 8, 9, 10);


        System.out.println("Current balance " + bankAccount.balance);

        System.out.println("Bank machine: please insert card and enter pin code");
        int userPinCode = scanner.nextInt();

        if (userPinCode != bankAccount.getPinCode())
            return;
        else {
            System.out.println("Please enter amount to add");
            double toAdd = scanner.nextDouble();
            bankAccount.add(toAdd);

            System.out.println("Please enter amount to minus");
            double toMinus = scanner.nextDouble();
            bankAccount.minus(toMinus);

            System.out.println("Balance over all " + bankAccount.balance);
        }

        System.out.println("Would you like to change pin code, enter 'Y' or 'N'");
        String pinCodeChange = scanner.next();
        if(pinCodeChange.equals("Y")) {
            System.out.println("Your old pin is " + bankAccount.getPinCode());
            System.out.println("Enter new pin");
            int newPinCode = scanner.nextInt();
            bankAccount.setPinCode(newPinCode);

            System.out.println("Your new pin code " + bankAccount.getPinCode());
        }
    }
}
