package com.upondev.JavaFundamentals.BankProjectByKestutis;

public class BankAccount {

    public String accountName;
    public String iban;
    public double balance;
    private int pinCode;

    public BankAccount(String accountName, String iban, double balance, int pinCode) {
        this.accountName = accountName;
        this.iban = iban;
        this.balance = balance;
        this.pinCode = pinCode;
    }

    public void add(double plus) {
        this.balance = this.balance + plus;
        System.out.println("New balance " + this.balance);
    }

    public void minus(double minus) {
        this.balance = this.balance - minus;
        System.out.println("New balance " + this.balance);
    }

    public void getBalance() {
        System.out.println("Balance " + this.balance);
    }

    public int getPinCode() {
        return this.pinCode;
    }

    public void setPinCode(int pinCode) {
        this.pinCode = pinCode;
    }

    public void tryVarAgrs(int... manoArgumentai) {
        for(int i = 0; i < manoArgumentai.length; i++) {
            System.out.println(manoArgumentai[i]);
        }
    }

}
