package com.upondev.JavaFundamentals;

import java.util.Scanner;

public class FlowControlEx4 {
    public static void main(String[] args) {
        //Write an application that for any entered number between 0 and 9 will provide it’s name. For
        //example for “3” program should print “three”.

        Scanner infoIn = new Scanner(System.in);
        System.out.println("Enter integer number from 0 to 9");
        int intergerNumber = infoIn.nextInt();

        switch (intergerNumber){
            case 0:
                System.out.println("Zero");
                break;
            case 1:
                System.out.println("One");
                break;
            case 2:
                System.out.println("Two");
                break;
            case 3:
                System.out.println("Tree");
                break;
            case 4:
                System.out.println("Four");
                break;
            case 5:
                System.out.println("Five");
                break;
            case 6:
                System.out.println("Six");
                break;
            case 7:
                System.out.println("Seven");
                break;
            case 8:
                System.out.println("Eight");
                break;
            case 9:
                System.out.println("Nine");
                break;

            default:
                System.out.println("Other");
                break;
        }
    }
}
