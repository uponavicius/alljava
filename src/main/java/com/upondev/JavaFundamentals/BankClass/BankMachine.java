package com.upondev.JavaFundamentals.BankClass;

import java.util.Date;
import java.util.Scanner;

public class BankMachine {
    public static void main(String[] args) {
        // banke susikuri banko accounta
        Scanner scanner = new Scanner(System.in);
        System.out.println("To create new account please enter: ");
        System.out.println("Name");
        String acoountName = scanner.next();
        System.out.println("iban");
        String iban = scanner.next();
        System.out.println("Starting Balance");
        double balance = scanner.nextDouble();

        //stovi prie automato
        System.out.println("Bank Machite: please insert card and enter pin");
        int pinCoce = scanner.nextInt();

        BankAccount bankAccount = new BankAccount(acoountName, iban, balance, pinCoce);
        //bankAccount referencas i atminti

        System.out.println("Please wait");
        bankAccount.tryVarArgs("* ", "* ","* ","* ","* ","* ","* ","* ","* ","* ","* ","* ","* ");
        System.out.println("");
        System.out.println("Current Balance: " + bankAccount.balance);
        bankAccount.timeStampInfo();


        System.out.println("Please enter pin code");
        int userPinCode = scanner.nextInt();

        if (userPinCode!=bankAccount.getPinCode()){
            return;
        } else {
            System.out.println("Plase enter amount to add");
            double toAdd = scanner.nextDouble();
            bankAccount.add(toAdd);

            System.out.println("Please enter amount to minus");
            double toMinus = scanner.nextDouble();
            bankAccount.minus(toMinus);
        }

        System.out.println("Would you like to change pin code, enter 'Y' or 'N' ");
        String pinCodeChange = scanner.next();

        if (pinCodeChange.equals("Y")){
            System.out.println("Your old pin is: " + bankAccount.getPinCode());
            System.out.println("Enter new pin");
            int newPinCode = scanner.nextInt();
            bankAccount.setPinCode(newPinCode);

            System.out.println("Your new pin code: " + bankAccount.getPinCode());

            bankAccount.magicBalance();
            Date now = new Date();

        }

    }
}
