package com.upondev.JavaFundamentals.BankClass;



import java.sql.Timestamp;
import java.util.Date;

public class BankAccount {
    public String accountName;
    public String iban;
    public double balance;
    private int pinCode;

    Date date = new Date();

    public  BankAccount (String accountName, String iban, double balance, int pin) {  //konstruktorius
        this.accountName = accountName;
        this.iban = iban;
        this.balance = balance;
        this.pinCode = pin;


    // -------- metodai----------
    }
    public void add(double plus) {
        this.balance = this.balance + plus;
        System.out.println("New balance: " + this.balance);
    }
    public void minus (double minus) {
        this.balance = this.balance - minus;
        System.out.println("New balance: " + this.balance);
    }
    public void getBalance () {
        System.out.println("Balance: " + this.balance);
    }

    public int getPinCode(){
        return this.pinCode; //gauna is atminties
    }

    public void setPinCode(int pinCode) {
        this.pinCode = pinCode; //iraso i atminti
    }

    public void magicBalance (){
        this.balance = this.balance * this.pinCode;
        System.out.println("You new magic balance: " + this.balance);
    }
    public void timeStampInfo(){
        long time = date.getTime();
        Timestamp timestamp = new Timestamp(time);
        System.out.println(timestamp);
    }

    public void tryVarArgs (String... manoArgumentai){
        for (int i = 0; i < manoArgumentai.length; i++){
            System.out.print(manoArgumentai[i]);

        }

    }
}