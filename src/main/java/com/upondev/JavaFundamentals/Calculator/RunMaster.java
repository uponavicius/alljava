/*
Sukurti kalkuliatoriaus klasę keturioms operacijoms atlikti (+ - * /)
Sukurti „main“ klasę kurioje:
Aprašymas: programa turi išeiti (baigti darbą), jei vartotojas parašė žodį "exit" arba simbolį 'q'
1. Programa pirmiausia turi paprašyti kokios operacijos vartotojas nori ( + - * /)
2. Po operacijos nuskaitymo programa turi paprašyti vartotojo įvesti pirma kintamajį
3. Po pirmo kintamojo nuskaitymo programa turi paprašyti vartotojo įvesti antrą kintamajį
4. Po antro kintamojo nuskaitymo programa turi išvesti rezultatą (kreiptis į kalkuliatoriuas klasę) ir vėl paprašyti kokios operacijos nori vartotojas.
5. jeigu vartotojas vietoje operacijos parašė "exit" arba 'q' -> programa turi baigti darbą
 */

// metodai paimti is calc klases.
package com.upondev.JavaFundamentals.Calculator;

import java.util.Scanner;

public class RunMaster {
    public static void main(String[] args) {

        System.out.println("Skaiciavimo masinele v1.1");
        Scanner veiksmasIn = new Scanner(System.in);
        String veiksmas = "";



        while (!veiksmas.equals("exit")) {
            System.out.println("Iveskite koki veiksma norite atlikti: +, -, *, / arba sin. Rasykite exit, jei norite iseiti");
            veiksmas = veiksmasIn.nextLine();
            if (veiksmas.equals("exit")){
                continue; //arba break
            }

            Scanner skaicius1In = new Scanner(System.in);
            System.out.println("Iveskite pirma skaiciu: ");
            double skaicius1 = skaicius1In.nextDouble();

            Scanner skaicius2In = new Scanner(System.in);
            System.out.println("Iveskite antra skaiciu: ");
            double skaicius2  = skaicius2In.nextDouble();

            Calc naujasCalc = new Calc(skaicius1, skaicius2); //nes noriu perpanaudoti Calc klase, todel kuriamas naujas objektas

            if (veiksmas.equals("+")) {
                double suma = naujasCalc.sudetiesVeiksmas();
                System.out.println("Suma: " + suma);
            } else if (veiksmas.equals("-")) {
                double atimtis = naujasCalc.atimtiesVeiksmas();
                System.out.println("Atimtis: " + atimtis);
            } else if (veiksmas.equals("*")) {
                double daugyva = naujasCalc.daugybosVeiksmas();
                System.out.println("Daugyba: " + daugyva);
            } else if (veiksmas.equals("/")) {
                double dalyba = naujasCalc.dalybosVeiksmas();
                System.out.println("Dalyba: " + dalyba);
            }
        }
        System.out.println("Exit");

    }
}
