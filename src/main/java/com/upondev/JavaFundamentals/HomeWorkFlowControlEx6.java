package com.upondev.JavaFundamentals;

public class HomeWorkFlowControlEx6 {
    public static void main(String[] args) {
        //Write a simple application that will simulate a shopping. Use only if-else flow control.
        //Consider following cases:
        //a. If you would like to buy a bottle of milk – cashier will ask you for a specific amount of
        //money. You have to enter that value and verify if it is same as the cashier asked.
        //b. If you would like to buy a bottle of wine – cashier will ask you if you are an adult and
        //for positive answer ask for a specific amount of money.
    }
}
