package com.upondev.JavaFundamentals.javaFundDay1;

public class StringsEX1 {
    public static void main(String[] args) {
        String textLine = "Lorem ipsum dolor sit amet, consectetur adipiscing elit";
        String textLower = textLine.toLowerCase();
        System.out.println(textLine);

        String textUpper = textLine.toUpperCase();
        System.out.println(textUpper);

        String textReplace = textLine.replace("o", "z");
        System.out.println(textReplace);

        Boolean textEnd = textLine.endsWith("elit");
        System.out.println(textEnd);


        /*int indexOfComa = textLine.indexOf(",");
        String phase1 = textLine.substring(0, indexOfComa);
        int indexOfSpace = phase1.lastIndexOf(" ");
        String wordSearch = phase1.substring(indexOfSpace+1);
        System.out.println(wordSearch);
        */

        //OPTIMIZUOTAS KODAS
        String phase1 = textLine.substring(0, textLine.indexOf(","));
        System.out.println(phase1.substring(phase1.lastIndexOf(" ")+1));


        /*

        Kestutis Kamantauskas 3:38 PM
        String phrase = "Lorem ipsum dolor sit amet, consectetur adipiscing elit";
       int indexOFComa = phrase.indexOf(',');
       System.out.println("index " + indexOFComa);
       String phrase1 = phrase.substring(0, indexOFComa);
       System.out.println("phrase 1 " + phrase1);
       int indexOfLastSpace = phrase1.lastIndexOf(" ");
       System.out.println("index of last pasce " + indexOfLastSpace);
       String phrase2 = phrase1.substring(indexOfLastSpace);
       System.out.println("phrase 2 " + phrase2);

         */

     //   StringBuilder
    }
}
