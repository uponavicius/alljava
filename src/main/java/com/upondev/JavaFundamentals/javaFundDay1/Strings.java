package com.upondev.JavaFundamentals.javaFundDay1;

public class Strings {
    public static void main(String[] args) {
        String a = "abc";
        String b = new String("abc");
        System.out.println(a==b);
        System.out.println(a.equals(b));

        String c = "Vladas";
        String d = "Uponavicius";
        System.out.println(c + " " + d + "\n" + "Siandien viskas gerai gaunasi");

    }
}
