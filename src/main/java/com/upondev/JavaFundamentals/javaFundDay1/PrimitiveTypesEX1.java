package com.upondev.JavaFundamentals.javaFundDay1;

public class PrimitiveTypesEX1 {
    public static void main(String[] args) {
        //1.	Define declare and initialize ) two variables : one of type “int ” and second of type “double ”.
        // Print theirs values
        //Output:
        //19
        //187.2342

        int sveikas = 19;
        double nesveikas = 187.2342;
        int intMax = Integer.MAX_VALUE;
        int intMin = Integer.MAX_VALUE;
        System.out.println("Exercises 1");
        System.out.println(sveikas);
        System.out.println(nesveikas);
       // 2.	Define variable of type int. What is a maximum and minimum value,
        // that you are able to store within that variable?
        System.out.println("Exercises 2");
        System.out.println("Int max " + intMax);
        System.out.println("Int min " + intMin);
        //3.	Do the same as above for other numeric types long, double, byte ..). Check results
        double doubleMax = Double.MAX_VALUE;
        byte byteMax = Byte.MAX_VALUE;
        byte byteMin = Byte.MIN_VALUE;
        long longMax = Long.MAX_VALUE;
        long longMin = Long.MIN_VALUE;
        float floatMax = Float.MAX_VALUE;
        float floatMin = Float.MIN_VALUE;
        System.out.println("Exercises 3");
        System.out.println("Byte max " + byteMax);
        System.out.println("Byte min " + byteMin);
        System.out.println("Long max" + longMax);
        System.out.println("Long min" + longMin);
        System.out.println("Float max " + floatMax);
        System.out.println("Float min " + floatMin);
        //4.	Define the int type variable with maximum value. Add 1 to it. What do you think will happen?
        System.out.println("Exercises 4");
        System.out.println("Int max +1 " + intMax + 1);
        System.out.println("Byte max +1 " + byteMax + 1);
        System.out.println("Long max +1 " + longMax + 1);
        System.out.println("Float max +1 " + floatMax + 1);



    }

}
