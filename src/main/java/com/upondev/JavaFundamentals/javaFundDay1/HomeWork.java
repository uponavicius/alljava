package com.upondev.JavaFundamentals.javaFundDay1;

public class HomeWork {
    public static void main(String[] args) {
        //•	char charAt (int index)
        String textLine1="Lorem ipsum dolor sit amet, consectetur adipiscing elit";


        char textCharAt = textLine1.charAt(1);
        System.out.println("Char: " + textCharAt); //o raide

        //•	int compareTo (String other)
        String textLine2="Lorem ipsum dolor sit amet, consectetur adipiscing elit";
        int intCompare = textLine1.compareTo(textLine2);
        System.out.println("Compare to: " + intCompare);

        //•	boolean endsWith (String suffix)
        boolean booleanEndsWith = textLine1.endsWith("elit");
        System.out.println("Boolean ends elit: " + booleanEndsWith);

        //•	boolean equals(Object other)
        boolean booleanEquals = textLine1.equals("amet");
        System.out.println("Boolean equals amet: " + booleanEquals);

        //•	boolean equalsIgnoreCase (String other)
        String textLine3="LOREM ipsum dolor sit amet, consectetur adipiscing elit";
        boolean eqIgnCase = textLine1.equalsIgnoreCase(textLine3);
        System.out.println("Boolean equals Ignore Case: " + eqIgnCase);

        //•	int indexOf (String str)
        int intIndexOf = textLine1.indexOf(",");
        System.out.println("Index of comma is: " + intIndexOf);

        //•	int lastIndexOf (String str)
        int intLastIndexOf = textLine1.lastIndexOf("t");
        System.out.println("Last index of: " + intLastIndexOf);

        //•	int length()
        int intLenght = textLine1.length();
        System.out.println("Lext line lenght: " + intLenght);

        //•	String replace( CharSequence oldString , CharSequence newString)
        String stringReplace = textLine1.replace("elit", "- koks cia dar elitas :D");
        System.out.println(stringReplace);

        //•	boolean startsWith (String prefix)
        boolean booleanStartWith = textLine1.startsWith("Rorem");
        System.out.println("Boolean start with Rorem: " + booleanStartWith);

        //•	String substring(int beginIndex)

        String delfiLine = "Adomaitis įsiuto oficialioje spaudos konferencijoje - keikėsi ir trenkė į stalą";
        /*
        int minInt = delfiLine.indexOf("-");
        String delfiLineShort = delfiLine.substring(0,minInt-1);
        int indexOfSpace = delfiLineShort.lastIndexOf(" ");
        String wordSearch = delfiLineShort.substring(indexOfSpace+1);
        System.out.println(wordSearch);
        */
        //Optimizuotas kodas
        String wordSearch = delfiLine.substring(0,delfiLine.indexOf("-")-1)
                .substring(delfiLine.substring(0,delfiLine.indexOf("-")-1)
                        .lastIndexOf(" ")+1);
        System.out.println("Word search: " + wordSearch);

        //•	String toLowerCase ()
        String lowerCase = delfiLine.toLowerCase();
        System.out.println(lowerCase);

        //•	String toUpperCase ()
        String upCase = delfiLine.toUpperCase();
        System.out.println(upCase);

        //•	String trim()
        String longText = "    Hello, JAVA!       ";
        String textTrim = longText.trim();
        System.out.println(textTrim);



    }
}
