package com.upondev.JavaFundamentals.javaFundDay1;

public class PrimitiveTypesEX2 {
    public static void main(String[] args) {
        //1.	Create two variables of type int with initial values of 6 and 11. Print sum of those variables

        int x = 6;
        int y = 11;
        System.out.println("EX 1");
        System.out.println(x + y);

        // 2.	Read any double literal from the console . Print that value rounded to the second decimal place.
        // Input: 3.23523
        // Output 3.23
        double v = 3.23523;
        double roundOff = Math.round(v * 100.0) / 100.0;
        System.out.println(roundOff);

        //3.	*Print values : 192, 168, 1, 10 in HEX format XX:XX:XX:XX.
        //        Use System.out.printf () method
        //Input: 192, 168, 1, 10
        //Output: „C0:A8:01:A0”
        int a = 192;
        int b = 168;
        int c = 1;
        int d = 10;
        System.out.printf("%02X:%02X:%02X:%02X", a, b, c, d);
    }
}
