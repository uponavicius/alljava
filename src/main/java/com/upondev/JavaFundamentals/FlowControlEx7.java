package com.upondev.JavaFundamentals;

import java.util.Scanner;
import java.text.DecimalFormat;

public class FlowControlEx7 {
    public static void main(String[] args) {
        /*
        Write a “divide by” application. User should be able to enter initial value that will be divided in a loop by a new value entered by a user.
        Division should occur as long, as entered value will be different than 0.
        Result of division should be rounded to the fourth decimal point and printed to the console.
         */

        /**
         * @param number ima iš komandinės eilutės, kuriuos suveda vertotojas. Šis skaičius bus dalinamas;
         * @param daliklis ima iš komandinės eilutės, kuriuos suveda vartotojas. Iš šio skaičiaus dalinama
         * Kad kitiems butu gera gyventi :D
         */


        Scanner scanner = new Scanner(System.in);
        System.out.println("Įveskite sveika skaičių kuri norite dalinti");
        double number = scanner.nextInt();

        System.out.println("Įveskite sveiką skaičių iš kurio norite dalinti");
        double daliklis = scanner.nextInt();
        double rez;
        DecimalFormat df = new DecimalFormat("#.####");

        for (double i = number; number > 1; i++ ) {
            rez = number / daliklis;
            number = rez;
            if (rez>1) {
                System.out.println(df.format(rez));
            }
        }
    }
}

/*
Gerardao variantas

Scanner input = new Scanner(System.in);
            System.out.println("Enter initial value: ");
            double number = input.nextDouble();
            System.out.println("Enter division value:");
            int divisionNumber = input.nextInt();

            while (number > 1) {
                number /= divisionNumber;
                System.out.printf("%.4f", number);
                System.out.println();
            }
        } catch (InputMismatchException e) {
            System.out.println("Wrong input.");
        }

 */