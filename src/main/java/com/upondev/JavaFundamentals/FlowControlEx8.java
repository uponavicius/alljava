package com.upondev.JavaFundamentals;

import java.util.Scanner;

public class FlowControlEx8 {
    public static void main(String[] args) {
        /**
        Write a simple “echo” application, that will:
            a. print back entered string,
            b. go to the beginning of a loop if user will enter “continue”,
            c. break the loop with a “good bye!” message, if user will enter “quit”.
         */

        Scanner input = new Scanner(System.in);

        while (true) {
            System.out.println("Write your word: ");
            String word = input.nextLine();
            if (word.equalsIgnoreCase("continue")) {
                continue;
            } else if (word.equalsIgnoreCase("quit")) {
                break;
            } else {
                System.out.println(word);
            }
        }



    }
}
