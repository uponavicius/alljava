package com.upondev.JavaFundamentals.Shop;

import java.util.Scanner;

public class RunShop {
    public static void main(String[] args) {
        /**
         *         Write a simple application that will simulate a shopping. Use only if-else flow control. Consider following cases:
         *         a. If you would like to buy a bottle of milk – cashier will ask you for a specific amount of money. You have to enter that value and verify if it is same as the cashier asked.
         *         b. If you would like to buy a bottle of wine – cashier will ask you if you are an adult and for positive answer ask for a specific amount of money.
         */

        int milk1Code = 1;
        double milk1Price = 1;
        String milk1Name = "Pienas 1%";

        int milk25Code = 2;
        double milk25Price = 1.2;
        String milk25Name = "Pienas 2.5%";


        int milk35Code = 3;
        double milk35Price  = 1.5;
        String milk35Name = "Pienas 3.5%";


        int redWineCode = 4;
        double redWinePrice = 25;
        String redWineName = "Raudonas vynas";


        int roseWineCode = 5;
        double roseWinePrice = 27;
        String roseWineName = "Rožinis vynas";

        int whiteWineCode = 6;
        double whiteWinwPrice = 30;
        String whiteWineName = "Baltas vynas";

        double priceIn;



        Scanner scanner = new Scanner(System.in);
        System.out.println("Sveiki atvykę į mano shop'ą. Čia galite įsigyti pieno ir vyno.");
        System.out.println("Produktai:");
        System.out.println("Produktas: " + milk1Name + ".          Kaina " + milk1Price + " EUR/1L.        Produkto kodas: " + milk1Code);
        System.out.println("Produktas: " + milk25Name + ".        Kaina " + milk25Price + " EUR/1L.        Produkto kodas: " + milk25Code);
        System.out.println("Produktas: " + milk35Name + ".        Kaina " + milk35Price + " EUR/1L.        Produkto kodas: " + milk35Code);
        System.out.println("Produktas: " + redWineName + ".     Kaina " + redWinePrice + " EUR/1L.       Produkto kodas: " + redWineCode);
        System.out.println("Produktas: " + roseWineName + ".      Kaina " + roseWinePrice + " EUR/1L.       Produkto kodas: " + roseWineCode);
        System.out.println("Produktas: " + whiteWineName + ".       Kaina " + whiteWinwPrice + " EUR/1L.       Produkto kodas: " + whiteWineCode);

        System.out.println("*****************************************\n");
        System.out.println("Įveskite produkto kodą, kurio produkto norite");
        int productCodeIn = scanner.nextInt();



        /*while (true) {
        System.out.println("Write your word: ");
        String word = input.nextLine();
        if (word.equalsIgnoreCase("continue")) {
            continue;
        } else if (word.equalsIgnoreCase("quit")) {
            break;
        } else {
            System.out.println(word);
        }
    }
 */

        if (productCodeIn == milk1Code){ //kodas 1


                System.out.println("Kaina produkto: " + milk1Name + " yra " + milk1Price + " EUR. " + "Prašaų įvesti produkto kaina:");
                priceIn = scanner.nextDouble();
                RunShop.priceCheck(milk1Price, priceIn);


        } else if (productCodeIn == milk25Code){ //kodas 2
            System.out.println("Kaina produkto: " + milk25Name + " yra " + milk25Price + " EUR. " +"Prašaų įvesti produkto kaina:");
            priceIn = scanner.nextDouble();
            RunShop.priceCheck(milk25Price,priceIn);

        } else if (productCodeIn == milk35Code){ //kodas 3
            System.out.println("Kaina produkto: " + milk35Name + " yra " + milk35Price + " EUR. " +"Prašaų įvesti produkto kaina:");
            priceIn = scanner.nextDouble();
            RunShop.priceCheck(milk35Price,priceIn);

        } else if (productCodeIn == redWineCode){ //kodas 4
            System.out.println("Kiek Jums metų?");
            int costumerAge = scanner.nextInt();
            RunShop.cosutmerAgeCheck(costumerAge);
            System.out.println("Kaina produkto: " + redWineName + " yra " + redWinePrice + " EUR. " +"Prašaų įvesti produkto kaina:");
            priceIn = scanner.nextDouble();
            RunShop.priceCheck(redWinePrice,priceIn);

        } else if (productCodeIn == roseWineCode){ //kodas 5
            System.out.println("Kiek Jums metų?");
            int costumerAge = scanner.nextInt();
            RunShop.cosutmerAgeCheck(costumerAge);
            System.out.println("Kaina produkto: " + roseWineName + " yra " + roseWinePrice + " EUR. " +"Prašaų įvesti produkto kaina:");
            priceIn = scanner.nextDouble();
            RunShop.priceCheck(roseWinePrice,priceIn);

        } else if (productCodeIn == whiteWineCode){ //kodas 6
            int costumerAge = scanner.nextInt();
            RunShop.cosutmerAgeCheck(costumerAge);
            System.out.println("Kaina produkto: " + whiteWineName + " yra " + whiteWinwPrice + " EUR. " +"Prašaų įvesti produkto kaina:");
            priceIn = scanner.nextDouble();
            RunShop.priceCheck(whiteWinwPrice,priceIn);

        } else {
            System.out.println("Toks produkto kodas nerastas");
        }




    }

    // METODAI //




    /*public static boolean priceCheck (double productPrice, double priceIn){
        if (priceIn >= productPrice){
            double change = priceIn - productPrice;
            System.out.println("Jūsų grąža: " + change + " EUR");
            return true;
        } else {
            double missingMoney = productPrice - priceIn;
            System.out.println("Iki reikiamos sumos Jums trūksta: " + missingMoney + "EUR");
            return false;
        }

    }

     */

    public static boolean priceCheck (double productPrice, double priceIn) {
        while (true) {
            if (priceIn >= productPrice) {
                double change = priceIn - productPrice;
                System.out.println("Jūsų grąža: " + change + " EUR");
                return true;
            } else {
                double missingMoney = productPrice - priceIn;
                System.out.println("Iki reikiamos sumos Jums trūksta: " + missingMoney + "EUR");
                return false;
            }
        }
    }

    public static boolean cosutmerAgeCheck (double userEnterAge){
        if (userEnterAge>=20){
            System.out.println("Tinkamas amzius");
            return true;
        } else {
            System.out.println("Alkoholinius gėrimus galima įsigyti nuo 20 metų!");
            return false;
        }

    }

/*
while (true) {
        System.out.println("Write your word: ");
        String word = input.nextLine();
        if (word.equalsIgnoreCase("continue")) {
            continue;
        } else if (word.equalsIgnoreCase("quit")) {
            break;
        } else {
            System.out.println(word);
        }
    }
 */




}
