package com.upondev.JavaFundamentals.TurboBank;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Scanner;
import java.util.regex.Pattern;

public class RunTurboBankPlusII {
    //----- INSTANCE KINTAMIEJI -----//
    static String passwordRegex = "[0-9]{4}";
    static String bankAccuntNumberRegex = "[LT]{2}[0-9]{2} [0-9]{4} [0-9]{4} [0-9]{4}"; //FORMATAS LT00 0000 0000 0000
    static Pattern passwordPatter = Pattern.compile(passwordRegex);
    static Pattern bankAccountPatter = Pattern.compile(bankAccuntNumberRegex);
    static Date date = new Date();
    //*****************************************************************************************************************

    public static void main(String[] args) {
        boolean pinCodeLogical = true;
        boolean bankAccountNumberLogical = true;
        boolean pinCodeLoginlLogical = true;
        int pinCode;
        String visaAccountNumber = "";
        double visaStartBalance = 0;
        String masterCardAccountNumber = "";
        double masterCardStartBalance = 0;
        String operation = "";


        //----- KURIAMA BANKO SĄSKAITA ----//
        Scanner scanner = new Scanner(System.in);
        System.out.println("***** NAUJOS SĄSKAITOS ATIDARYMAS *****");
        System.out.println("Įrašykite savo vardą ir pavardę");
        String holderName = scanner.nextLine();

        System.out.println("Pasirinkite sąakaitos tipą:");
        System.out.println("Visa, rašykite:       v");
        System.out.println("MasterCard, rašykite: m");
        String inputBankNumberType = scanner.nextLine();


        if (inputBankNumberType.equals("v")){
            do {
                System.out.println("Įrašykite Visa sąskaitos numerį. Formatas 'LT00 0000 0000 0000'");
                visaAccountNumber = scanner.nextLine();
                if (RunTurboBankPlus.checkBankAccNr(visaAccountNumber)) {
                    bankAccountNumberLogical = true;

                    System.out.println("Kokią pinigų sumą norite įnėšti į Visa sąskaitą?"); // KAI IDEDU PINIGU INESIMA I SASKAIA, TOLIAU PRASOKA, INESIMA ISIMU - VEIKIA
                    visaStartBalance = scanner.nextDouble();

                    System.out.println("Ar atidaryti ir MasterCard tipo sąskaita?");
                    System.out.println("Atidaryti, rašykite:   y");
                    System.out.println("Neatidaryti, rašykite: n");
                    String openMasterCardAccount = scanner.nextLine();
                    if (openMasterCardAccount.equals("y")){
                        do {
                            System.out.println("Įrašykite Visa sąskaitos numerį. Formatas 'LT00 0000 0000 0000'");
                            masterCardAccountNumber = scanner.nextLine();
                            if (RunTurboBankPlus.checkBankAccNr(masterCardAccountNumber)) {
                                bankAccountNumberLogical = true;
                                System.out.println("Kokią pinigų sumą norite įnėšti į MasterCard sąskaitą?");
                                masterCardStartBalance = scanner.nextDouble();
                            } else {
                                bankAccountNumberLogical = false;
                            }
                        } while (!bankAccountNumberLogical);
                    } else if (openMasterCardAccount.equals("n")){
                        continue;
                    }
                } else {
                    bankAccountNumberLogical = false;
                }
            } while (!bankAccountNumberLogical);


        } else if (inputBankNumberType.equals("m")){
            do {
                System.out.println("Įrašykite MasterCard sąskaitos numerį. Formatas 'LT00 0000 0000 0000'");
                masterCardAccountNumber = scanner.nextLine();
                if (RunTurboBankPlus.checkBankAccNr(masterCardAccountNumber)) {
                    bankAccountNumberLogical = true;
                    System.out.println("Kokią pinigų sumą norite įnėšti į MasterCard sąskaitą?");
                    masterCardStartBalance = scanner.nextDouble();
                    System.out.println("Ar atidaryti ir Visa tipo sąskaita?");
                    System.out.println("Atidaryti, rašykite:   y");
                    System.out.println("Neatidaryti, rašykite: n");
                    String openVisaCardAccount = scanner.nextLine();
                    if (openVisaCardAccount.equals("y")){
                        do {
                            System.out.println("Įrašykite Visa sąskaitos numerį. Formatas 'LT00 0000 0000 0000'");
                            masterCardAccountNumber = scanner.nextLine();
                            if (RunTurboBankPlus.checkBankAccNr(masterCardAccountNumber)) {
                                bankAccountNumberLogical = true;
                                System.out.println("Kokią pinigų sumą norite įnėšti į Visa sąskaitą?");
                                visaStartBalance = scanner.nextDouble();
                            } else {
                                bankAccountNumberLogical = false;
                            }
                        } while (!bankAccountNumberLogical);
                    } else if (openVisaCardAccount.equals("n")){
                        continue;
                    }
                } else {
                    bankAccountNumberLogical = false;
                }
            } while (!bankAccountNumberLogical);
        }




        do {
            System.out.println("Įrašykite PIN kodą iš 4 skaičių");
            pinCode = scanner.nextInt();
            if (RunTurboBankPlusII.checkPinCode(pinCode)) {
                System.out.println("**** BANKO SĄSKAITA SUKURTA ****");
                RunTurboBankPlusII.timeStampInfo();
                pinCodeLogical = true;
            } else {
                System.out.println("**** Pin kodas neteisignas ****");
                pinCodeLogical = false;
            }
        } while (!pinCodeLogical);

        //----- PRISIJUNGIMAS PRIE BANKO -----//
        Visa bankAccount = new Visa(holderName, visaAccountNumber, visaStartBalance, pinCode);
        MasterCard bankAccountMasterCard = new MasterCard(masterCardAccountNumber, masterCardStartBalance);
        System.out.println("************************************");
        System.out.println("**** Prisijungimas prie banko *****");


        //----- PATIKRINIMAS AR PIN KODAS IS 4 SKAITMENU IR AR TOKS KAIP SUKURTA PIN KODAS -----//


        do {
            System.out.println("Įveskite PIN kodą prisijungimui");
            int userEnterLoginPinCode = scanner.nextInt();
            if (bankAccount.checkPinCode(userEnterLoginPinCode) & bankAccount.checkVisaLoginPin(userEnterLoginPinCode)) {
                System.out.println("PRISIJUNGETE");
                pinCodeLoginlLogical = true;
            } else {
                //System.out.println("**** Prisijungiant Pin kodas neteisignas ****");
                pinCodeLoginlLogical = false;
            }
        } while (!pinCodeLoginlLogical);



        bankAccount.getVisaAccountNumber();
        bankAccount.getVisaBalance();
        bankAccount.getVisaPinCode();
        bankAccountMasterCard.getMasterCardAccountNumber();
        bankAccountMasterCard.getMasterCardBalance();



        } //----- MAIN METODO PABAIGA -----//

        //----- METODAI -----//

        public static boolean checkPinCode (int userEnterPinCode) {
            if (passwordPatter.matcher(Integer.toString(userEnterPinCode)).matches()) {
                System.out.println("Įvestas teisingas PIN kodas");
                return true;
            } else {
                //System.out.println("Įvestas PIN kodas neteisingas. Įveskite iš 4 skaitmenų");
                return false;
            }
        }

        public static boolean checkBankAccNr (String userEnterBankAccNr) {
            if (bankAccountPatter.matcher(userEnterBankAccNr).matches()){
                System.out.println("**** SASKAITOS NUMERIS TEISINGA ****");
                return true;
            } else {
                System.out.println("**** SĄSKAITOS NUMERIS NETEISINGAS ****");
                return false;
            }
        }

        public static long timeStampInfo(){
            long time = date.getTime();
            Timestamp timestamp = new Timestamp(time);
            System.out.println(timestamp);
            return time;
        }



    }

