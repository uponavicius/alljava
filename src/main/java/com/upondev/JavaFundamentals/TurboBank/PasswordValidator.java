package com.upondev.JavaFundamentals.TurboBank;
import java.util.regex.Pattern;

public class PasswordValidator {

    private static final String passwordRegex = "[0-9]{4}";
    private static final String bankAccuntNumberRegex = "[LT]{2}[0-9]{2} [0-9]{4} [0-9]{4} [0-9]{4}";

    private static final Pattern passwordPatter = Pattern.compile(passwordRegex);
    private static final Pattern bankAccountPatter = Pattern.compile(bankAccuntNumberRegex);

    public static void main(String[] args) {
        String password = "1283";
        String bankAccount = "LT00 0000 0000 0000";

        //password patikra
        if (passwordPatter.matcher(password).matches()){
            System.out.println("Įvestas teisingas PIN kodas");
        } else {
            System.out.println("Įvestas PIN kodas neteisingas. Įveskite iš 4 skaitmenų");
        }

        if (bankAccountPatter.matcher(bankAccount).matches()){
            System.out.println("Sąskaitos numeris teisingas");
        } else {
            System.out.println("Sąskaitos numeris neteisingas. Banko numerio formatas LT00 0000 0000 0000");
        }



    }


}
