package com.upondev.JavaFundamentals.TurboBank;

import java.sql.Timestamp;
import java.util.Date;
import java.util.regex.Pattern;

public class Visa {

    private String accountName;
    // VISA //
    private String visaAccountNumber;
    private double visaBalance;
    private int visaPinCode;

    // MASTER CARD //
    private String masterCardAccountnumber;
    private double masterCardBalance;
    private int masterCardPinCode;


    Date date = new Date();

    private static final String passwordRegex = "[0-9]{4}";
    private static final String bankAccuntNumberRegex = "[LT]{2}[0-9]{2} [0-9]{4} [0-9]{4} [0-9]{4}";
    private static final Pattern passwordPatter = Pattern.compile(passwordRegex);
    private static final Pattern bankAccountPatter = Pattern.compile(bankAccuntNumberRegex);

    //------KONSTRUKTORIAI-------
    public Visa(String accountName, String visaAccountNumber, double visaBalance, int visaPinCode){
        this.accountName = accountName;
        this.visaAccountNumber = visaAccountNumber;
        this.visaBalance = visaBalance;
        this.visaPinCode = visaPinCode;
    }

    //------METODAI--------
    public void visaMoneyPlus(double plus){
        this.visaBalance = this.visaBalance + plus;
        System.out.print("Jūsų sąskaitos balansas: " + this.visaBalance);
        System.out.println("");
    }

    public void visaMoneyMinus (double minus){
        this.visaBalance = this.visaBalance - minus;
        System.out.print("Jūsų sąskaitos balandas: " + this.visaBalance);
        System.out.println("");
    }

    public int getVisaPinCode(){
        System.out.println("Jūsų Pin kodas: " + this.visaPinCode);
        return this.visaPinCode;
    }

    public double getVisaBalance(){
        System.out.println("Jūsų sąskaitos balansas: " + this.visaBalance);
        //System.out.println(this.balance);
        return this.visaBalance;
    }

    public String getVisaAccountNumber(){
        System.out.println("Jūsų sąskaitos numeris: " + this.visaAccountNumber);
        return this.visaAccountNumber;
    }

    public void setVisaPinCode(int visaPinCode){
        this.visaPinCode = visaPinCode;
    }

    public void timeStampInfo(){
        long time = date.getTime();
        Timestamp timestamp = new Timestamp(time);
        System.out.println(timestamp);
    }

    public boolean checkPinCode (int userEnterPinCode){ //patikrina ar kodas yra is 4 skaiciu
        if (passwordPatter.matcher(Integer.toString(userEnterPinCode)).matches()){
            System.out.println("***** PIN kodo formatas teisingas *****");
            return true;
        } else {
            System.out.println("Įvestas PIN kodas neteisingas. Įveskite iš 4 skaitmenų");
            return false;
        }
    }


    public boolean checkBankAccountNumber (String userEnterBankAccountNumber){
        if (bankAccountPatter.matcher(userEnterBankAccountNumber).matches()){
            System.out.println("Įvesta banko sąskaita teisingai");
            return true;
        } else {
            System.out.println("Sąskaitos numeris neteisingas. Banko numerio formatas LT00 0000 0000 0000");
            return false;
        }
    }

    public boolean checkVisaLoginPin (int userEnterPinLogin){ //Patikrinimas ar ivedamas teisignas pin kodas
        if (userEnterPinLogin == visaPinCode){
            System.out.println("***** Prisijungėte prie banko ****");
            return true;
        } else {
            System.out.println("Įvestas PIN kodas nesutampa su sistema");
            return false;
        }
    }







}
