package com.upondev.JavaFundamentals.TurboBank;

import java.util.Scanner;
import java.util.regex.Pattern;
import java.sql.Timestamp;
import java.util.Date;

public class RunTurboBank {
    //----- INSTANCE KINTAMIEJI -----//
    static String passwordRegex = "[0-9]{4}";
    static String bankAccuntNumberRegex = "[LT]{2}[0-9]{2} [0-9]{4} [0-9]{4} [0-9]{4}"; //FORMATAS LT00 0000 0000 0000
    static Pattern passwordPatter = Pattern.compile(passwordRegex);
    static Pattern bankAccountPatter = Pattern.compile(bankAccuntNumberRegex);
    static Date date = new Date();
    //*****************************************************************************************************************

    public static void main(String[] args) {
        boolean pinCodeLogical = true;
        boolean bankAccountNumberLogical = true;
        boolean pinCodeLoginlLogical = true;
        int pinCode;
        String accountNumber;
        String operation = "";


        //----- KURIAMA BANKO SĄSKAITA ----//
        //----- VARDAS PAVARDĖ ----//
        Scanner scanner = new Scanner(System.in);
        System.out.println("***** NAUJOS SĄSKAITOS ATIDARYMAS *****");
        System.out.println("Įrašykite savo vardą ir pavardę");
        String holdeName = scanner.nextLine();

        //----- BANKO SĄSKAITA -----//
        do {
            System.out.println("Įrašykite sąskaitą. Banko sąskaitos formatas 'LT00 0000 0000 0000'");
            accountNumber = scanner.nextLine();
            if (RunTurboBank.checkBankAccNr(accountNumber)) {
                //System.out.println("**** SASKAITA TEISINGA****");
                bankAccountNumberLogical = true;
            } else {
                //System.out.println("**** SASKAITA NETEISINGA ****");
                bankAccountNumberLogical = false;
            }
        } while (!bankAccountNumberLogical);


        //----- PRADINIS BALANSAS -----//
        System.out.println("Kokią pinigų sumą norite įnešti atidarant sąskaitą: ");
        double startBalance = scanner.nextDouble();

        //-----PIN KODO SUKURIMAS-----///
        do {
            System.out.println("Įrašykite PIN kodą iš 4 skaičių");
            pinCode = scanner.nextInt();
            if (RunTurboBank.checkPinCode(pinCode)) {
                System.out.println("**** BANKO SĄSKAITA SUKURTA ****");
                RunTurboBank.timeStampInfo();
                pinCodeLogical = true;
            } else {
                System.out.println("**** Pin kodas neteisignas ****");
                pinCodeLogical = false;
            }
        } while (!pinCodeLogical);

        //----- PRISIJUNGIMAS PRIE BANKO -----//
        Visa bankAccount = new Visa(holdeName, accountNumber, startBalance, pinCode);
        System.out.println("************************************");
        System.out.println("**** Prisijungimas prie banko *****");


        //----- PATIKRINIMAS AR PIN KODAS IS 4 SKAITMENU IR AR TOKS KAIP SUKURTA PIN KODAS -----//


        do {
            System.out.println("Įveskite PIN kodą prisijungimui");
            int userEnterLoginPinCode = scanner.nextInt();
            if (bankAccount.checkPinCode(userEnterLoginPinCode) & bankAccount.checkVisaLoginPin(userEnterLoginPinCode)) {
                System.out.println("PRISIJUNGETE");
                pinCodeLoginlLogical = true;
            } else {
                //System.out.println("**** Prisijungiant Pin kodas neteisignas ****");
                pinCodeLoginlLogical = false;
            }
        } while (!pinCodeLoginlLogical);

        while (!operation.equals("exit")) {
            System.out.println("Įnešti pinigų, rašykite:           +");
            System.out.println("Pinigų išėmimas, rašykite:         -");
            System.out.println("Sąskaitos balansas, rašykite:      *");
            System.out.println("Sąskaitos numeris, rašykite:       s");
            System.out.println("Paskeisti PIN koda, rašykite:      Y");
            System.out.println("Išeiti iš banko, rašykite:      exit");
            operation = scanner.next();
            if (operation.equals("exit")) {
                continue; //jei įvedama exit, toliau nebetesiama patikros
            }



            if (operation.equals("+")) {
                System.out.println("Parašykite pinigų sumą kokią norite įnešti");
                int moneyIn = scanner.nextInt();
                bankAccount.visaMoneyPlus(moneyIn);

            } else if (operation.equals("*")) {
                bankAccount.getVisaBalance();

            } else if (operation.equals("s")) {
                System.out.println("Jūsų sąskaitos numeris");
                bankAccount.getVisaAccountNumber();

            } else if (operation.equals("Y")){
                System.out.println("Įveskite naują pin kodą");
                int changePin = scanner.nextInt();
                bankAccount.setVisaPinCode(changePin);
                //bankAccount.visaMoneyMinus();
                //System.out.println("Jūsų naujas Pin kodas: " + bankAccount.getPinCode());
            } else if (operation.equals("-")){
                System.out.println("Kokią pinigų sumą norite pasiimti: ");
                int moneyOut = scanner.nextInt();
                if (bankAccount.getVisaBalance() >= moneyOut){
                    System.out.println("Jūsų pasimta pinigų suma: " + moneyOut);
                    bankAccount.visaMoneyMinus(moneyOut);
                } else if (bankAccount.getVisaBalance()< moneyOut){
                    System.out.println("Jūsų pageidaujama pinigų suma virsija balansą.");
                    //System.out.println("Jūsų balansas: " + bankAccount.getBalance());
                    bankAccount.getVisaBalance();
                }


            }


        }















        System.out.println("==================================================");
        System.out.println("=  ********  **   **  ******   *****     *****   =");
        System.out.println("=  ********  **   **  ******   ******   *******  =");
        System.out.println("=     **     **   **  **  **   **  **   **   **  =");
        System.out.println("=     **     **   **  ******   *****    **   **  =");
        System.out.println("=     **     **   **  ****     ******   **   **  =");
        System.out.println("=     **     **   **  ****     **  **   **   **  =");
        System.out.println("=     **     *******  **  **   ******   *******  =");
        System.out.println("=     **     *******  **  **   *****     *****   =");
        System.out.println("==================================================");
        System.out.println("==================== BANKAS ======================");
        //*************************************************************************************************************
        } //----- MAIN METODO PABAIGA -----//

        //----- METODAI -----//

        public static boolean checkPinCode (int userEnterPinCode) {
            if (passwordPatter.matcher(Integer.toString(userEnterPinCode)).matches()) {
                System.out.println("Įvestas teisingas PIN kodas");
                return true;
            } else {
                //System.out.println("Įvestas PIN kodas neteisingas. Įveskite iš 4 skaitmenų");
                return false;
            }
        }

        public static boolean checkBankAccNr (String userEnterBankAccNr) {
            if (bankAccountPatter.matcher(userEnterBankAccNr).matches()){
                System.out.println("**** SASKAITA TEISINGA ****");
                return true;
            } else {
                System.out.println("**** NETEISINGAS SĄSKAITOS NUMERIS ****");
                return false;
            }
        }

        public static long timeStampInfo(){
            long time = date.getTime();
            Timestamp timestamp = new Timestamp(time);
            System.out.println(timestamp);
            return time;
        }



    }

