package com.upondev.JavaFundamentals.TurboBank;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Scanner;
import java.util.regex.Pattern;

public class RunTurboBankPlus {
    //----- INSTANCE KINTAMIEJI -----//
    static String passwordRegex = "[0-9]{4}";
    static String bankAccuntNumberRegex = "[LT]{2}[0-9]{2} [0-9]{4} [0-9]{4} [0-9]{4}"; //FORMATAS LT00 0000 0000 0000
    static Pattern passwordPatter = Pattern.compile(passwordRegex);
    static Pattern bankAccountPatter = Pattern.compile(bankAccuntNumberRegex);
    static Date date = new Date();
    //*****************************************************************************************************************

    public static void main(String[] args) {
        boolean pinCodeLogical = true;
        boolean bankAccountNumberLogical = true;
        boolean pinCodeLoginlLogical = true;
        int pinCode;
        String visaAccountNumber = "";
        double visaStartBalance = 0;
        String masterCardAccountNumber = "";
        double masterCardStartBalance = 0;
        String operation = "";


        //----- KURIAMA BANKO SĄSKAITA ----//
        Scanner scanner = new Scanner(System.in);
        System.out.println("***** NAUJOS SĄSKAITOS ATIDARYMAS *****");
        System.out.println("Įrašykite savo vardą ir pavardę");
        String holderName = scanner.nextLine();

        System.out.println("Pasirinkite sąakaitos tipą:");
        System.out.println("Visa, rašykite:       v");
        System.out.println("MasterCard, rašykite: m");
        String inputBankNumberType = scanner.nextLine();

        switch (inputBankNumberType){
            case "v":
               do {
                   System.out.println("Įrašykite Visa sąskaitos numerį. Formatas 'LT00 0000 0000 0000'");
                   visaAccountNumber = scanner.nextLine();
                   if (RunTurboBankPlus.checkBankAccNr(visaAccountNumber)){
                       bankAccountNumberLogical = true;

                       System.out.println("Kiek norite įvešti pinigų atidarant Visa sąskaitą?");
                       visaStartBalance = scanner.nextDouble();

                       System.out.println("Ar atidaryti ir MasterCard tipo sąskaita?");
                       System.out.println("Atidaryti, rašykite:   y");
                       System.out.println("Neatidaryti, rašykite: n");
                       String openMasterCardAccount = scanner.nextLine();
                       switch (openMasterCardAccount){
                           case "y":
                               do {
                                   System.out.println("Įrašykite Visa sąskaitos numerį. Formatas 'LT00 0000 0000 0000'");
                                   masterCardAccountNumber = scanner.nextLine();
                                   if (RunTurboBankPlus.checkBankAccNr(masterCardAccountNumber)){
                                       bankAccountNumberLogical = true;
                                   } else {
                                       bankAccountNumberLogical = false;
                                   }
                               } while (!bankAccountNumberLogical);
                               break;
                           case "n":
                               break;
                       }

                   } else {
                       bankAccountNumberLogical = false;
                   }
                } while (!bankAccountNumberLogical);
                break;


            case "m":
                    do {
                    System.out.println("Įrašykite MasterCard sąskaitos numerį. Formatas 'LT00 0000 0000 0000'");
                    masterCardAccountNumber = scanner.nextLine();
                    if (RunTurboBankPlus.checkBankAccNr(masterCardAccountNumber)){
                        bankAccountNumberLogical = true;
                        System.out.println("Ar atidaryti ir Visa tipo sąskaita?");
                        System.out.println("Atidaryti, rašykite:   y");
                        System.out.println("Neatidaryti, rašykite: n");
                        String openVisaBankAccount = scanner.nextLine();
                        switch (openVisaBankAccount){
                            case "y":
                                do {
                                    System.out.println("Įrašykite Visa sąskaitos numerį. Formatas 'LT00 0000 0000 0000'");
                                    visaAccountNumber = scanner.nextLine();
                                    if (RunTurboBankPlus.checkBankAccNr(visaAccountNumber)){
                                        bankAccountNumberLogical = true;
                                    } else {
                                        bankAccountNumberLogical = false;
                                    }
                                } while (!bankAccountNumberLogical);
                            case "n":
                                break;
                        }

                    } else {
                        bankAccountNumberLogical = false;
                    }
                } while (!bankAccountNumberLogical);
                break;

        }




        /*
        if (inputBankNumberType.equals("v")){
            do {
                System.out.println("Įrašykite Visa sąskaitos numerį. Formatas 'LT00 0000 0000 0000'");
                visaAccountNumber = scanner.nextLine();
                if (RunTurboBankPlus.checkBankAccNr(visaAccountNumber)) {
                    bankAccountNumberLogical = true;
                    System.out.println("Kokią pinigų sumą norite įnėšti į Visa sąskaitą?");
                    visaStartBalance = scanner.nextDouble();
                    System.out.println("Ar atidaryti ir MasterCard tipo sąskaita?");
                    System.out.println("Atidaryti, rašykite:   y");
                    System.out.println("Neatidaryti, rašykite: n");
                    String openMasterCardAccount = scanner.nextLine();
                    if (openMasterCardAccount.equals("y")){
                        do {
                            System.out.println("Įrašykite Visa sąskaitos numerį. Formatas 'LT00 0000 0000 0000'");
                            masterCardAccountNumber = scanner.nextLine();
                            if (RunTurboBankPlus.checkBankAccNr(masterCardAccountNumber)) {
                                bankAccountNumberLogical = true;
                                System.out.println("Kokią pinigų sumą norite įnėšti į MasterCard sąskaitą?");
                                masterCardStartBalance = scanner.nextDouble();
                            } else {
                                bankAccountNumberLogical = false;
                            }
                        } while (!bankAccountNumberLogical);
                    } else if (openMasterCardAccount.equals("n")){
                        continue;
                    }
                } else {
                    bankAccountNumberLogical = false;
                }
            } while (!bankAccountNumberLogical);


        } else if (inputBankNumberType.equals("m")){
            do {
                System.out.println("Įrašykite MasterCard sąskaitos numerį. Formatas 'LT00 0000 0000 0000'");
                masterCardAccountNumber = scanner.nextLine();
                if (RunTurboBankPlus.checkBankAccNr(masterCardAccountNumber)) {
                    bankAccountNumberLogical = true;
                    System.out.println("Kokią pinigų sumą norite įnėšti į MasterCard sąskaitą?");
                    masterCardStartBalance = scanner.nextDouble();
                    System.out.println("Ar atidaryti ir Visa tipo sąskaita?");
                    System.out.println("Atidaryti, rašykite:   y");
                    System.out.println("Neatidaryti, rašykite: n");
                    String openVisaCardAccount = scanner.nextLine();
                    if (openVisaCardAccount.equals("y")){
                        do {
                            System.out.println("Įrašykite Visa sąskaitos numerį. Formatas 'LT00 0000 0000 0000'");
                            masterCardAccountNumber = scanner.nextLine();
                            if (RunTurboBankPlus.checkBankAccNr(masterCardAccountNumber)) {
                                bankAccountNumberLogical = true;
                                System.out.println("Kokią pinigų sumą norite įnėšti į Visa sąskaitą?");
                                visaStartBalance = scanner.nextDouble();
                            } else {
                                bankAccountNumberLogical = false;
                            }
                        } while (!bankAccountNumberLogical);
                    } else if (openVisaCardAccount.equals("n")){
                        continue;
                    }
                } else {
                    bankAccountNumberLogical = false;
                }
            } while (!bankAccountNumberLogical);
        }

         */


        //----- BANKO SĄSKAITA -----//
        /*
        do {
            System.out.println("Įrašykite sąskaitą. Banko sąskaitos formatas 'LT00 0000 0000 0000'");
            accountNumber = scanner.nextLine();
            if (RunTurboBankPlus.checkBankAccNr(accountNumber)) {
                bankAccountNumberLogical = true;
            } else {
                bankAccountNumberLogical = false;
            }
        } while (!bankAccountNumberLogical);

         */




        //----- PRADINIS BALANSAS -----//
        //System.out.println("Kokią pinigų sumą norite įnešti atidarant sąskaitą: ");
        //double startBalance = scanner.nextDouble();

        //-----PIN KODO SUKURIMAS-----///
        do {
            System.out.println("Įrašykite PIN kodą iš 4 skaičių");
            pinCode = scanner.nextInt();
            if (RunTurboBankPlus.checkPinCode(pinCode)) {
                System.out.println("**** BANKO SĄSKAITA SUKURTA ****");
                RunTurboBankPlus.timeStampInfo();
                pinCodeLogical = true;
            } else {
                System.out.println("**** Pin kodas neteisignas ****");
                pinCodeLogical = false;
            }
        } while (!pinCodeLogical);

        //----- PRISIJUNGIMAS PRIE BANKO -----//
        Visa bankAccount = new Visa(holderName, visaAccountNumber, visaStartBalance, pinCode);
        MasterCard bankAccountMasterCard = new MasterCard(masterCardAccountNumber, masterCardStartBalance);
        System.out.println("************************************");
        System.out.println("**** Prisijungimas prie banko *****");


        //----- PATIKRINIMAS AR PIN KODAS IS 4 SKAITMENU IR AR TOKS KAIP SUKURTA PIN KODAS -----//


        do {
            System.out.println("Įveskite PIN kodą prisijungimui");
            int userEnterLoginPinCode = scanner.nextInt();
            if (bankAccount.checkPinCode(userEnterLoginPinCode) & bankAccount.checkVisaLoginPin(userEnterLoginPinCode)) {
                System.out.println("PRISIJUNGETE");
                pinCodeLoginlLogical = true;
            } else {
                //System.out.println("**** Prisijungiant Pin kodas neteisignas ****");
                pinCodeLoginlLogical = false;
            }
        } while (!pinCodeLoginlLogical);



        bankAccount.getVisaAccountNumber();
        bankAccount.getVisaBalance();
        bankAccount.getVisaPinCode();
        bankAccountMasterCard.getMasterCardAccountNumber();
        bankAccountMasterCard.getMasterCardBalance();






        //////
        /*
        while (!operation.equals("exit")) {
            System.out.println("Įnešti pinigų, rašykite:           +");
            System.out.println("Pinigų išėmimas, rašykite:         -");
            System.out.println("Sąskaitos balansas, rašykite:      *");
            System.out.println("Sąskaitos numeris, rašykite:       s");
            System.out.println("Paskeisti PIN koda, rašykite:      Y");
            System.out.println("Išeiti iš banko, rašykite:      exit");
            operation = scanner.next();
            if (operation.equals("exit")) {
                continue; //jei įvedama exit, toliau nebetesiama patikros
            }



            if (operation.equals("+")) {
                System.out.println("Parašykite pinigų sumą kokią norite įnešti");
                int moneyIn = scanner.nextInt();
                bankAccount.visaMoneyPlus(moneyIn);

            } else if (operation.equals("*")) {
                bankAccount.getVisaBalance();

            } else if (operation.equals("s")) {
                System.out.println("Jūsų sąskaitos numeris");
                bankAccount.getVisaAccountNumber();

            } else if (operation.equals("Y")){
                System.out.println("Įveskite naują pin kodą");
                int changePin = scanner.nextInt();
                bankAccount.setVisaPinCode(changePin);
                //bankAccount.visaMoneyMinus();
                //System.out.println("Jūsų naujas Pin kodas: " + bankAccount.getPinCode());
            } else if (operation.equals("-")){
                System.out.println("Kokią pinigų sumą norite pasiimti: ");
                int moneyOut = scanner.nextInt();
                if (bankAccount.getVisaBalance() >= moneyOut){
                    System.out.println("Jūsų pasimta pinigų suma: " + moneyOut);
                    bankAccount.visaMoneyMinus(moneyOut);
                } else if (bankAccount.getVisaBalance()< moneyOut){
                    System.out.println("Jūsų pageidaujama pinigų suma virsija balansą.");
                    //System.out.println("Jūsų balansas: " + bankAccount.getBalance());
                    bankAccount.getVisaBalance();
                }


            }


        }



         */













        System.out.println("==================================================");
        System.out.println("=  ********  **   **  ******   *****     *****   =");
        System.out.println("=  ********  **   **  ******   ******   *******  =");
        System.out.println("=     **     **   **  **  **   **  **   **   **  =");
        System.out.println("=     **     **   **  ******   *****    **   **  =");
        System.out.println("=     **     **   **  ****     ******   **   **  =");
        System.out.println("=     **     **   **  ****     **  **   **   **  =");
        System.out.println("=     **     *******  **  **   ******   *******  =");
        System.out.println("=     **     *******  **  **   *****     *****   =");
        System.out.println("==================================================");
        System.out.println("==================== BANKAS ======================");
        //*************************************************************************************************************
        } //----- MAIN METODO PABAIGA -----//

        //----- METODAI -----//

        public static boolean checkPinCode (int userEnterPinCode) {
            if (passwordPatter.matcher(Integer.toString(userEnterPinCode)).matches()) {
                System.out.println("Įvestas teisingas PIN kodas");
                return true;
            } else {
                //System.out.println("Įvestas PIN kodas neteisingas. Įveskite iš 4 skaitmenų");
                return false;
            }
        }

        public static boolean checkBankAccNr (String userEnterBankAccNr) {
            if (bankAccountPatter.matcher(userEnterBankAccNr).matches()){
                System.out.println("**** SASKAITOS NUMERIS TEISINGA ****");
                return true;
            } else {
                System.out.println("**** SĄSKAITOS NUMERIS NETEISINGAS ****");
                return false;
            }
        }

        public static long timeStampInfo(){
            long time = date.getTime();
            Timestamp timestamp = new Timestamp(time);
            System.out.println(timestamp);
            return time;
        }



    }

