package com.upondev.JavaFundamentals.TurboBank;


public class MasterCard {
    // MASRTERCARD //
    private String masterCardAccountNumber;
    private double masterCardBalance;

    //------KONSTRUKTORIAI-------
    public MasterCard(String masterCardAccountNumber, double masterCardBalance){
        this.masterCardAccountNumber = masterCardAccountNumber;
        this.masterCardBalance = masterCardBalance;
    }

    //------METODAI--------
    public void masterCardMoneyPlus(double plus){
        this.masterCardBalance = this.masterCardBalance + plus;
        System.out.print("Jūsų sąskaitos balansas: " + this.masterCardBalance);
        System.out.println("");
    }

    public void masterCardVisaMoneyMinus (double minus){
        this.masterCardBalance = this.masterCardBalance - minus;
        System.out.print("Jūsų sąskaitos balandas: " + this.masterCardBalance);
        System.out.println("");
    }

    public double getMasterCardBalance(){
        System.out.println("Jūsų sąskaitos balansas: " + this.masterCardBalance);
        //System.out.println(this.balance);
        return this.masterCardBalance;
    }

    public String getMasterCardAccountNumber(){
        System.out.println("Jūsų sąskaitos numeris: " + this.masterCardAccountNumber);
        return this.masterCardAccountNumber;
    }















}
