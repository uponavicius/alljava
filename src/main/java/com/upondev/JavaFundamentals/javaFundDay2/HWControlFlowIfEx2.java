package src.JavaFundamentals.javaFundDay2;

import java.util.Scanner;

public class HWControlFlowIfEx2 {
   public static void main(String[] args) {
       //2.	Pick from the console a value from 0 to 5. On the basis of the obtained value, display any sign.
       // For example, for number 0, display "*", for 1 display "$" (or any other).
        Scanner scan = new Scanner(System.in);
        System.out.println("Please enter value from 0 to 5 ");
        int numberValue = scan.nextInt();
        if (numberValue==0){
            System.out.println("$");
        }else if (numberValue==1){
            System.out.println("*");
        }else if (numberValue==2){
            System.out.println("**");
        }else if (numberValue==3){
            System.out.println("***");
        }else if (numberValue==4){
            System.out.println("****");
        }else if (numberValue==5){
            System.out.println("*****");
        }else
            System.out.println("Incorrect value");

    }


}
