package src.JavaFundamentals.javaFundDay2;

import java.util.Scanner;

public class HWForLoopEx3 {
    public static void main(String[] args) {
        //1.	The same as above , but index should be printed from the biggest value (5 included ) to the smallest one.
        Scanner textIn = new Scanner(System.in);
        System.out.println("How many times print Your name");
        int times = textIn.nextInt();
        int reverstimes = times+1;

        for (int x = 0; times > x;x++){
             reverstimes = times - x;
            System.out.println("Vladas: " + reverstimes);
        }
    }
}
