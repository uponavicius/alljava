package src.JavaFundamentals.javaFundDay2;

import java.util.Scanner;

public class HWDoWhileEx4 {
    public static void main(String[] args) {
        //4.	Within a loop read text from console and print it back (simple „echo”).
        Scanner textIn = new Scanner(System.in);
        System.out.println("Write something");
        String textOut = textIn.nextLine();
        int i = 1;

        do {
            System.out.println(textOut);
            i--;
        } while (i > 0);
    }
}
