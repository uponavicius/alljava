package com.upondev.JavaFundamentals.javaFundDay2;

public class NESUPRANTUHWForLoopEx5 {
    public static void main(String[] args) {
        //1.	*Create nested for loop . Print actual values of the iterators .
        //E.g.:
        //i=5 : j=0
        //i=5 : j=1
        //i=5 : j=2
        //…
        for (int i = 1; i <= 5; ) {
            // System.out.println("Outer loop iteration " + i);
            for (int j = 1; ; j++) {
                System.out.println("i = 5" + "; j = " + j);
            }
        }
    }
}
