package src.JavaFundamentals.javaFundDay2;

import java.util.Scanner;

public class HWWhileLoopEx1 {
    public static void main(String[] args) {
        //1.	Print your name 5 times
        Scanner scan = new Scanner(System.in);
        System.out.println("Please insert number, how many time You want print name Vladas");
        int numerPrintName = scan.nextInt();


        while (numerPrintName > 0){
            System.out.println("Vladas");
            numerPrintName--;
        }
    }
}
