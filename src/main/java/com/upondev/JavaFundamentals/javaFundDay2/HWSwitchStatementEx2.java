package src.JavaFundamentals.javaFundDay2;

import java.util.Scanner;

public class HWSwitchStatementEx2 {
    public static void main(String[] args) {
        //2.	Pick from the console a value from 0 to 5 . On the basis of the obtained value, display any sign.
        // For example, for number 0, display "*", for 1 display "$" (or any other).
        Scanner scan = new Scanner(System.in);
        System.out.println("Insert number from 0 to 5");
        int numerValue = scan.nextInt();

        switch (numerValue){
            case 0:
                System.out.println("*");
                break;
            case 1:
                System.out.println("$");
                break;
            case 2:
                System.out.println("#");
                break;
            case 3:
                System.out.println("@");
                break;
            case 4:
                System.out.println("^");
                break;
            case 5:
                System.out.println("&");
                break;
            default:
                System.out.println("Bad symbol");

        }

    }
}
