package src.JavaFundamentals.javaFundDay2;

import java.util.Scanner;

public class HWControlFlowIfEx1 {
    public static void main(String[] args) {
        //1.	Modify the sample application so that the retrieved number age comes from the console.
        // Verify the application for each case (number smaller, equal to or greater than… )
        Scanner scan = new Scanner(System.in);
        System.out.println("Please enter Your age ");
        int age = scan.nextInt();
        if (age<18){
            System.out.println("Maziau uz 18");
        } else if (age > 50){
            System.out.println("Virs 50");
        } else {
            System.out.println("tarp 18 ir 50");
        }



        //System.out.println("From console: " + something);
    }
}

