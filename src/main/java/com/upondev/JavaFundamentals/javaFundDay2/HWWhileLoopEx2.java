package src.JavaFundamentals.javaFundDay2;

public class HWWhileLoopEx2 {
    public static void main(String[] args) {
        //2.	Create while loop that will never execute
        int i = 1;
        while (i >1){
            System.out.println("i"); //i=1, 1 nera daugiau uz 1
            i--;
        }
    }
}
