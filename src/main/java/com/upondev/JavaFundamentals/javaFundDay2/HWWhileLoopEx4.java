package com.upondev.JavaFundamentals.javaFundDay2;

import java.util.Scanner;

public class HWWhileLoopEx4 {
    public static void main(String[] args) {
        //4.	Within a loop read text from console and print it back (simple „echo”)
        Scanner scan = new Scanner(System.in);
        System.out.println("Write sometring");
        String text = scan.nextLine();
        int i = 1;

        while (i > 0){
            System.out.println(text);
            i--;
        }
    }
}
