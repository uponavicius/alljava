package src.JavaFundamentals.javaFundDay2;

import java.util.Scanner;

public class HWSwitchStatementEx3 {
    public static void main(String[] args) {
        //3.	As above, but instead of values, operate on strings.
        // E.g. for the word "star", display "*".
        Scanner scan = new Scanner(System.in);
        System.out.println("Please insert symbol *, -, +, #, @");
        String symbol = scan.nextLine();

        switch (symbol){
            case "*":
                System.out.println("Star");
                break;
            case "-":
                System.out.println("Minus");
                break;
            case "+":
                System.out.println("Plus");
                break;
            case "#":
                System.out.println("Hatch tag");
                break;
            case "@":
                System.out.println("Eta");
            default:
                System.out.println("Nice job ;)");
        }
    }
}
