package src.JavaFundamentals.javaFundDay2;

public class HWArraysEx1 {
    public static void main(String[] args) {
        //1.	Create int array with the specified size. Fill it with different values.
        // Print all values to the console using enhanced for loop
        int[] arrayOfInt = new int[5];
        arrayOfInt [0] = 10;
        arrayOfInt [1] = 15;
        arrayOfInt [2] = 20;
        arrayOfInt [3] = 25;
        arrayOfInt [4] = 30;
        int print  = arrayOfInt.length;

        for (int i = 0; print > i; i++){
            System.out.println(arrayOfInt[i]);
        }

    }
}
