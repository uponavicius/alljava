package src.JavaFundamentals.javaFundDay2;

import java.util.Scanner;

public class HWArraysEx2 {
    public static void main(String[] args) {
        //2.	The same as above, but array size should come from user

        Scanner scanArraySize = new Scanner(System.in); //ivedu 5
        System.out.println("Write integer number: ");
        int arraySize = scanArraySize.nextInt(); //5
        int[] arrayOfInt = new int[arraySize]; //sukuria 5 masyva

        System.out.println("Array of: " + arraySize);
        for (int i = 0; (arraySize - i) > 0; i++){
            System.out.println(arrayOfInt[i]);
        }




    }
}
