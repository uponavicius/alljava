package src.JavaFundamentals.javaFundDay2;

import java.util.Scanner;

public class HWArraysEx3Ex4 {
    public static void main(String[] args) {
        //3.	The same as above, but values should also come from user

        Scanner scanArraySize = new Scanner(System.in); //Masyvo dydi
        System.out.println("Write integer number: ");
        int arraySize = scanArraySize.nextInt(); //pvz ivede 5, tamasyvo dydis 5
        int[] arrayOfInt = new int[arraySize]; //sukuria 5 masyva


        for (int i = 0; (arraySize - i) > 0; i++){
            Scanner arrayValues = new Scanner(System.in);
            System.out.println("Write array " + i + " value" );
            arrayOfInt[i] = arrayValues.nextInt();
            //System.out.println(arrayOfInt[i]);
         }
        int sum = 0;
        System.out.println("Array of: " + arraySize);
        for (int x = 0; (arraySize - x) > 0; x++){
            System.out.println(arrayOfInt[x]);
            sum = sum + arrayOfInt[x];
        }
        // 4.	Print sum of all of the values from your array
        System.out.println("Sum of array: ");
        System.out.println(sum);

    }
}
