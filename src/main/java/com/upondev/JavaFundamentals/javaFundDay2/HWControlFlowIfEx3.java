package src.JavaFundamentals.javaFundDay2;

import java.util.Scanner;

public class HWControlFlowIfEx3 {
    public static void main(String[] args) {
        //3.	As above, but instead of values, operate on strings.
        //E.g. for the word "star", display "*".
        Scanner scan = new Scanner(System.in);
        System.out.println("Plase enter one of this symbol *, +, -, $ or %");
        String symbol = scan.nextLine();

        if (symbol.equals("*")){
            System.out.println("Star");
        } else if (symbol.equals("+")){
            System.out.println("Plus");
        }else if (symbol.equals("-")){
            System.out.println("Minus");
        }else if (symbol.equals("$")){
            System.out.println("Dollar");
        }else if (symbol.equals("%")){
            System.out.println("Percent");
        }



    }
}
