package src.JavaFundamentals.javaFundDay2;

public class HWForLoopEx4 {
    public static void main(String[] args) {
        //1.	Calculate sum of index value from 10 to 30, using for loop (rezultatas 420)
        int indexSum = 0;
        for (int x = 10; x<31;x++){
            indexSum = indexSum + x;
        }
        System.out.println("Sum of index value from 10 to 30" + indexSum);
    }
}
