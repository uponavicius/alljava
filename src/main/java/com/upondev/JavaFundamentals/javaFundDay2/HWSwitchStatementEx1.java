package src.JavaFundamentals.javaFundDay2;

import java.util.Scanner;

public class HWSwitchStatementEx1 {
    public static void main(String[] args) {
        //1.	Modify the sample application so that the retrieved direction comes from the console.
        // Verify the application for each case ( ‚e’, w' ...).
        Scanner scan = new Scanner(System.in);
        System.out.println("Please enter Your direction ");
         char direction = scan.next().charAt(0);

        switch (direction) {
            case 'n':
                System. out . println("You are going Noth!");
                break;
            case 's':
                System. out . println("You are going South!");
                break;
            case 'e':
                System. out . println("You are going East!");
                break;
            case 'w':
                System. out . println("You are going West!");
                break;
            default:
                System. out . println("Bad direction!");


        }

    }
}
