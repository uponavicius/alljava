package com.upondev.JavaFundamentals.javaFundDay2;

public class LoopDoWhile {
    public static void main(String[] args) {
        int x = 5;
        int y = 5;

      while (x > 2){
          System.out.println("While Loop");
          x--;
      }

      do {
          System.out.println("Do while Loop");
          y--;
      } while (y > 2);
    }
}
