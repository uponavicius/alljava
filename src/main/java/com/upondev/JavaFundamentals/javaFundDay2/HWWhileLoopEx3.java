package com.upondev.JavaFundamentals.javaFundDay2;

public class HWWhileLoopEx3 {
    public static void main(String[] args) {
        //3.	Create while loop that will print the same value  to the console, as long, as application will be active
        int i = 1;
        while (i > 0){
            System.out.println("Gooooo....");
            i++;
        }
    }
}
