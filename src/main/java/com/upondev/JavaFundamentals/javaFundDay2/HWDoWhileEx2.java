package src.JavaFundamentals.javaFundDay2;

public class HWDoWhileEx2 {
    public static void main(String[] args) {
        //2.	Create do while loop that will execute only once

        do {
            System.out.println ("Endless loop...");
        } while (true);

    }
}
