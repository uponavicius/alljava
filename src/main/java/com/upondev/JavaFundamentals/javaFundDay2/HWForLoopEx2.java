package src.JavaFundamentals.javaFundDay2;

import java.util.Scanner;

public class HWForLoopEx2 {
    public static void main(String[] args) {
        //1.	The same as above , but your application should also print the actual value of the index.
        //Output:
        //Mike: 0
        //Mike: 1
        //…
        //Mike: 4
        Scanner scan = new Scanner(System.in);
        System.out.println("How many times print Your name");
        int timesPrintName = scan.nextInt();

        for (int x = 0; timesPrintName > x; x++){
            int index = x+1;
            System.out.println("Vladas: " + index );
        }
    }
}
