package src.JavaFundamentals.javaFundDay2;

import java.util.Scanner;

public class HWForLoopEx1 {
    public static void main(String[] args) {
        //1.	Print your name 5 times
        Scanner scan = new Scanner(System.in);
        System.out.println("How many times print Your name");
        int timesPrintName = scan.nextInt();

        for (int x = 0; timesPrintName > x; x++){
            System.out.println("Vladas");
        }
    }
}
