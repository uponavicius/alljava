package com.upondev.JavaFundamentals.javaFundDay2;

import java.util.Scanner;

public class HWWhileLoopEx5 {
    //5.	Within a loop read text from console and print it backwards
    public static void main(String[] args) {
        Scanner scan=new Scanner(System.in);
        System.out.print("Write sometging: ");

        String textInsert = scan.nextLine();
        int i=textInsert.length();

        while(i>0)
        {
            System.out.print(textInsert.charAt(i-1));
            i--;
        }

    }
}
