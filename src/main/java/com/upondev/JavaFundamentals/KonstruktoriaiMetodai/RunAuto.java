package com.upondev.JavaFundamentals.KonstruktoriaiMetodai;

import java.util.Scanner;

public class RunAuto {
    public static void main(String[] args) { //nuo cia paleidziama programa

        Scanner markeIn = new Scanner(System.in);
        System.out.println("Iveskite automoblio gamintoja");
        String automarke = markeIn.nextLine();

        Scanner modelisIn = new Scanner(System.in);
        System.out.println("Iveskite automobilio modeli");
        String autoModelis = markeIn.nextLine();


        Scanner gamybosMetaiIn = new Scanner(System.in);
        System.out.println("Iveskite automobilio gamybos metus");
        int autoGamybosMetai = gamybosMetaiIn.nextInt();

        Scanner autoRidaIn = new Scanner(System.in);
        System.out.println("Iveskite automobilio rida");
        int autoRida = autoRidaIn.nextInt();

        Scanner kebuloTipasIn = new Scanner(System.in);
        System.out.println("Iveskite kebulo tipa");
        String autoKebuloTipas = kebuloTipasIn.nextLine();

        Scanner spalvaIn = new Scanner(System.in);
        System.out.println("Iveskite automobilio spalva");
        String autoSpalva = spalvaIn.nextLine();

        Scanner variklioTurisIn = new Scanner(System.in);
        System.out.println("Iveskite aumobilio varklio turi");
        double autoVariklioTuris = variklioTurisIn.nextDouble();

        Scanner kwIn = new Scanner(System.in);
        System.out.println("Iveskite kiek variklis turi KW");
        int autoKw = kwIn.nextInt();

        Scanner degaluTipasIn = new Scanner(System.in);
        System.out.println("Iveskite koks automobilio degalu tipas");
        String autoDegaluTipas = degaluTipasIn.nextLine();

        Scanner pavareuDezeIn = new Scanner(System.in);
        System.out.println("Iveskite automobilio pavazu dezes tipa");
        String autoPavaruDeze = pavareuDezeIn.nextLine();

        Scanner ratuDydisIn = new Scanner(System.in);
        System.out.println("Iveskite ratu dydi");
        int autoRatuDydis = ratuDydisIn.nextInt();



        KonstruktoriaiMetodaiInfoAuto auto = new KonstruktoriaiMetodaiInfoAuto(automarke,
                autoModelis, autoGamybosMetai, autoRida,
                autoKebuloTipas, autoSpalva, autoVariklioTuris,
                autoKw, autoDegaluTipas, autoPavaruDeze, autoRatuDydis);
        //Sukurtas objektas auto, kuris suristas su KonstruktoriaiMetodaiInfoAuto klase


        //panaudojami metodai is klases KonstruktoriaiMetodaiInfoAuto
        auto.fullautoInfo();
        auto.galiaHP();
        auto.galingasAutomobilis();
        auto.ekologiskumas();


    }
}
