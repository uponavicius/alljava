package com.upondev.JavaFundamentals.KonstruktoriaiMetodai;

public class SalaryData {
    protected double salaryBruto;
    static double gpm = 19.1675; //nuo visos sumos, o ne 20%
    static double psd = 6.98;
    static double vsd = 12.52;
    static double pipk = 3; //papildoma imoka pensijos kaupimui

    // --------- KONSTRUKTORIAI --------//

    protected SalaryData(double salaryBruto){ //konstuktorius
        this.salaryBruto = salaryBruto;
    }


    //-------- METODAI --------- //

    protected void printSalaryBruto (){
        System.out.println("Jusu atlyginimas pries mokescius: " + salaryBruto );

    }


    protected double gpmValue (){
        double gpmValue = salaryBruto / 100 * gpm;
        double roundGpmValue = Math.round(gpmValue * 100.0) / 100.0;
        System.out.println("GPM: " + roundGpmValue);
        return roundGpmValue;
    }


    protected double psdValue (){
        double psdValue = salaryBruto / 100 * psd;
        double roundPsdValue = Math.round(psdValue * 100.0) / 100.0;
        System.out.println("PSD: " + roundPsdValue);
        return roundPsdValue;
    }

    protected double vsdValue(){
        double vsdValue = salaryBruto / 100 * vsd;
        double roundVsdValue = Math.round(vsdValue * 100.0) / 100.0;
        System.out.println("VSD: " + roundVsdValue);
        return roundVsdValue;
    }

    protected double pipkValue(){
        double pipkValue = salaryBruto / 100 * pipk;
        double roundPipkValue = Math.round(pipkValue * 100.0) / 100.0;
        System.out.println("PIPK: " + roundPipkValue);
        return roundPipkValue;
    }


    protected double salarytNeto (){
        double salaryNeto = salaryBruto -
                (salaryBruto / 100 * gpm) -
                (salaryBruto / 100 * psd) -
                (salaryBruto / 100 * vsd) -
                salaryBruto / 100 * pipk;
        double roundSalaruNeto = Math.round(salaryNeto * 100.0) / 100.0;
        System.out.println("Jusu atlyginimas po mokesciu: " + roundSalaruNeto);
        return  roundSalaruNeto;
    }

    private double salaryNetoForCalculation = salaryBruto -
            (salaryBruto / 100 * gpm) -
            (salaryBruto / 100 * psd) -
            (salaryBruto / 100 * vsd) -
            salaryBruto / 100 * pipk;

    public void changeJob (){
        if (salaryNetoForCalculation<=1000){
            System.out.println("Laikas ieskotis kito darbo...");
        } else if (salaryNetoForCalculation > 1000 & salaryNetoForCalculation<=1500){
            System.out.println("gali bandyti gyventi net ir Lietuvoje...");
        } else if (salaryNetoForCalculation>1500 & salaryNetoForCalculation <= 2500){
            System.out.println("Tik jau nereikia skustis ;)");
        }else if (salaryNetoForCalculation>2500){
            System.out.println("Parodyk kur kasi ;)");
        }

    }



}
