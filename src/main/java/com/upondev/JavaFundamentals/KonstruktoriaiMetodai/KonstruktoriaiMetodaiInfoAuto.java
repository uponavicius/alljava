package com.upondev.JavaFundamentals.KonstruktoriaiMetodai;

public class KonstruktoriaiMetodaiInfoAuto { // klase
    protected String gamintojas;            //instance. klases kintamasis
    protected String modelis;          //instance. kalses kintamasis
    protected int gamybosMetai;        //instance. klases kintamasis
    protected int rida;                //instance. klases kintamasis
    protected String kebuloTipas;      //instance. klases kintamasis
    protected String spalva;           //instance. klases kintamasis
    protected double variklioTuris;    //instance. klases kintamasis
    protected int kw;                  //instance. klases kintamasis
    protected String degaluTipas;      //instance. klases kintamasis
    protected String pavaruDeze;       //instance. klases kintamasis
    protected int ratuDydis;           //instance. klases kintamasis


    public KonstruktoriaiMetodaiInfoAuto (String marke, String modelis, int gamybosMetai,
                                          int rida, String kebuloTipas,
                                          String spalva, double variklioTuris, int kw, String degaluTipas,
                                          String pavaruDeze, int ratuDydis){ //konstruktorius. Pavadinimas turi buti toks pat kaip klases

        this.gamintojas = marke;
        this.modelis = modelis;
        this.gamybosMetai = gamybosMetai;
        this.rida = rida;
        this.kebuloTipas = kebuloTipas;
        this.spalva = spalva;
        this.variklioTuris = variklioTuris;
        this.kw = kw;
        this.degaluTipas = degaluTipas;
        this.pavaruDeze = pavaruDeze;
        this.ratuDydis = ratuDydis;
    }


    protected void fullautoInfo () { //metodas
        System.out.println("---- INFORMACIJA APIE AUTOMOBOLI----");
        System.out.println("Jusu automobilio aprasymas / konfiguracija");
        System.out.println("Automobilio marke: " + gamintojas);
        System.out.println("Automobilio modelis: " + modelis);
        System.out.println("Automobilio gamybos metai: " + gamybosMetai);
        System.out.println("Automobilio rida: " + rida);
        System.out.println("Automobilio kebulo tipas" + kebuloTipas);
        System.out.println("Automobilio slapva: " + spalva);
        System.out.println("Automobilio variklio darbinis turis: " + variklioTuris);
        System.out.println("Automobilio galia (kw): " + kw);
        System.out.println("Automobilio degalu tipas: " + degaluTipas);
        System.out.println("Automobilio pavaru dezes tipas: " + pavaruDeze);
        System.out.println("Automobilio ratu dydis: " + ratuDydis);


    }


    protected double galiaHP (){ //metodas
        double hp = kw * 1.36;
        System.out.println("Variklis turi arkliu: " + hp);
        return hp;

    }

    protected void galingasAutomobilis (){ //metodas
        if (kw<=100){
            System.out.println("Automobilis nera galingas");
        } else if (kw >100 & kw<=200){
            System.out.println("Automobilis yra galingas");
        } else if (kw>200){
            System.out.println("Automobilis yra labai galingas");
        }
    }

    protected void ekologiskumas (){
        if (degaluTipas=="benzinas" | degaluTipas=="Benzinas"){
            System.out.println("Isidek dujas, vaziuosi pigiau");
        } else if (degaluTipas=="Dyzelinas" | degaluTipas=="dyzelinas"){
            System.out.println("Visi darome klaidas, savu nuodujate. Pirkite benzina ir dekite dujas");
        } else if (degaluTipas=="hibridas" | degaluTipas=="Hibridas"){
            System.out.println("Saunuolis, taupai seimos pinigus ir rupiniesi gamta");
        } else if (degaluTipas=="Elekta" | degaluTipas=="elektra"){
            System.out.println("Saunuolis, saugai gamta!");
        }
    }

}