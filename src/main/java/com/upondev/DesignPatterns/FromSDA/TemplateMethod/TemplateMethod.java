package com.upondev.DesignPatterns.FromSDA.TemplateMethod;


import com.upondev.DesignPatterns.FromSDA.TemplateMethod.example.Capriciosa;
import com.upondev.DesignPatterns.FromSDA.TemplateMethod.example.Margharita;
import com.upondev.DesignPatterns.FromSDA.TemplateMethod.example.Pizza;

public class TemplateMethod {

    public static void main(String[] args) {
        Pizza margharita = new Margharita();
        Pizza capriciosa = new Capriciosa();
        margharita.bakingPizza();
        System.out.println("-----------------");
        capriciosa.bakingPizza();

//        System.out.println("-----------------");
//        Pizza fruttiDiMare = new FruttiDiMare();
//        fruttiDiMare.bakingPizza();
//
//        Sort bubbleSort = new BubbleSort();
//        Sort bucketSort = new BucketSort();
//        bubbleSort.sortArray(10);
//        System.out.println("-----------------");
//        bucketSort.sortArray(10);
    }

}
