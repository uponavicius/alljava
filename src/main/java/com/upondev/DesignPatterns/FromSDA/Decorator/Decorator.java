package com.upondev.DesignPatterns.FromSDA.Decorator;


import com.upondev.DesignPatterns.FromSDA.Decorator.example.BasicPizza;
import com.upondev.DesignPatterns.FromSDA.Decorator.example.HamPizza;
import com.upondev.DesignPatterns.FromSDA.Decorator.example.MushroomsPizza;
import com.upondev.DesignPatterns.FromSDA.Decorator.example.Pizza;

public class Decorator {

    public static void main(String[] args) {
        Pizza pizza = new BasicPizza();
        pizza.printIngredients();
        Pizza hamPizza = new HamPizza(pizza);
        pizza.printIngredients();
        Pizza mushroomsPizza = new MushroomsPizza(pizza);
        pizza.printIngredients();

//        Pizza seafoodPizza = new SeafoodPizza(new BasicPizza());
//        pizza.printIngredients();
    }

}