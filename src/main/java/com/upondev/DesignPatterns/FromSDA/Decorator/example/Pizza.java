package com.upondev.DesignPatterns.FromSDA.Decorator.example;

public interface Pizza {
    public void addIngredients(String ingredient);
    public void printIngredients();
}
