package com.upondev.DesignPatterns.FromSDA.Strategy.example;

public class Cash implements Payment {

    public Cash(){

    }


    public void pay(int amount) {
        System.out.println("Cost: " + amount + "$, paid whit cash");
    }

}