package com.upondev.DesignPatterns.FromSDA.Strategy.example;

public interface Payment {
    public void pay(int amount);
}