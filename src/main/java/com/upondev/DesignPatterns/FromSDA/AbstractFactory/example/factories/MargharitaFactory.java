package com.upondev.DesignPatterns.FromSDA.AbstractFactory.example.factories;

import com.upondev.DesignPatterns.FromSDA.AbstractFactory.example.pizza.Margharita;
import com.upondev.DesignPatterns.FromSDA.AbstractFactory.example.pizza.Pizza;


public class MargharitaFactory implements PizzaAbstractFactory {

    @Override
    public Pizza create(int size) {
        return new Margharita(size);
    }
}
