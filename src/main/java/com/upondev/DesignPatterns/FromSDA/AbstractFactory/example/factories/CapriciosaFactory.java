package com.upondev.DesignPatterns.FromSDA.AbstractFactory.example.factories;

import com.upondev.DesignPatterns.FromSDA.AbstractFactory.example.pizza.Capriciosa;
import com.upondev.DesignPatterns.FromSDA.AbstractFactory.example.pizza.Pizza;


public class CapriciosaFactory implements PizzaAbstractFactory {

    @Override
    public Pizza create(int size) {
        return new Capriciosa(size);
    }
}
