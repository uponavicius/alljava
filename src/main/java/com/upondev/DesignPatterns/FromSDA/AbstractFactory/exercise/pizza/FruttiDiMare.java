package com.upondev.DesignPatterns.FromSDA.AbstractFactory.exercise.pizza;

import com.upondev.DesignPatterns.FromSDA.AbstractFactory.example.pizza.Pizza;


public class FruttiDiMare extends Pizza {

    private final int size;

    public FruttiDiMare(int size) {
        this.size = size;
    }

    @Override
    public String getName() {
        return "Frutti di Mare";
    }

    @Override
    public int getSize() {
        return size;
    }

    @Override
    public String getIngredients() {
        return "Chees, Tomato Sauce, Seafood";
    }
}
