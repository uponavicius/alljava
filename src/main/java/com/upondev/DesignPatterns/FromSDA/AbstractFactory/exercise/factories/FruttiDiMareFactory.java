package com.upondev.DesignPatterns.FromSDA.AbstractFactory.exercise.factories;

import com.upondev.DesignPatterns.FromSDA.AbstractFactory.example.factories.PizzaAbstractFactory;
import com.upondev.DesignPatterns.FromSDA.AbstractFactory.example.pizza.Pizza;
import com.upondev.DesignPatterns.FromSDA.AbstractFactory.exercise.pizza.FruttiDiMare;


public class FruttiDiMareFactory implements PizzaAbstractFactory {

    @Override
    public Pizza create(int size) {
        return new FruttiDiMare(size);
    }
}
