package com.upondev.DesignPatterns.FromSDA.AbstractFactory.example.factories;


import com.upondev.DesignPatterns.FromSDA.AbstractFactory.example.pizza.Pizza;


public interface PizzaAbstractFactory {

    public Pizza create(int size);
}

