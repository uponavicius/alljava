package com.upondev.DesignPatterns.FromSDA.FluentInterface.example;

public interface Pizza {

    public Pizza getName();

    public Pizza getIngredients();

    public Integer getCost();
}
