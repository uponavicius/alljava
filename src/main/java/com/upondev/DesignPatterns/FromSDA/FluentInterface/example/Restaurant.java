package com.upondev.DesignPatterns.FromSDA.FluentInterface.example;

public interface Restaurant {

    public Restaurant name(String name);

    public Menu getMenu();
}
