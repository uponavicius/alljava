package com.upondev.DesignPatterns.FromSDA.FluentInterface;


import com.upondev.DesignPatterns.FromSDA.FluentInterface.example.RestaurantImpl;

import java.util.Arrays;

public class FluentInterface {

    public static void main(String[] args) {
        new RestaurantImpl().name("Primavera").getMenu().orderPizza(Arrays.asList(new Integer[]{1, 3})).eatPizza().payPizza();
    }

}
