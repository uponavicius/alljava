package com.upondev.DesignPatterns.FromSDA.FactoryMethod;


import com.upondev.DesignPatterns.FromSDA.FactoryMethod.example.Pizza;
import com.upondev.DesignPatterns.FromSDA.FactoryMethod.example.PizzaFactory;

public class FactoryMethod {

    public static void main(String[] args) {
        PizzaFactory factoris = new PizzaFactory();
        Pizza margharita = factoris.getPizza(1);
        Pizza capriciosa = factoris.getPizza(2);
        System.out.println(margharita.getDetectedPizza());
        System.out.println(capriciosa.getDetectedPizza());
        Pizza fruttiDiMare = factoris.getPizza(3);
        System.out.println(fruttiDiMare.getDetectedPizza());
    }

}