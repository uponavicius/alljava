package com.upondev.DesignPatterns.FromSDA.FactoryMethod.example;

public class Capriciosa implements Pizza {

    @Override
    public String getDetectedPizza() {
        return "2. Capriciosa";
    }
}
