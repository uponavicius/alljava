package com.upondev.DesignPatterns.FromSDA.FactoryMethod.example;

public class Margharita implements Pizza {

    @Override
    public String getDetectedPizza() {
        return "1. Margherita";
    }
}