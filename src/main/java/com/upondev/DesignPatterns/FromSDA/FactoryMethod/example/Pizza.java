package com.upondev.DesignPatterns.FromSDA.FactoryMethod.example;

public interface Pizza {

    String getDetectedPizza();
}