package com.upondev.DesignPatterns.FromSDA.FactoryMethod.exercise;


import com.upondev.DesignPatterns.FromSDA.FactoryMethod.example.Pizza;

public class FruttiDiMare implements Pizza {

    @Override
    public String getDetectedPizza() {
        return "3. Frutti di Mare";
    }
}
