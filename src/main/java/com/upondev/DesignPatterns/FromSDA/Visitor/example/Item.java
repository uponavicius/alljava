package com.upondev.DesignPatterns.FromSDA.Visitor.example;

public interface Item {

    public int accept(ShoppingCart visitor);
}