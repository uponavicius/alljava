package com.upondev.DesignPatterns.FromSDA.Visitor.example;

import com.upondev.DesignPatterns.FromSDA.Visitor.exercise.Animal;


public interface ShoppingCart {

    int visit(Car car);

    int visit(Computer computer);

    int visit(Animal animal);
}