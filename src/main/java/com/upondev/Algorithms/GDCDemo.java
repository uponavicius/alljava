package src.Algorithms;

public class GDCDemo {

    public static int gcdEuclid (int a, int b){

        while (a != b){
            if (a<b)
                b = b - a;
            else
                a = a - b;
        }
        return a;
    }


    public static void main(String[] args) {
        System.out.println(gcdEuclid(3,2));
        System.out.println(gcdEuclid(9,6));

    }
}
