package com.upondev.FundCoding.HomeWork;

import java.util.Scanner;




public class DrinkShop {
    /**
     * Write a simple application that will simulate shopping process. Use only if-else flow control. Consider following cases:
     * - If you would like to buy a bottle of milk – cashier will ask you for a specific amount of money.
     * You have to enter that value and verify if it is same as the cashier asked.
     * - If you would like to buy a bottle of wine – cashier will ask you if you are an adult and for positive answer ask for a specific amount of money.
     *
     * For the exercise with drinkShop():
     * - Find all place where we can ask the user to enter a value one more time, when he makes a mistake (while loop).
     * - ** Extract the logic responsible to checking the price against user's entered money value. It should be a separate method.
     */

    static double milkPrice = 1.99;
    static double winePrice = 29.99;

    public static void meniu(){
        System.out.println("Ką norite nusipirkti?");
        System.out.println("Jei norite išeiti parašykite: quit");
    }

    public static void milkPrice (){
        System.out.println("Pieno kaina: " + milkPrice + " EUR/L. Prašau sumokėti");
    }

    public static void winePrice(){
        System.out.println("Vyno kaina: " + winePrice + " EUR/L. Prašau sumokėti");
    }

    public static void operationWithMoney(double userEnterPrice, String product){
        double moneyBack;

        if (product.equalsIgnoreCase("pienas")){
            if (userEnterPrice>=milkPrice){
                moneyBack = userEnterPrice - milkPrice;
                System.out.println("Jūsų grąža: " + moneyBack + "EUR");
            } else {
                System.out.println("Dėje Jums nepakanka pinigų...");
            }
        } else if (product.equalsIgnoreCase("vynas")){


        }


    }

    public static boolean userAgeCheck(int userEnterAge){
        if (userEnterAge>=20){
            return true;

        } else {
            return false;
        }

    }

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Sveiki atvykę į pieno ir vyno parduotuvę");
        while (true){
            meniu();
            String whatToBuyAnswer = sc.next();

            if (whatToBuyAnswer.equalsIgnoreCase("quit")){
                System.out.println("Viso gero!");
                break;

            } else if(whatToBuyAnswer.equalsIgnoreCase("continue")){
                meniu();
                continue;

            } else if(whatToBuyAnswer.equalsIgnoreCase("pienas") || whatToBuyAnswer.equalsIgnoreCase("vynas")){

                if (whatToBuyAnswer.equalsIgnoreCase("pienas")){
                    milkPrice();
                    double userEnterPrice = sc.nextDouble();
                    operationWithMoney(userEnterPrice, whatToBuyAnswer);

                } else if (whatToBuyAnswer.equalsIgnoreCase("vynas")) {
                    System.out.println("Kiek Jums metų:");
                    int userEnterAge = sc.nextInt();
                    boolean age = userAgeCheck(userEnterAge);
                    if (age==true){
                        winePrice();
                        double userEnterPrice = sc.nextDouble();
                        operationWithMoney(userEnterPrice, whatToBuyAnswer);

                    } else if (age==false){
                        System.out.println("Lietuvos respublikoje alkoholinius gėrimus galima galima įsigyti nuo 20m. Gerkite pieną :D");
                    }

                }



            } else {
                System.out.println("Tokio produkto neturime: " + whatToBuyAnswer);
            }

        }




    }
}
