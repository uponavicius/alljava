package com.upondev.FundCoding.HomeWork;

public class Program {
     public static void helloWorldPrinter (){
         System.out.println("Hello World!");
     }

     public void hellowrld (){
         System.out.println("Hello World!");
     }


    public static void main(String[] args) {
        //1. Parašyti programą kuri spausdina "Hello world" nuo pradžių. Klasė bus pavadinta "Program".
         System.out.println("Hello World!");

        //2. Tada tas pats kaip #1, bet "Hello World" spausdiname iš kito metodo (galime jį pasivadinti "helloWorldPrinter()".
        helloWorldPrinter();

        //3. helloWorldPrinter() mokėti pakviesti kaip statinį kaip ne statinį metodą.
        Program HW = new Program();
        HW.hellowrld();

        //** 4. Pasirenamai: sukurti naują klasę HelloWorldPrinter, kurioje iškelsime metodą helloWorldPrinter().
        // Ir pakviesime tos klasės metodą iš pirmos klasės "Program".
        HelloWorldPrinter HelloWorldPrinter = new HelloWorldPrinter();
        HelloWorldPrinter.Hello();


    }
}
