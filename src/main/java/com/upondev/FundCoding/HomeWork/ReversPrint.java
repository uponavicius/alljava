package com.upondev.FundCoding.HomeWork;

import java.util.Scanner;

/**
 * Write a simple "echo" application, that will:
 * - print back entered string,
 * - go to the beginning of a loop if user will enter "continue",
 * - break the loop with a "good bye!" message, if user will enter "quit".
 */

public class ReversPrint {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        while (true){
            System.out.println("Parašykite ką norite atspausdinti atvirkščiai");
            System.out.println("Tęsti rašykite: continue");
            System.out.println("Išeiti rašykite: quit");
            String stringIn = scanner.nextLine();


            if (stringIn.equalsIgnoreCase("continue")){
                continue;
            } else if (stringIn.equalsIgnoreCase("quit")){
                System.out.println("good bye!");
                break;
            } else {
                //StringBuilder textInput = new StringBuilder();
                //textInput.append(stringIn);
                //textInput = textInput.reverse();
                String reverse = new StringBuilder(stringIn).reverse().toString();
                System.out.println(reverse);

            }

        }

    }

}

