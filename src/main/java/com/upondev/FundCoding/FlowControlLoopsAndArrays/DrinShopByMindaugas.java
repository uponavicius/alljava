package com.upondev.FundCoding.FlowControlLoopsAndArrays;


import java.text.DecimalFormat;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DrinShopByMindaugas {

    public static double handleCustomerPayment(Scanner sc){
        Matcher matcher;
        do {
            String customersPaidAmountStr = sc.nextLine();
            Pattern pattern = Pattern.compile("\\d{0,3}(\\.\\d{0,2})?");
            matcher = pattern.matcher(customersPaidAmountStr);
        } while(!matcher.matches());
        return Double.parseDouble(matcher.group());
    }

    public static void handleDrinkPrice(String drinkType, double price, Scanner sc) {
        System.out.println(drinkType.substring(0, 1).toUpperCase()
                + drinkType.substring(1) + " kaina: " + price + ", prašome sumokėti.");

        double customersPaidAmount = handleCustomerPayment(sc);

        while (price > customersPaidAmount) {
            System.out.println("Nepakako sumokėtų pinigų! Įdėkite trūkstamus.");
            customersPaidAmount = customersPaidAmount + handleCustomerPayment(sc);
        }

        if ((price < customersPaidAmount))
            System.out.println("Graža: " + new DecimalFormat("#.###").format((customersPaidAmount - price)));

        System.out.println("Skanaus " + drinkType + ", geros dienos!");
    }

    public static void drinkShopRefactored() {

        double milkPrice = 3.99;
        double winePrice = 6.99;
        int ageOfLegalConsumption = 20;
        Scanner sc = new Scanner(System.in);

        System.out.println("Ką norėtumėte nusipirkti (pienas / vynas)?");
        String whatToBuyAnswer = sc.nextLine();

        while (!whatToBuyAnswer.equalsIgnoreCase("pienas")
                && !whatToBuyAnswer.equalsIgnoreCase("vynas")) {
            System.out.println("Įveskite pienas arba vynas!");
            whatToBuyAnswer = sc.nextLine();
        }

        if (whatToBuyAnswer.equalsIgnoreCase("pienas")) {
            // sc.nextLine();
            handleDrinkPrice("pieno", milkPrice, sc);
        } else {
            System.out.println("Koks jūsų amžius?");
            int customersAge = sc.nextInt();
            sc.nextLine();
            if (ageOfLegalConsumption > customersAge) {
                System.out.println("Jūs per jaunas, pirkti vyną. Siūlome pirkti pieną!");
            } else {
                handleDrinkPrice("vyno", winePrice, sc);
            }
        }
    }

    public static void main(String[] args) {
     drinkShopRefactored();


    }
}
