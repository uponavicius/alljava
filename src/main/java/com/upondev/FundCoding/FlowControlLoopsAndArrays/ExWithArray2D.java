package com.upondev.FundCoding.FlowControlLoopsAndArrays;

import java.util.Arrays;

public class ExWithArray2D {

    public static void print2DArray(){
        String[][] myArr = {{"Vladas", "Upon", "222"},{"Jonas", "Simonaitis", "555"}};

        for(int i = 0; i<myArr.length; i++){
            for (int j = 0; j < myArr[i].length; j++){ //submasybo ilgis nurodomas su  [i]
                System.out.print(myArr[i][j] + " ");
            }
            System.out.println("");
        }

    }

    public static void printArrayToString(){
        String[] myArr = {"Vladas", "Uponavicius", "197"};

        System.out.println(Arrays.toString(myArr));
    }


    public static void main(String[] args) {
        //print2DArray();
        printArrayToString();
    }
}
