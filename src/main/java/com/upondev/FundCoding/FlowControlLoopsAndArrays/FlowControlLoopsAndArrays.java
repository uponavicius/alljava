package com.upondev.FundCoding.FlowControlLoopsAndArrays;


import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class FlowControlLoopsAndArrays {

    public static void printArray (){
        /**
         * remembering arrays and 2D
         */
        String[] myArr = {"Tomas", "Vladas", "Jonas"};

        for (int i = 0; i < myArr.length; i++){
            System.out.println(myArr[i]);
        }

        for (int i = 0; i < myArr.length; i+=2){
            System.out.println(myArr[i]);
        }

    }


    public static void print2DArray (){
        /**
         * remembering arrays and 2D
         */
        String[][] arr2d = {
                //   0          1          2
                { "Tomas", "Petraitis", "168" },    // 0
                { "Tadas", "Jonaitis", "179" },     // 1
                { "Mantas", "Kavarskas", "181" },   // 2
                { "Petras", "Petrarskas", "180" },  // 3
                { "Antanas", "Gražulis", "189" },   // 4
                { "Martynas", "Baisulis", "201" }   // 5
        };

        // System.out.println(arr2d[2][2]);

        int counter = 0;

        // for(int i = 0; i < arr2d.length; i++){
        //     // System.out.println(arr2d[i]); // prints just the hascode of each subarray
        //     // System.out.println(Arrays.toString(arr2d[i])); // printing the nested array
        //     for(int j = 0; j < arr2d[i].length; j++){
        //         System.out.print(arr2d[i][j] + " ");
        //         counter++;
        //     }
        //     System.out.println();
        // }


        for(int i = 0; i < arr2d.length; i++){
            // System.out.println(arr2d[i]); // prints just the hascode of each subarray
            // System.out.println(Arrays.toString(arr2d[i])); // printing the nested array
            for(int j = 0; j < arr2d[i].length; j+=2){
                System.out.print(arr2d[i][j] + " ");
                counter++;
            }
            System.out.println();
        }
        // System.out.println(counter);
    }

    public static void printSquer(int parametter){
        /**
         * Using nested for loops draw (parents loop iterator should be called “row”,
         * child – “column”):
         * - squere
         * a. triangle,
         * b. *rectangle with diagonals,
         * c. **Christmas tree
         */

        for (int row = 0; row < parametter; row++) {
            for (int column = 0; column < parametter; column++) {
                System.out.print(" *");
            }
            System.out.println();
        }
    }

    public static void printHalfSquer (int parametter){
        for (int row = 0; row < parametter; row++) {
            for (int collumn = 0; collumn <= row; collumn++) {
                System.out.print(" *");
            }
            System.out.println();
        }

    }

    public static void printPyramid(int parametter){
        /**
         * Using nested for loops draw (parents loop iterator should be called “row”,
         * child – “column”):
         * - squere
         * a. triangle,
         * b. *rectangle with diagonals,
         * c. **Christmas tree
         */

        for (int i = 1; i <= parametter; i++) {

            // loop to print the number of spaces before the star
            for (int j = parametter; j >= i; j--) {
                System.out.print(" ");
            }
            // loop to print the number of stars in each row
            for (int j = 1; j <= i; j++) {
                System.out.print("* ");
            }
            // for new line after printing each row
            System.out.println();
        }

        for (int k = 0; k < parametter; k++){
            System.out.print(" ");
        }
        System.out.println("*");

        for (int z = 0; z < parametter; z++){
            System.out.print(" ");
        }
        System.out.println("*");



    }

    public static void christmasTree (int parametter){
        /**
         * Using nested for loops draw (parents loop iterator should be called “row”,
         * child – “column”):
         * - squere
         * a. triangle,
         * b. *rectangle with diagonals,
         * c. **Christmas tree
         */

        for (int i = 1; i <= parametter; i++) {

            // loop to print the number of spaces before the star
            for (int j = parametter; j >= i; j--) {
                System.out.print(" ");
            }
            // loop to print the number of stars in each row
            for (int j = 1; j <= i; j++) {
                System.out.print("* ");
            }
            // for new line after printing each row
            System.out.println();
        }

        for (int i = 1; i <= parametter; i++) {

            // loop to print the number of spaces before the star
            for (int j = parametter; j >= i; j--) {
                System.out.print(" ");
            }
            // loop to print the number of stars in each row
            for (int j = 1; j <= i; j++) {
                System.out.print("* ");
            }
            // for new line after printing each row
            System.out.println();
        }

    }

    /**
     * Print christmas tree
     * @param size - size of the xmas tree
     * @param year - the size of trunk of the tree
     */
    private static void drawPyramindFromInternet(int size, int year){
        for (int i = 1; i <= size; i++) {
            // loop to print the number of spaces before the star
            for (int j = size; j >= i; j--) {
                System.out.print(" ");
            }
            // loop to print the number of stars in each row
            for (int j = 1; j <= i; j++) {
                System.out.print("* ");
            }
            // for new line after printing each row
            System.out.println();
        }
        for(int k = 0; k <= size - year; k++ ){
            System.out.print(" ");
        }
        for(int p = 0; p < year; p++ ){
            System.out.print("* ");
        }
        System.out.println();
        for(int z = 0; z <= size - year; z++ ){
            System.out.print(" ");
        }
        for(int x = 0; x < year; x++ ){
            System.out.print("* ");
        }
    }

    /**
     * Write a simple application that will simulate shopping process. Use only if-else flow control. Consider following cases:
     * - If you would like to buy a bottle of milk – cashier will ask you for a specific amount of money.
     * You have to enter that value and verify if it is same as the cashier asked.
     * - If you would like to buy a bottle of wine – cashier will ask you if you are an adult and for positive answer ask for a specific amount of money.
     */
    public static void drinkShop() {
        Scanner sc = new Scanner(System.in);

        System.out.println("Ką norėtumėte nusipirkti (pienas / vynas)?");

        String whatToBuyAnswer = sc.nextLine();

        while (!whatToBuyAnswer.equalsIgnoreCase("pienas")
                && !whatToBuyAnswer.equalsIgnoreCase("vynas")) {
            System.out.println("Įveskite pienas arba vynas!");
            whatToBuyAnswer = sc.nextLine();
        }

        if (whatToBuyAnswer.equalsIgnoreCase("pienas")) {
            double milkPrice = 3.99;
            System.out.println("Pieno kaina: " + milkPrice + ", prašome sumokėti.");
            double customersPaidAmount = sc.nextDouble();
            if (milkPrice > customersPaidAmount) {
                System.out.println("Nepakako sumokėtų pinigų!");
            } else {
                if((milkPrice < customersPaidAmount)){
                    System.out.println("Graža: " + new DecimalFormat("#.###").format((customersPaidAmount - milkPrice)));
                }
                System.out.println("Skanaus pieno, geros dienos!");
            }
        } else {
            int ageOfLegalConsumption = 20;

            System.out.println("Koks jūsų amžius?");
            int customersAge = sc.nextInt();

            if (ageOfLegalConsumption > customersAge) {
                System.out.println("Jūs per jaunas, pirkti vyną. Siūlome pirkti pieną!");
            } else {
                double winePrice = 6.99;
                System.out.println("Vyno kaina: " + winePrice + ", prašome sumokėti.");
                double customersPaidAmount = sc.nextDouble();
                if (winePrice > customersPaidAmount) {
                    System.out.println("Nepakako sumokėtų pinigų!");
                } else {
                    if((winePrice < customersPaidAmount)){
                        System.out.println("Graža: " + new DecimalFormat("#.##").format((customersPaidAmount - winePrice)));
                    }
                    System.out.println("Skanaus vyno, geros dienos!");
                }
            }
        }
    }

    /**
     * Write a “divide by” application. User should be able to enter initial value that will be divided
     * in a loop by a new value entered by a user. Division should occur as long, as entered value will be different than 0.
     * Result of division should be rounded to the fourth decimal point and printed to the console.
     */

    public static void divideBy (){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Įveskite sveika skaičių kuri norite dalinti");
        double number = scanner.nextInt();

        System.out.println("Įveskite sveiką skaičių iš kurio norite dalinti");
        double daliklis = scanner.nextInt();
        double rez;
        DecimalFormat df = new DecimalFormat("#.###");

        for (double i = number; number > 1; i++ ) {
            rez = number / daliklis;
            number = rez;
            if (rez>1) {
                //System.out.println(df.format(rez));
                System.out.println(new DecimalFormat("#.###").format(rez));
            }
        }


    }


    /**
     *Write an application that will find biggest value within array of int variables.
     * a. check your application using randomly generated array (use Random class),
     * b. check your application at least 5 times in a loop (generate random array -> print
     * array to the console -> find biggest value -> print biggest value -> manually verify results).
     */

    public static void usingRandom (){
        Random randomNumberGet = new Random();
        System.out.println(randomNumberGet.nextInt(40)); //skaicius nebus didenis nei 40
        System.out.println(randomNumberGet.nextInt());
    }



    public static void randomSizeArray(){
        //lets try initialize to random zise

        Random randomNumberGet = new Random();
        int[] intArr = new int[randomNumberGet.nextInt(20) + 1];

        for (int i = 0; i < intArr.length; i++) {
            intArr[i] = randomNumberGet.nextInt(10000);
        }
        System.out.println(Arrays.toString(intArr));
    }


    public static void randomIntMax(){

        Random randomNumberGet = new Random();
        int[] intArr = new int[randomNumberGet.nextInt(20) + 1]; //+1, kad nebutu nulinio masyvo, kad butu bent vienas narys
        int curentMax = intArr[0];

        for (int i = 1; i < intArr.length; i++) {
            intArr[i] = randomNumberGet.nextInt(10000);
            if (curentMax<intArr[i]){
                curentMax = intArr[i];
            }
        }
        System.out.println(Arrays.toString(intArr));
        System.out.println(curentMax);
    }


    public static void sumOfArr(){

        Random randomNumberGet = new Random();
        int[] intArr = new int[randomNumberGet.nextInt(20) + 1]; //+1, kad nebutu nulinio masyvo, kad butu bent vienas narys
        int sumOfArray = 0;


        for (int i = 0; i < intArr.length; i++) {
            intArr[i] = randomNumberGet.nextInt(10000);
            sumOfArray = sumOfArray + intArr[i];

        }
        System.out.println(Arrays.toString(intArr));
        System.out.println(sumOfArray);
    }

    public static void avarangeOfArr(){

        Random randomNumberGet = new Random();
        int[] intArr = new int[randomNumberGet.nextInt(5) + 1]; //+1, kad nebutu nulinio masyvo, kad butu bent vienas narys
        int sumOfArray = 0;

        for (int i = 0; i < intArr.length; i++) {
            intArr[i] = randomNumberGet.nextInt(10);
            sumOfArray = sumOfArray + intArr[i];
        }
        System.out.println(Arrays.toString(intArr));
        System.out.println(sumOfArray);
        System.out.println((double)sumOfArray/intArr.length);
    }


    public static void averageOfsalary(){
        String[][] arr2d = {
                //   0          1          2
                {"Tomas", "Petraitis", "1680"},     // 0
                {"Tadas", "Jonaitis", "1500"},      // 1
                {"Mantas", "Kavarskas", "500"},     // 2
                {"Petras", "Petrarskas", "180"},    // 3
                {"Antanas", "Gražulis", "380"},     // 4
                {"Martynas", "Baisulis", "230"}     // 5
        };

        int sumOfSalary = 0;

        for (int i = 0; i <arr2d.length ; i++) {
            for (int j = 2; j < arr2d[i].length; j+=3) {
                sumOfSalary = sumOfSalary + (Integer.parseInt(arr2d[i][j]));

            }
        }
        System.out.println("Sum of array: " + sumOfSalary + ", length: " + arr2d.length + ", avg: " + (double)sumOfSalary / arr2d.length);
    }

    public static void findByName(String name){
        String[][] arr2d = {
                //   0          1          2
                {"Tomas", "Petraitis", "1680"},    // 0
                {"Tadas", "Jonaitis", "1500"},     // 1
                {"Mantas", "Kavarskas", "500"},   // 2
                {"Tomas", "Petrarskas", "180"},  // 3
                {"Antanas", "Gražulis", "380"},   // 4
                {"Martynas", "Baisulis", "230"}   // 5
        };

        for (int i = 0; i <arr2d.length ; i++) {
            for (int j = 0; j < arr2d[i].length; j+=3) {
                if (arr2d[i][j].equalsIgnoreCase(name)){
                    System.out.println(arr2d[i][j] + " " + arr2d[i][j=+1] + " " + arr2d[i][j=+2]);
                }

            }
        }
    }
    /**
     * findByName(String name)
     */
    public static void findByNameByMindaugas(String name){
        String[][] arr2d = {
                {"Tomas", "Petraitis", "1600"},
                {"Antanas", "Baisulis", "2300"},
                {"Tadas", "Jonaitis", "1500"},
                {"Mantas", "Kavarskas", "500"},
                {"Petras", "Petrarskas", "180"},
                {"Antanas", "Gražulis", "380"},
                {"Martynas", "Baisulis", "230"}
        };

        for (int i = 0; i < arr2d.length; i++) {
            if(name.equalsIgnoreCase(arr2d[i][0])){
                System.out.println("Name: " + arr2d[i][0]
                        + ", surname: " + arr2d[i][1]
                        + ", salary: " + arr2d[i][2]);
            }
        }
    }



    public static void main(String[] args) {
        //printArray();
        //print2DArray();
        //printSquer(5);
        //printHalfSquer(10);
        //printPyramid(10);
        //christmasTree(5);
        //drawPyramindFromInternet(10, 3);
        //drinkShop();
        //divideBy();
        //usingRandom();
        //randomSizeArray();
        //randomIntMax();
        //sumOfArr();
        //avarangeOfArr();
        //avarangeOfArr2D();
        //averageOfsalary();
        //findByName("tomas");
        //findByNameByMindaugas("Antanas");

        //inicialize an array of product





    }


}
