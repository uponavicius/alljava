package com.upondev.FundCoding.tests.unittesting;

    public class Calc {
        public double skaicius1;
        public double skaicius2;

        public Calc (double skaicius1, double skaicius2) {
            this.skaicius1 = skaicius1;
            this.skaicius2 = skaicius2;
        }

        public double sudetiesVeiksmas(){
            double sudetis = skaicius1 + skaicius2;
            return sudetis;
        }

        public double atimtiesVeiksmas(){
            double atimtis = skaicius1 - skaicius2;
            return atimtis;
        }

        public double daugybosVeiksmas(){
            double daugyva = skaicius1 * skaicius2;
            return daugyva;
        }

        public double dalybosVeiksmas(){
            if(skaicius2 == 0)
            throw new ArithmeticException("/ by zero");
            double dalyba = skaicius1 / skaicius2;
            return dalyba;
        }

        public double sinusoVeiksmas() {
            double sinusas = Math.sin(skaicius1);
            return sinusas;

        }
}
