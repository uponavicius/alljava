package com.upondev.FundCoding.tests.tictactoe;

import java.util.Scanner;

public class TicTacToeDriver {
    public static void main(String[] args) {
        TicTacToe.play();
    }
}

class TicTacToe {

    private static Scanner sc = new Scanner(System.in);
    private static String winner = "";

    private static String[][] board = {
            {" _ ", " _ ", " _ "},
            {" _ ", " _ ", " _ "},
            {" _ ", " _ ", " _ "}
    };

    public static void play(){
        do {
            printBoard();
            playersMove();
            checkWinner();
            if(!winner.equalsIgnoreCase("")){
                System.out.println(winner + " won!");
                break;
            }
            computersMove();
            checkWinner();
            if(!winner.equalsIgnoreCase("")){
                System.out.println(winner + " won!");
                break;
            }
        } while (winner.equalsIgnoreCase(""));
    }

    private static void checkWinner() {
        // check rows
        for(int row = 0; row < board.length; row++){
            if(board[row][0].equalsIgnoreCase(" 0 ")
                    && board[row][1].equalsIgnoreCase(" 0 ")
                    && board[row][2].equalsIgnoreCase(" 0 "))
                winner = "You";

            if(board[row][0].equalsIgnoreCase(" x ")
                    && board[row][1].equalsIgnoreCase(" x ")
                    && board[row][2].equalsIgnoreCase(" x "))
                winner = "Computer";
        }

        // check columns

        // check diagonals

    }

    private static void computersMove() {
        boolean computerMoved = false;
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                if(board[i][j].equalsIgnoreCase(" _ ")) {
                    board[i][j] = " x ";
                    computerMoved = true;
                    break;
                }
            }
            if(computerMoved) break;
        }
    }

    private static void printBoard(){
        for (String[] row : board) {
            for (String cell : row)
                System.out.print(cell);
            System.out.println();
        }
    }

    private static void playersMove(){
        System.out.println("Your move: ");
        String playerCoodinates = sc.nextLine();
        int rowCoord = Integer.parseInt(playerCoodinates.substring(0, 1));
        int collCoord =  Integer.parseInt(playerCoodinates.substring(1, 2));
        // System.out.println("X: " + rowCoord + ", Y:" + collCoord);
        board[rowCoord][collCoord] = " 0 ";
    }
}