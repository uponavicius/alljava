package com.upondev.FundCoding.Basic;

import java.math.BigInteger;
import java.text.DecimalFormat;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Basics {

    //-----METODAI-----//
    public static void printHwInTwoSeparateLines(){
        /**
         * 1. Use System.out.print method to print the same statement in separate lines. Example:
        * Hello, World!
        * Hello, World!
         */

        // 0. Using escape sequence \n
        // System.out.print("Hello world!\nHello world!\n");
        // System.out.println("Hello world!\nHello world!");

        // 1.
        // for (int i = 0; i <= 1; i++){
        //     System.out.print("Hello world!\n");
        // }

        // i++ in more depth
        // int i = 1;
        // i++; // unary post-increment operator
        // i = i + 1;
        // i += 1;
        // System.out.println(i);

        // let's compare unary pre-increment and unary post-increment operator
        // int j = 1;
        // System.out.println(j);
        // // System.out.println(++j); // unary pre-increment
        // // System.out.println(j++); // unary post-increment

        // 2. while
        int i = 0;
        while (i < 2){
            System.out.println("Hello World!");
            i++;
        }
    }

    public static void allignTectToRight(){
        /**
         * 3. Display any three strings of characters on one line
         * so that they are aligned to the right edge of the 15-character blocks.
         * How to align strings to the left edge?
         */
        String[] stringArr = {"AAAA", "BBBBB", "C"};
        for (int i = 0; i < stringArr.length; i++){
            System.out.printf("%15s\n", stringArr[i]);
        }
    }

    public static void printTable(){
        /**
         * destytojo uzdavinys (Mindaugo)
         * Print a table | Name | Surname | Salary |
         * Each collumn has to be 20 characters wide.
         * Use the data provided
         */
        String[] data = {
                "Mindaugas", "Bernatavičius", "100",
                "Ramūnas", "Karbauskis", "99"};

        String marker = "\n-----------------------------------------------------------------";

        System.out.println(marker);
        System.out.printf("|%20s|%20s|%20s|", "Vardas", "Pavarde", "Atlyginimas");
//        System.out.print("|");
//        System.out.printf("%20s|", "Vardas");
//        System.out.printf("%20s|", "Pavardė");
//        System.out.printf("%20s|", "Atlyginimas");
        System.out.println(marker);


        for (int i = 0; i < data.length; i+=3){
//            System.out.printf("%20s|", data[i]);
//            System.out.printf("%20s|", data[i+1]);
//            System.out.printf("%20s|", data[i+2]);
//            System.out.println("");

            // ... all in one line
            System.out.printf("|%20s|%20s|%20s|\n", data[i], data[i+1], data[i+2]);

        }


    }

    public static void myPrintf(String formatSpecifier, String varToPrint){
        /**
         * Implement a function that will accept a parameter
         * in the form %-15s as a format specifier and another parameter
         * a variable to print.
         *
         * In the format specifier %-15s we say that:
         * - % will denote the start of formatting string
         * - then the minus sign (-) will specify the alignment (right/left)
         * - s/d - will specify the data type. Need to support string and int printing
         */
        Pattern pattern = Pattern.compile("%\\d+[sd]");
        Matcher m = pattern.matcher(formatSpecifier);
        System.out.println(m.matches());
        if(!m.matches()){
            System.out.println("Error in pattern!");
        }

        // Lets get the size of the block
        int blockSize = Integer.parseInt(
                m.group().substring(1, m.group().length() - 1));

        for(int i = 0; i < blockSize - varToPrint.length(); i++){
            System.out.print(" ");
        }
        System.out.println(varToPrint);
    }


    public static void myPrintf(String formatSpecifier, int varToPrint){
        /**
         * Implement a function that will accept a parameter
         * in the form %15s as a formats specification an another parameter
         * a variable to print.
         *
         * In the format specifier %-15s we say that:
         * - % denote the start of formatting starting
         * - then the minus sign (-) will specify the alignment  (fight/left)
         * - s - will specify the data type. We need to support string and int printing
         *
         */

        System.out.println(varToPrint);
    }

    public static void twoIntDevRezDoubleRoundTwo (int firstInt, int secondInt){
        /**
         * 4. Enter two values of type int. Display their division casted to the double type and rounded to the third decimal point.
         */
//        int firstInt = 13;
//        int secondInt = 9;

        //Cast integer  to double
        double rez = (double) firstInt/secondInt;
        System.out.println(new DecimalFormat("#.###").format(rez));
    }

    public static void roundDouble(){
        /**
         * 4. Enter any value with several digits after the decimal point and assign it to variable of type double.
         * Display the given value rounded to two decimal places.
         *
         */
        Scanner doubleDigit = new Scanner(System.in);
        System.out.println("Insert number, format X.XXX...");
        double firstDouble = doubleDigit.nextDouble();

        DecimalFormat df = new DecimalFormat("#.###");
        System.out.println(df.format(firstDouble));
        //System.out.println((new DecimalFormat("#.##").format(firstDouble)));
    }

    public static void sumIntMaxValue (){
        /**
         * 5. Sum two integer variables initialized with maximal values for that type.
         */

        int firstInt = Integer.MAX_VALUE;
        int secondInt = Integer.MAX_VALUE;
        long rezultLong = (long) firstInt + secondInt;
        System.out.println(rezultLong);

        long firstLong = Long.MAX_VALUE;
        long secondLong = Long.MAX_VALUE;
        long resultLong = firstLong + secondLong;
        System.out.println(resultLong);

        BigInteger firstLongB = new BigInteger("" + Long.MAX_VALUE);
        BigInteger secondLongB = new BigInteger("" + Long.MAX_VALUE);
        BigInteger bigSum = firstLongB.add(secondLongB);
        System.out.println(bigSum);


    }

    public static void threeDiferentTypes (){
        /**
         * 6. Create three variables, one for each type: float, byte and char. Enter values corresponding to
         * those types using Scanner. What values are you able to enter for each type?
         */
        Scanner reader = new Scanner(System.in);
        System.out.println("Insert letter: ");
        char oneChar = reader.next().charAt(0);
        System.out.println("Letter: " + oneChar);

        System.out.println("Insert byte");
        byte oneByte = reader.nextByte();
        System.out.println("Byte: " + oneByte);

        System.out.println("Insert float");
        float oneFloat = reader.nextFloat();
        System.out.println("Float: " + oneFloat);

    }



    public static void main(String[] args) {
//        printHwInTwoSeparateLines();
//        allignTectToRight();
//        printTable();
//        myPrintf("%15s", "AAA");
//        myPrintf("%-15d", 456);
//        twoIntDevRezDoubleRoundTwo(5, 3);
//        roundDouble();
//         sumIntMaxValue();
//        threeDiferentTypes();




    }
}