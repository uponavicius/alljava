package com.upondev.udemy.JavaDesginPatternsAndArchitecture.StructurePatterns.L5_ObserverPatternImplementingFromScratchWithMVC;

import javax.swing.SwingUtilities;

import com.upondev.udemy.JavaDesginPatternsAndArchitecture.StructurePatterns.L5_ObserverPatternImplementingFromScratchWithMVC.controller.Controller;
import com.upondev.udemy.JavaDesginPatternsAndArchitecture.StructurePatterns.L5_ObserverPatternImplementingFromScratchWithMVC.model.Model;
import com.upondev.udemy.JavaDesginPatternsAndArchitecture.StructurePatterns.L5_ObserverPatternImplementingFromScratchWithMVC.view.View;

public class Application {

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				runApp();
			}
			
		});
	}
	
	public static void runApp() {
		Model model = new Model();
		View view = new View(model);

		Controller controller = new Controller(view, model);
		
		view.setLoginListener(controller);
	}

}
