package com.upondev.udemy.JavaDesginPatternsAndArchitecture.StructurePatterns.L1_2_ModelViewControllery.controller;


import com.upondev.udemy.JavaDesginPatternsAndArchitecture.StructurePatterns.L1_2_ModelViewControllery.model.Model;
import com.upondev.udemy.JavaDesginPatternsAndArchitecture.StructurePatterns.L1_2_ModelViewControllery.view.View;

public class Controller {
	private View view;
	private Model model;
	
	public Controller(View view, Model model) {
		this.view = view;
		this.model = model;
	}
	
	
}
