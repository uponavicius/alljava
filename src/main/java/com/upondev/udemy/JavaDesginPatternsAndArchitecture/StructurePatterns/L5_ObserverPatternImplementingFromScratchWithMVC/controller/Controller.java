package com.upondev.udemy.JavaDesginPatternsAndArchitecture.StructurePatterns.L5_ObserverPatternImplementingFromScratchWithMVC.controller;

import com.upondev.udemy.JavaDesginPatternsAndArchitecture.StructurePatterns.L5_ObserverPatternImplementingFromScratchWithMVC.model.Model;
import com.upondev.udemy.JavaDesginPatternsAndArchitecture.StructurePatterns.L5_ObserverPatternImplementingFromScratchWithMVC.view.LoginFormEvent;
import com.upondev.udemy.JavaDesginPatternsAndArchitecture.StructurePatterns.L5_ObserverPatternImplementingFromScratchWithMVC.view.LoginListener;
import com.upondev.udemy.JavaDesginPatternsAndArchitecture.StructurePatterns.L5_ObserverPatternImplementingFromScratchWithMVC.view.View;

public class Controller implements LoginListener {
	private View view;
	private Model model;
	
	public Controller(View view, Model model) {
		this.view = view;
		this.model = model;
	}

	@Override
	public void loginPerformed(LoginFormEvent event) {
		System.out.println("Login event received: " + event.getName() + "; " + event.getPassword());
		
	}
	
	
}
