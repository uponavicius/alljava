package com.upondev.udemy.JavaDesginPatternsAndArchitecture.StructurePatterns.L1_2_ModelViewControllery.view;

import com.upondev.udemy.JavaDesginPatternsAndArchitecture.StructurePatterns.L1_2_ModelViewControllery.model.Model;

import javax.swing.JFrame;



public class View extends JFrame {
	
	private Model model;

	public View(Model model) {
		super("MVC Demo");
		
		this.model = model;
	}
	
	

}
