package com.upondev.udemy.JavaDesginPatternsAndArchitecture.StructurePatterns.L3_4_ObserverPatternAnExampleInvolvingButtons.controller;


import com.upondev.udemy.JavaDesginPatternsAndArchitecture.StructurePatterns.L3_4_ObserverPatternAnExampleInvolvingButtons.model.Model;
import com.upondev.udemy.JavaDesginPatternsAndArchitecture.StructurePatterns.L3_4_ObserverPatternAnExampleInvolvingButtons.view.View;

public class Controller {
	private View view;
	private Model model;
	
	public Controller(View view, Model model) {
		this.view = view;
		this.model = model;
	}
	
	
}
