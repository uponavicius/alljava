package com.upondev.udemy.JavaDesginPatternsAndArchitecture.StructurePatterns.L5_ObserverPatternImplementingFromScratchWithMVC.view;

public interface LoginListener {
	public void loginPerformed(com.upondev.udemy.JavaDesginPatternsAndArchitecture.StructurePatterns.L5_ObserverPatternImplementingFromScratchWithMVC.view.LoginFormEvent event);
}
