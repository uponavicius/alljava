package com.upondev.udemy.JavaDesginPatternsAndArchitecture.StructurePatterns.L1_2_ModelViewControllery;

import javax.swing.SwingUtilities;

import com.upondev.udemy.JavaDesginPatternsAndArchitecture.StructurePatterns.L1_2_ModelViewControllery.controller.Controller;
import com.upondev.udemy.JavaDesginPatternsAndArchitecture.StructurePatterns.L1_2_ModelViewControllery.model.Model;
import com.upondev.udemy.JavaDesginPatternsAndArchitecture.StructurePatterns.L1_2_ModelViewControllery.view.View;

public class Application {

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				runApp();
			}
			
		});
	}
	
	public static void runApp() {
		Model model = new Model();
		View view = new View(model);

		Controller controller = new Controller(view, model);
	}

}
