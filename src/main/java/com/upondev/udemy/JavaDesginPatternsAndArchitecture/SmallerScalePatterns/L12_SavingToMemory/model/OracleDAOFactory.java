package com.upondev.udemy.JavaDesginPatternsAndArchitecture.SmallerScalePatterns.L12_SavingToMemory.model;

public class OracleDAOFactory extends DAOFactory {

	@Override
	public PersonDAO getPersonDAO() {
		return new OraclePersonDAO();
	}

	@Override
	public LogDAO getLogDAO() {
		return new OracleLogDAO();
	}

}
