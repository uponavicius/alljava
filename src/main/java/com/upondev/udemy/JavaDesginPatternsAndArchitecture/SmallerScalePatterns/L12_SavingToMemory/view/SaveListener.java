package com.upondev.udemy.JavaDesginPatternsAndArchitecture.SmallerScalePatterns.L12_SavingToMemory.view;

public interface SaveListener {
	public void onSave();
}
