package com.upondev.udemy.JavaDesginPatternsAndArchitecture.SmallerScalePatterns.L11_ExtendingDAOForMultipleDatabases.controller;

import com.upondev.udemy.JavaDesginPatternsAndArchitecture.SmallerScalePatterns.L11_ExtendingDAOForMultipleDatabases.model.DAOFactory;
import com.upondev.udemy.JavaDesginPatternsAndArchitecture.SmallerScalePatterns.L11_ExtendingDAOForMultipleDatabases.model.Model;
import com.upondev.udemy.JavaDesginPatternsAndArchitecture.SmallerScalePatterns.L11_ExtendingDAOForMultipleDatabases.model.Person;
import com.upondev.udemy.JavaDesginPatternsAndArchitecture.SmallerScalePatterns.L11_ExtendingDAOForMultipleDatabases.model.PersonDAO;
import com.upondev.udemy.JavaDesginPatternsAndArchitecture.SmallerScalePatterns.L11_ExtendingDAOForMultipleDatabases.view.CreateUserEvent;
import com.upondev.udemy.JavaDesginPatternsAndArchitecture.SmallerScalePatterns.L11_ExtendingDAOForMultipleDatabases.view.CreateUserListener;
import com.upondev.udemy.JavaDesginPatternsAndArchitecture.SmallerScalePatterns.L11_ExtendingDAOForMultipleDatabases.view.View;

import java.sql.SQLException;


public class Controller implements CreateUserListener {
	private View view;
	private Model model;
	
	public Controller(View view, Model model) {
		this.view = view;
		this.model = model;
	}

	@Override
	public void userCreated(CreateUserEvent event) {
		
		DAOFactory factory = DAOFactory.getFactory(DAOFactory.MYSQL);
		
		PersonDAO personDAO = factory.getPersonDAO();
		
		System.out.println("Login event received: " + event.getName() + "; " + event.getPassword());
		
		try {
			personDAO.addPerson(new Person(event.getName(), event.getPassword()));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
}
