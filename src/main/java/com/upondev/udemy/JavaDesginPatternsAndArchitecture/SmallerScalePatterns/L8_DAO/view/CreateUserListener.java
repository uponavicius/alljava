package com.upondev.udemy.JavaDesginPatternsAndArchitecture.SmallerScalePatterns.L8_DAO.view;

public interface CreateUserListener {
	public void userCreated(CreateUserEvent event);
}
