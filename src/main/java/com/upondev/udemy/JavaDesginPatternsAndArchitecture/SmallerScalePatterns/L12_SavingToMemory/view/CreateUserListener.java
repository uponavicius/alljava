package com.upondev.udemy.JavaDesginPatternsAndArchitecture.SmallerScalePatterns.L12_SavingToMemory.view;

public interface CreateUserListener {
	public void onUserCreated(CreateUserEvent event);
}
