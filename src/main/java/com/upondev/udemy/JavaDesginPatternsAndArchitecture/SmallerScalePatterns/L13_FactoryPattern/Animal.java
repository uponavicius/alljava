package com.upondev.udemy.JavaDesginPatternsAndArchitecture.SmallerScalePatterns.L13_FactoryPattern;

public interface Animal {
	public void speak();
	public void eat();
}
