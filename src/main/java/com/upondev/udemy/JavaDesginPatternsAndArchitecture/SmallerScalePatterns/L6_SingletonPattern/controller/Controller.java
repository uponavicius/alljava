package com.upondev.udemy.JavaDesginPatternsAndArchitecture.SmallerScalePatterns.L6_SingletonPattern.controller;


import com.upondev.udemy.JavaDesginPatternsAndArchitecture.SmallerScalePatterns.L6_SingletonPattern.model.Model;
import com.upondev.udemy.JavaDesginPatternsAndArchitecture.SmallerScalePatterns.L6_SingletonPattern.view.LoginFormEvent;
import com.upondev.udemy.JavaDesginPatternsAndArchitecture.SmallerScalePatterns.L6_SingletonPattern.view.LoginListener;
import com.upondev.udemy.JavaDesginPatternsAndArchitecture.SmallerScalePatterns.L6_SingletonPattern.view.View;

public class Controller implements LoginListener {
	private View view;
	private Model model;
	
	public Controller(View view, Model model) {
		this.view = view;
		this.model = model;
	}

	@Override
	public void loginPerformed(LoginFormEvent event) {
		System.out.println("Login event received: " + event.getName() + "; " + event.getPassword());
		
	}
	
	
}
