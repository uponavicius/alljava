package com.upondev.udemy.JavaDesginPatternsAndArchitecture.SmallerScalePatterns.L7_Beans.controller;


import com.upondev.udemy.JavaDesginPatternsAndArchitecture.SmallerScalePatterns.L7_Beans.model.Model;
import com.upondev.udemy.JavaDesginPatternsAndArchitecture.SmallerScalePatterns.L7_Beans.view.LoginFormEvent;
import com.upondev.udemy.JavaDesginPatternsAndArchitecture.SmallerScalePatterns.L7_Beans.view.LoginListener;
import com.upondev.udemy.JavaDesginPatternsAndArchitecture.SmallerScalePatterns.L7_Beans.view.View;

public class Controller implements LoginListener {
	private View view;
	private Model model;
	
	public Controller(View view, Model model) {
		this.view = view;
		this.model = model;
	}

	@Override
	public void loginPerformed(LoginFormEvent event) {
		System.out.println("Login event received: " + event.getName() + "; " + event.getPassword());
		
	}
	
	
}
