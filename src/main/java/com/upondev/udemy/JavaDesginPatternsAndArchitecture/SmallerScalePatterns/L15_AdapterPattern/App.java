package com.upondev.udemy.JavaDesginPatternsAndArchitecture.SmallerScalePatterns.L15_AdapterPattern;

public class App {
    //adapter  - viena interface sulyginti su kitu interface
    public static void main(String[] args) {
        // compsition
        ConsoleLogWriter logWriter = new ConsoleLogWriter();
        Logger logger = new Logger(logWriter);
        logger.write("Hello there");

        // Inheritance
        ConsoleLogWriter logWriter2 = new ConsoleLogWriter();
        Logger logger2 = new Logger(logWriter);
        logger2.write("Hello there 2.");

    }
}
