package com.upondev.udemy.JavaDesginPatternsAndArchitecture.SmallerScalePatterns.L8_DAO.controller;

import com.upondev.udemy.JavaDesginPatternsAndArchitecture.SmallerScalePatterns.L8_DAO.model.Model;
import com.upondev.udemy.JavaDesginPatternsAndArchitecture.SmallerScalePatterns.L8_DAO.model.Person;
import com.upondev.udemy.JavaDesginPatternsAndArchitecture.SmallerScalePatterns.L8_DAO.model.PersonDAO;
import com.upondev.udemy.JavaDesginPatternsAndArchitecture.SmallerScalePatterns.L8_DAO.view.CreateUserEvent;
import com.upondev.udemy.JavaDesginPatternsAndArchitecture.SmallerScalePatterns.L8_DAO.view.CreateUserListener;
import com.upondev.udemy.JavaDesginPatternsAndArchitecture.SmallerScalePatterns.L8_DAO.view.View;

import java.sql.SQLException;



public class Controller implements CreateUserListener {
	private View view;
	private Model model;
	
	private PersonDAO personDAO = new PersonDAO();
	
	public Controller(View view, Model model) {
		this.view = view;
		this.model = model;
	}

	@Override
	public void userCreated(CreateUserEvent event) {
		System.out.println("Login event received: " + event.getName() + "; " + event.getPassword());
		
		try {
			personDAO.addPerson(new Person(event.getName(), event.getPassword()));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
}
