package com.upondev.udemy.JavaDesginPatternsAndArchitecture.SmallerScalePatterns.L6_SingletonPattern.view;

public interface LoginListener {
	public void loginPerformed(LoginFormEvent event);
}
