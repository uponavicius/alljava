package com.upondev.udemy.JavaDesginPatternsAndArchitecture.SmallerScalePatterns.L14_FacadesPattern;/*
 * Dummy implementation
 */

public class InputSystem {
	public void getInput() {
		System.out.println("Getting input ...");
	}
}
