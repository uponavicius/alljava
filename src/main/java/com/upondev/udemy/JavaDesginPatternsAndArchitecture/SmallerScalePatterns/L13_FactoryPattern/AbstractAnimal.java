package com.upondev.udemy.JavaDesginPatternsAndArchitecture.SmallerScalePatterns.L13_FactoryPattern;

public class AbstractAnimal {
	public void eat() {
		System.out.println("Chomp chomp chomp");
	}
}
