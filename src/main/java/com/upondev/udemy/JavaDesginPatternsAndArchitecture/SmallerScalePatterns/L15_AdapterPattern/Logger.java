package com.upondev.udemy.JavaDesginPatternsAndArchitecture.SmallerScalePatterns.L15_AdapterPattern;

public class Logger {

    public LogWriter logWriter;

    public Logger(LogWriter logWriter) {
        this.logWriter = logWriter;
    }

    public void write(String text) {
        logWriter.out(text);

    }
}
