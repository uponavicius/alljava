package com.upondev.udemy.JavaDesginPatternsAndArchitecture.SmallerScalePatterns.L7_Beans.view;

public interface LoginListener {
	public void loginPerformed(LoginFormEvent event);
}
