package com.upondev.udemy.JavaDesginPatternsAndArchitecture.SmallerScalePatterns.L10_JUnitBasicsTestingTheDAO.controller;

import com.upondev.udemy.JavaDesginPatternsAndArchitecture.SmallerScalePatterns.L10_JUnitBasicsTestingTheDAO.model.DAOFactory;
import com.upondev.udemy.JavaDesginPatternsAndArchitecture.SmallerScalePatterns.L10_JUnitBasicsTestingTheDAO.model.Model;
import com.upondev.udemy.JavaDesginPatternsAndArchitecture.SmallerScalePatterns.L10_JUnitBasicsTestingTheDAO.model.Person;
import com.upondev.udemy.JavaDesginPatternsAndArchitecture.SmallerScalePatterns.L10_JUnitBasicsTestingTheDAO.model.PersonDAO;
import com.upondev.udemy.JavaDesginPatternsAndArchitecture.SmallerScalePatterns.L10_JUnitBasicsTestingTheDAO.view.View;
import com.upondev.udemy.JavaDesginPatternsAndArchitecture.SmallerScalePatterns.L8_DAO.view.CreateUserEvent;
import com.upondev.udemy.JavaDesginPatternsAndArchitecture.SmallerScalePatterns.L8_DAO.view.CreateUserListener;

import java.sql.SQLException;



public class Controller implements CreateUserListener {
	private View view;
	private Model model;
	
	private PersonDAO personDAO = DAOFactory.getPersonDAO();
	
	public Controller(View view, Model model) {
		this.view = view;
		this.model = model;
	}

	@Override
	public void userCreated(CreateUserEvent event) {
		System.out.println("Login event received: " + event.getName() + "; " + event.getPassword());
		
		try {
			personDAO.addPerson(new Person(event.getName(), event.getPassword()));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
}
