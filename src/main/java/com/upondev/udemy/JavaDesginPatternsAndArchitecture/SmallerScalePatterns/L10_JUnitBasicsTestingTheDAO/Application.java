package com.upondev.udemy.JavaDesginPatternsAndArchitecture.SmallerScalePatterns.L10_JUnitBasicsTestingTheDAO;

import javax.swing.SwingUtilities;


import com.upondev.udemy.JavaDesginPatternsAndArchitecture.SmallerScalePatterns.L10_JUnitBasicsTestingTheDAO.controller.Controller;
import com.upondev.udemy.JavaDesginPatternsAndArchitecture.SmallerScalePatterns.L10_JUnitBasicsTestingTheDAO.model.Model;
import com.upondev.udemy.JavaDesginPatternsAndArchitecture.SmallerScalePatterns.L10_JUnitBasicsTestingTheDAO.view.CreateUserListener;
import com.upondev.udemy.JavaDesginPatternsAndArchitecture.SmallerScalePatterns.L10_JUnitBasicsTestingTheDAO.view.View;


public class Application {
	// https://www.baeldung.com/java-dao-pattern info about DAO pattern

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				runApp();
			}
			
		});
	}
	
	public static void runApp() {
		Model model = new Model();
		View view = new View(model);

		Controller controller = new Controller(view, model);
		
		view.setLoginListener((CreateUserListener) controller);
	}

}
