package com.upondev.udemy.JavaDesginPatternsAndArchitecture.SmallerScalePatterns.L10_JUnitBasicsTestingTheDAO.view;

public interface CreateUserListener {
	public void userCreated(CreateUserEvent event);
}
