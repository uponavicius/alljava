package com.upondev.udemy.JavaDesginPatternsAndArchitecture.SmallerScalePatterns.L11_ExtendingDAOForMultipleDatabases;

import com.upondev.udemy.JavaDesginPatternsAndArchitecture.SmallerScalePatterns.L11_ExtendingDAOForMultipleDatabases.controller.Controller;
import com.upondev.udemy.JavaDesginPatternsAndArchitecture.SmallerScalePatterns.L11_ExtendingDAOForMultipleDatabases.model.Model;
import com.upondev.udemy.JavaDesginPatternsAndArchitecture.SmallerScalePatterns.L11_ExtendingDAOForMultipleDatabases.view.View;

import javax.swing.SwingUtilities;

// https://www.baeldung.com/java-dao-pattern info about DAO pattern

public class Application {

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				runApp();
			}
			
		});
	}
	
	public static void runApp() {
		Model model = new Model();
		View view = new View(model);

		Controller controller = new Controller(view, model);
		
		view.setLoginListener(controller);
	}

}
