package com.upondev.udemy.JavaDesginPatternsAndArchitecture.SmallerScalePatterns.L15_AdapterPattern;

public interface LogWriter {
    public void out(String text);

}
