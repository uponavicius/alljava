package com.upondev.udemy.JavaDesginPatternsAndArchitecture.SmallerScalePatterns.L12_SavingToMemory.model;

import java.util.List;

public interface LogDAO {

	public void addEntry(String message);

	public List<Log> getEntries(int number);

}