package com.upondev.udemy.JavaDesginPatternsAndArchitecture.SmallerScalePatterns.L15_AdapterPattern;

public class ConsoleLogWriter implements LogWriter {
    private ConsoleWriter consoleWriter = new ConsoleWriter();


    @Override
    public void out(String text) {
    consoleWriter.writeConsole(text);
    }
}
