package com.upondev.udemy.JavaDesginPatternsAndArchitecture.SmallerScalePatterns.L8_DAO;

import com.upondev.udemy.JavaDesginPatternsAndArchitecture.SmallerScalePatterns.L8_DAO.controller.Controller;
import com.upondev.udemy.JavaDesginPatternsAndArchitecture.SmallerScalePatterns.L8_DAO.model.Model;
import com.upondev.udemy.JavaDesginPatternsAndArchitecture.SmallerScalePatterns.L8_DAO.view.View;

import javax.swing.SwingUtilities;

// https://www.baeldung.com/java-dao-pattern info about DAO pattern

public class Application {

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				runApp();
			}
			
		});
	}
	
	public static void runApp() {
		Model model = new Model();
		View view = new View(model);

		Controller controller = new Controller(view, model);
		
		view.setLoginListener(controller);
	}

}
