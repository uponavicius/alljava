package com.upondev.udemy.JavaDesginPatternsAndArchitecture.SmallerScalePatterns.L11_ExtendingDAOForMultipleDatabases.view;

public interface CreateUserListener {
	public void userCreated(CreateUserEvent event);
}
