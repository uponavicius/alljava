package com.upondev.udemy.JavaDesginPatternsAndArchitecture.SmallerScalePatterns.L6_SingletonPattern;

import javax.swing.SwingUtilities;


import com.upondev.udemy.JavaDesginPatternsAndArchitecture.SmallerScalePatterns.L6_SingletonPattern.controller.Controller;
import com.upondev.udemy.JavaDesginPatternsAndArchitecture.SmallerScalePatterns.L6_SingletonPattern.model.Model;
import com.upondev.udemy.JavaDesginPatternsAndArchitecture.SmallerScalePatterns.L6_SingletonPattern.view.View;

public class Application {

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				runApp();
			}
			
		});
	}
	
	public static void runApp() {
		Model model = new Model();
		View view = new View(model);

		Controller controller = new Controller(view, model);
		
		view.setLoginListener(controller);
	}
	

}
