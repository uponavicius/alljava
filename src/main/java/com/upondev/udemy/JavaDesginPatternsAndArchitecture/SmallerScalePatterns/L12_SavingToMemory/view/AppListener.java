package com.upondev.udemy.JavaDesginPatternsAndArchitecture.SmallerScalePatterns.L12_SavingToMemory.view;

public interface AppListener {
	public void onOpen();
	public void onClose();
}
