package com.upondev.udemy.JavaTutorialForCompletreBeginners.UdemyJavaTutorialWhatIsNewInJava8.L66_LambdaExpressions;
//https://www.udemy.com/course/java-tutorial/learn/lecture/1467284#overview

interface Executable { //funkcinis interface, nes turi tik 1 metoda
    int execute(int a, int b);
}

interface StringExecutable {
    int execute(String a);
}

class Runner {
    public void run(Executable e) {
        System.out.println("Executing code block... ");
        int value = e.execute(12, 13);
        System.out.println("Return value is: " + value);
    }

    public void run(StringExecutable e) {
        System.out.println("Executing code block... ");
        int value = e.execute("Hello");
        System.out.println("Return value is: " + value);
    }
}

// () -> System.out.println("Hello there.")

/*
() -> {
            System.out.println("This is code passed in a lambda expression");
            System.out.println("Hello there.");
        }
 */

/*
() -> {
            System.out.println("This is code passed in a lambda expression");
            System.out.println("Hello there.");
            return  8;
        }
 */

/*
() -> {
            return  8;
        }
 */

/*
() -> 8
 */

/*
(int a) ->   8
 */

/*
(int a) -> {
            System.out.println("Hello there.");
            return 7 + a;
        }
 */

/*
(a) -> {
            System.out.println("Hello there.");
            return 7 + a;
        }
 */


/*
a -> {
            System.out.println("Hello there.");
            return 7 + a;
        }
 */

/*
(a, b) -> {
            System.out.println("Hello there.");
            return a + b;
        }
 */

public class App {
    public static void main(String[] args) {
        //norint tai suprasti, raikia gerai suprasti kas yra Interface anonimines klases

        int c = 100;

        Runner runner = new Runner();
        runner.run(
                new Executable() {
                    @Override
                    public int execute(int a, int b) {
                        System.out.println("Hello there.");
                        return a + b + c;
                    }
                }
        );

        System.out.println("========================================");

        runner.run((a, b) -> {
            System.out.println("Hello there.");
            return a + b + c;
        });

        System.out.println("========================================");
        Executable ex = (a, b) -> {
            System.out.println("Hello there.");
            return a + b + c;
        };
        runner.run(ex);

        System.out.println("========================================");
        Object codeBlock = (Executable) (a, b) -> {
            System.out.println("Hello there.");
            return a + b + c;
        };
        runner.run(ex);

    }
}
