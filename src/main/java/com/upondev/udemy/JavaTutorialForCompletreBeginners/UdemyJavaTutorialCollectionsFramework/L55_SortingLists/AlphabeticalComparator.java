package com.upondev.udemy.JavaTutorialForCompletreBeginners.UdemyJavaTutorialCollectionsFramework.L55_SortingLists;

import java.util.Comparator;

public class AlphabeticalComparator implements Comparator<String> {

    @Override
    public int compare(String s1, String s2) {
        return s1.compareTo(s2);
    }
}
