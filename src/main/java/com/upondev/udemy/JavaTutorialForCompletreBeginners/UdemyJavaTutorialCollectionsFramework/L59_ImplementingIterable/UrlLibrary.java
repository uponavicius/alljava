package com.upondev.udemy.JavaTutorialForCompletreBeginners.UdemyJavaTutorialCollectionsFramework.L59_ImplementingIterable;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Spliterator;
import java.util.function.Consumer;

public class UrlLibrary implements Iterable<String> {

    private LinkedList<String> urls = new LinkedList<>();

    private class UrlIterator implements Iterable<String> {
        private int index = 0;


        @Override
        public Iterator<String> iterator() {
            return null;
        }

        @Override
        public void forEach(Consumer<? super String> action) {

        }

        @Override
        public Spliterator<String> spliterator() {
            return null;
        }

        // @Override
        public void remove() {
            urls.remove(index);
        }

        //@Override
        public boolean hasNext() {
            return index < urls.size();
        }

        // @Override
        public String next() {
            StringBuilder sb = new StringBuilder();
            try {
                URL url = new URL(urls.get(index));

                InputStream in;
                BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));

                String line = null;

                while ((line = br.readLine()) != null) {
                    sb.append(line);
                    sb.append("\n");
                }
                br.close();
            } catch (Exception e) {
                e.printStackTrace();
            }


            index++;
            return sb.toString();
        }

    }

    public UrlLibrary() {
        urls.add("http://upondev.com");
        urls.add("https://www.facebook.com/uponavicius");
        urls.add("https://www.linkedin.com/in/vladas-uponavicius/");


    }

    @Override
    public Iterator<String> iterator() {
        return (Iterator<String>) new UrlIterator();
    }

    /*
    @Override
    public Iterator<String> iterator() {
        return urls.iterator();
    }

     */
}
