package com.upondev.udemy.JavaTutorialForCompletreBeginners.UdemyJavaTutorialCollectionsFramework.L52_SortedMaps;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

public class App {
    public static void main(String[] args) {
        // skirtumai https://www.geeksforgeeks.org/differences-treemap-hashmap-linkedhashmap-java/
        // HashMap  - tvarka belekokia, jokios logikos
        // LinkedHashMap - tai tas pats, kaip „HashMap“, tvarka tokia kokia ireasei
        // TreeMap - Tai yra tas pats, kas „HashMap“, tačiau palaiko kylančiąja tvarka (Rūšiuojama pagal natūralią rakto tvarką).

        Map<Integer, String> hashMap = new HashMap<>();
        Map<Integer, String> linkedHashMap = new LinkedHashMap<>();
        Map<Integer, String> treeMap = new TreeMap<>();

        //System.out.println(new Temp()); //gausime hash koda Temp@61bbe9ba
        System.out.println("hashMap");
        testMap(hashMap);
        System.out.println("\nlinkedHashMap");
        testMap(linkedHashMap);
        System.out.println("\ntreeMap");
        testMap(treeMap);

    }

    public static void testMap(Map<Integer, String> map) {
        map.put(9, "Fox");
        map.put(4, "Cat");
        map.put(8, "Dog");
        map.put(1, "Giraffe");
        map.put(0, "Swan");
        map.put(15, "Bear");
        map.put(6, "Snake");

        for (Integer key : map.keySet()) {
            String value = map.get(key);
            System.out.println(key + ": " + value);
        }

    }
}

class Temp {

}