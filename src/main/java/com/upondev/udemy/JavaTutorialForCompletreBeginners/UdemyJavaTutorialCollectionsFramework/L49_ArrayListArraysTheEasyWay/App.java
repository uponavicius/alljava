package com.upondev.udemy.JavaTutorialForCompletreBeginners.UdemyJavaTutorialCollectionsFramework.L49_ArrayListArraysTheEasyWay;

import java.util.ArrayList;
import java.util.List;

public class App {
    public static void main(String[] args) {
        // List interface
        List<Integer> numbers = new ArrayList<>();

        // Adding
        numbers.add(10);
        numbers.add(100);
        numbers.add(40);

        // Retrieving
        System.out.println(numbers.get(0));

        // Indexed for loop interation
        System.out.println("\n Iteration #1:");
        for (int i = 0; i < numbers.size(); i++) {
            System.out.println(numbers.get(i));
        }

        System.out.println("\n Iteration #2:");
        for (Integer num : numbers) {
            System.out.println(num);
        }

        // Removing items
        numbers.remove((numbers.size() - 1));

        // Very slow. Kai trinama reikems ir priekio, tai visos kikos reiksmes perkopijuojamos po viena, kad uzpildytu tarpa.
        // jei salinti reikia is priekio, tai geriau daryti  su LinkeList
        numbers.remove(0);



    }
}
