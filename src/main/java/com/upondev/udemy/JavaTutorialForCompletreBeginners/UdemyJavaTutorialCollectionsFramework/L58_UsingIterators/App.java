package com.upondev.udemy.JavaTutorialForCompletreBeginners.UdemyJavaTutorialCollectionsFramework.L58_UsingIterators;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class App {
    public static void main(String[] args) {
        List<String> animals = new LinkedList<>();

        animals.add("Fox");
        animals.add("Cat");
        animals.add("Dog");
        animals.add("Rabbit");

        Iterator <String> it = animals.iterator();

        while (it.hasNext()) {
            String value = it.next();
            System.out.println(value);

            if (value.equalsIgnoreCase("Cat")){
                it.remove();
            }
        }

        System.out.println();


        // Modert iterations, Java 5 and later
        for (String  animal : animals ) {
            System.out.println(animal);

            //animals.remove(2); negalime taip daryti
        }

    }
}
