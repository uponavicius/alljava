package com.upondev.udemy.JavaTutorialForCompletreBeginners.UdemyJavaTutorialCollectionsFramework.L55_SortingLists;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class App {
    public static void main(String[] args) {

        // ============ Sorting Strings ===================
        List<String> animals = new ArrayList<>();

        animals.add("Cat");
        animals.add("Elephant");
        animals.add("Tiger");
        animals.add("Lion");
        animals.add("Snake");
        animals.add("Mongoose");

        Collections.sort(animals, new StringLengthComparator()); //surusiuoja pagal stringo ilgi
        Collections.sort(animals, new AlphabeticalComparator()); //rusiavimas pagal abecele
        Collections.sort(animals, new ReverseAlphabeticalComparator()); //reverts rusiavimas pagal abecele


        for (String animal : animals) {
            System.out.println(animal);
        }

        // ============ Sorting numbers ===================
        List<Integer> numbers = new ArrayList<>();
        numbers.add(3);
        numbers.add(1);
        numbers.add(36);
        numbers.add(73);
        numbers.add(40);

        //Collections.sort(numbers);
        Collections.sort(numbers, new Comparator<Integer>() { //anonimine klase
            @Override
            public int compare(Integer num1, Integer num2) { //revesinis rusiavimas
                return -num1.compareTo(num2);
            }
        });

        for (Integer num : numbers) {
            System.out.println(num);
        }

        // ============ Sorting Arbitary objects (classes) ===================

        List<Person> people = new ArrayList<>();
        people.add(new Person(1, "Joe"));
        people.add(new Person(2, "Sue"));
        people.add(new Person(3, "Bob"));
        people.add(new Person(4, "Clare"));

        // Sort in order of ID
        Collections.sort(people, new Comparator<Person>() {
            @Override
            public int compare(Person p1, Person p2) {
                if (p1.getId() > p2.getId()) {
                    return 1;
                } else if (p1.getId() < p2.getId()) {
                    return -1;
                }
                return 0;
            }
        });


        for (Person person : people) {
            System.out.println(person);
        }

        System.out.println("\n");
        // Sort in order of name
        // Sort in order of ID
        Collections.sort(people, new Comparator<Person>() {
            @Override
            public int compare(Person p1, Person p2) {

                return p1.getName().compareTo(p2.getName());
            }
        });


        for (Person person : people) {
            System.out.println(person);
        }


    }
}
