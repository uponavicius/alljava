package com.upondev.udemy.JavaTutorialForCompletreBeginners.UdemyJavaTutorialCollectionsFramework.L50_LinkedList;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class App {
    public static void main(String[] args) {
        /*
        ArrayList manage arrays interally.
        [0][1][2][3][4][5]......
        ArrayList<>() jeigu norite prideti arba pasalinti elementus is gali, nes veikai greiciau nei LinkedList
         */
        List<Integer> arrayList = new ArrayList<>();

        /*
        LinkedLists consists of elements where each element
        has a reference to the previous and next element
        [0]->[1]->[2] ...
            <-  <-

        LinkedList<>() jei norite prideti elementus arba pasalinti ir priekio, vidurio. Veikai greiciau nei ArraysList
         */
        List<Integer> linkedList = new LinkedList<>();
        doTimings("ArrayList ", arrayList);
        doTimings("LinkedList ", linkedList);


    }

    private static void doTimings(String type, List<Integer> list) {
        for (int i = 0; i < 1E5; i++) {
            list.add(i);
        }
        long star = System.currentTimeMillis();

        // // Add items at end of list
        // for (int i = 0; i <1E5 ; i++) {
        //     list.add(i);
        // }
        
        // Add items elsewhere in list
        for (int i = 0; i <1E5 ; i++) {
            list.add(0, i);
        }
        
        
        long end = System.currentTimeMillis();
        System.out.println("Time taken: " + (end - star) + " ms for " + type);
    }
}
