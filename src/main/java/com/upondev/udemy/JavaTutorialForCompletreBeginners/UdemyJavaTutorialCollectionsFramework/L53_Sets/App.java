package com.upondev.udemy.JavaTutorialForCompletreBeginners.UdemyJavaTutorialCollectionsFramework.L53_Sets;

import java.util.*;


public class App {
    public static void main(String[] args) {
        // INFORMACIJA https://docs.oracle.com/javase/8/docs/api/java/util/Set.html
        //Sets laikomi tik unikalus elementai
        //HashSet does not retain order. (Neislaiko tvarkos)
        Set<String> hashSet = new HashSet<>();

        hashSet.add("Dog");
        hashSet.add("Cat");
        hashSet.add("Mouse");
        hashSet.add("Snake");
        hashSet.add("Bear");

        // Adding duplicate items does nothing
        hashSet.add("Mouse");
        System.out.println("HashSet");
        System.out.println(hashSet);

        //laiko tvarka tokia kokia sudejai
        Set<String> linkedHashSet = new LinkedHashSet<>();
        linkedHashSet.add("Dog");
        linkedHashSet.add("Cat");
        linkedHashSet.add("Mouse");
        linkedHashSet.add("Snake");
        linkedHashSet.add("Bear");
        System.out.println("\nlinkedHashSet");
        System.out.println(linkedHashSet);

        //surusiuoja naturalia tvarka
        Set<String> treeSet = new TreeSet<>();
        if (treeSet.isEmpty()) {
            System.out.println("TreeSet is empty at start");
        }

        treeSet.add("Dog");
        treeSet.add("Cat");
        treeSet.add("Mouse");
        treeSet.add("Snake");
        treeSet.add("Bear");
        System.out.println("\ntreeSet");
        System.out.println(treeSet);

        if (treeSet.isEmpty()) {
            System.out.println("TreeSet is empty  after add (no!)");
        }

        //======== Iteration ========
        for (String element : treeSet) {
            System.out.println(element);
        }

        //======== Does set contains a give idem? ========
        if (treeSet.contains("Cat")) {
            System.out.println("Contains Cat");
        }

        // treeSet2 contains some common elements with treeSet, and some new
        Set<String> treeSet2 = new TreeSet<>();
        if (treeSet.isEmpty()) {
            System.out.println("TreeSet is empty at start");
        }

        treeSet2.add("Dog");
        treeSet2.add("Cat");
        treeSet2.add("Giraffe");
        treeSet2.add("Monkey");
        treeSet2.add("Ant");

        //======== Intersection ========
        Set<String> intersection = new HashSet<>(treeSet); //padaro kopija treeSet
        System.out.println(intersection);
        intersection.retainAll(treeSet2);
        System.out.println(intersection);

        //======== Difference ========
        Set<String> difference = new HashSet<>(treeSet2); //padaro kopija treeSet
        difference.removeAll(treeSet);
        System.out.println(difference);



    }
}
