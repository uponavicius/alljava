package com.upondev.udemy.JavaTutorialForCompletreBeginners.UdemyJavaTutorialCollectionsFramework.L57_Queues;

import java.util.NoSuchElementException;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;

public class App {
    public static void main(String[] args) {
        //Queues - eiles  https://docs.oracle.com/javase/8/docs/api/java/util/Queue.html
        //(head)<- ooooooooooooooooooooo <-(tail) FIFO (first in, first out)

        //Throws NoSuchElementException --- no element in queue yet
        // System.out.println("Head of queue is: "+ q1.element());
        Queue<Integer> q1 = new ArrayBlockingQueue<>(3);
        q1.add(10);
        q1.add(20);
        q1.add(30);

        System.out.println("Head of queue is: "+ q1.element());

        try {
            q1.add(40);
        } catch (Exception e) {
            System.out.println("Tried to add to many items to the queue");
        }
        for (Integer val : q1) {
            System.out.println("Queue value: " + val);
        }
        System.out.println("Removed from queue: " + q1.remove());
        System.out.println("Removed from queue: " + q1.remove());
        System.out.println("Removed from queue: " + q1.remove());

        try {
            System.out.println("Removed from queue: " + q1.remove());
        } catch (NoSuchElementException e) {
            System.out.println("Tried to remove too many items from queue");
        }

        //=====================================================
        Queue<Integer> q2 = new ArrayBlockingQueue<>(2);

        System.out.println("Queue 2 peek: " +q2.peek());
        q2.offer(10);
        q2.offer(20);

        System.out.println("Queue 2 peek: " +q2.peek());

        if (q1.offer(30) ==false){
            System.out.println("Offer failed to add third items");
        }


        for (Integer val : q2) {
            System.out.println("Queue 2 value: " + val);
        }

        System.out.println("Queue 2 poll: " + q2.poll());
        System.out.println("Queue 2 poll: " + q2.poll());
        System.out.println("Queue 2 poll: " + q2.poll());



    }
}
