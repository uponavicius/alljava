package com.upondev.udemy.JavaTutorialForCompletreBeginners.UdemyJavaTutorial.L35_MultipleExceptions;

import java.io.IOException;
import java.text.ParseException;

public class App {
    public static void main(String[] args) {
        Test test = new Test();

        try {
            test.run();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            System.out.println("Couldn't parse command file.");
            //e.printStackTrace();
        }

    }
}
