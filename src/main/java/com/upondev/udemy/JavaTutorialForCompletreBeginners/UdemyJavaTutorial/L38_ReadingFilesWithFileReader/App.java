package com.upondev.udemy.JavaTutorialForCompletreBeginners.UdemyJavaTutorial.L38_ReadingFilesWithFileReader;

import java.io.*;

public class App {
    public static void main(String[] args) {
        //iki Java 7
        // /Users/pro/IdeaProjects/allsdainfo/src/UdemyJavaTutorial/L38_ReadingFilesWithFileReader/Info.txt
        File file = new File("/Users/pro/IdeaProjects/allsdainfo/src/UdemyJavaTutorial/L38_ReadingFilesWithFileReader/Info.txt");
        BufferedReader bufferedReader = null;
        try {
            FileReader fileReader = new FileReader(file);
            bufferedReader = new BufferedReader(fileReader);
            String line;

            while ((line = bufferedReader.readLine()) != null) {
                System.out.println(line);
            }


        } catch (FileNotFoundException e) {
            //e.printStackTrace();
            System.out.println("File not found: " + file.toString());
        } catch (IOException e) {
            System.out.println("Unable to read file: " + file.toString());
        } finally {
            try {
                bufferedReader.close();
            } catch (IOException e) {
                System.out.println("Uanble to close file: " + file.toString());
            } catch (NullPointerException e) {
                // File was probably never opened!
            }
        }

    }
}
