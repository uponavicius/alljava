package com.upondev.udemy.JavaTutorialForCompletreBeginners.UdemyJavaTutorial.L14_Methods;

public class Person {
    // Classes can contain
    //1. Data (states) name, surname, ... Instance kintamietji
    //2. Subroutines (methods)

    //Instance kintamieji (data or "states")
    String name;
    int age;

    void speak() {
        System.out.println("Hello");
        System.out.println("My name is " + name + " and I am " + age + " years old.");
    }

    void sayHello(){

    }
}
