package com.upondev.udemy.JavaTutorialForCompletreBeginners.UdemyJavaTutorial.L15_GettersAndReturnValues;


public class App {
    public static void main(String[] args) {
        Person person = new Person();
        person.name = "John Smith";
        person.age = 34;

        person.speak();
        int years = person.calculateYearsToRetirement();
        System.out.println("Left: " + years);

        int age = person.getAge();
        System.out.println("Age is:" + age);

    }
}
