package com.upondev.udemy.JavaTutorialForCompletreBeginners.UdemyJavaTutorial.L32_AnonymousClasses;

public interface Plant {
    public void grow();
}
