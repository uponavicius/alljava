package com.upondev.udemy.JavaTutorialForCompletreBeginners.UdemyJavaTutorial.L46_SerializingArraysAndArrayList;

import java.io.Serializable;

public class Person46 implements Serializable{
    private int id;
    private String name;

    public Person46(int id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public String toString() {
        return "Peron [id=" + id + ", name=" + name + "]";
    }
}
