package com.upondev.udemy.JavaTutorialForCompletreBeginners.UdemyJavaTutorial.L22_Inheritance_Paveldejimas;

public class Machine {
    public void start(){
        System.out.println("Machine started.");
    }

    public void stop(){
        System.out.println("Machine stopped.");
    }

}
