package com.upondev.udemy.JavaTutorialForCompletreBeginners.UdemyJavaTutorial.L25_PublicPrivateProtectedAccessModifier;
/*
--kintamuju modifier
private - tik toje pacioje klaseje
public - is bet kur
protected  - tame paciame pakete
be modifier - tame paciame pakete
 */

public class App {
    public static void main(String[] args) {
        Plant plant = new Plant("Rose", "Plant","Small");
        System.out.println(plant.name);
        System.out.println(Plant.ID);


        
    }
}
