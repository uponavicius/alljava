package com.upondev.udemy.JavaTutorialForCompletreBeginners.UdemyJavaTutorial.L13_ClassesAndObjects;

public class App {
    public static void main(String[] args) {
        //programa prasideda ten kur prasideda main metodas
        Person person1 = new Person(); //kintamasis person1 su Person kintamojo tipu
        person1.name = "Joe Bloggs";
        person1.age = 34;

        Person person2 = new Person();
        person2.name = "John Smitch";
        person2.age = 20;

        System.out.println(person1.name);
    }
}
