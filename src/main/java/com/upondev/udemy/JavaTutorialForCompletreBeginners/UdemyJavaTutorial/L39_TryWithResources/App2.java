package com.upondev.udemy.JavaTutorialForCompletreBeginners.UdemyJavaTutorial.L39_TryWithResources;

import java.io.*;

public class App2 {
    public static void main(String[] args) throws FileNotFoundException {
        File file = new File("/Users/pro/IdeaProjects/allsdainfo/src/UdemyJavaTutorial/L39_TryWithResources/Info.txt");
        // FileReader fileReader = new FileReader(file);
        // BufferedReader bufferedReader = new BufferedReader(fileReader);
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(file))) {

        } catch (FileNotFoundException e) {
            System.out.println("Can't find file:" + file.toString());
        } catch (IOException e) {
            System.out.println("Unable to read file: " + file.toString());
        }


    }
}
