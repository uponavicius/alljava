package com.upondev.udemy.JavaTutorialForCompletreBeginners.UdemyJavaTutorial.L22_Inheritance_Paveldejimas;

public class App {
    public static void main(String[] args) {
        Machine mach1 = new Machine();
        mach1.start();
        mach1.stop();


        //Machine klase yra tevime klase Car klasei (lass Car extends  Machine)
        //todel Car klase paveldi visus metodus Machine klases.
        Car car1 = new Car();
        car1.start();
        car1.wipeWindShield();
        car1.stop();

    }
}
