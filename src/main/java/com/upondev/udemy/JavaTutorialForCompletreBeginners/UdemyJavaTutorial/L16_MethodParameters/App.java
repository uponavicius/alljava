package com.upondev.udemy.JavaTutorialForCompletreBeginners.UdemyJavaTutorial.L16_MethodParameters;

public class App {
    public static void main(String[] args) {
        Robot sam = new Robot();
        sam.speak("Hi I'm Sam.");

        sam.jump(7);
        sam.move("West", 3.5);

    }
}
