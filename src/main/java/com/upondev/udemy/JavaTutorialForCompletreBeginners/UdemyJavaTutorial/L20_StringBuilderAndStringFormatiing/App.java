package com.upondev.udemy.JavaTutorialForCompletreBeginners.UdemyJavaTutorial.L20_StringBuilderAndStringFormatiing;

public class App {
    public static void main(String[] args) {
        //String java yra neikintantis, viena karta sukures, jo nebepaleisti.
        //efficient (efektuvys)
        String info="";
        info +="My name is Bob.";
        info+=" ";
        info+="I am a builder";
        System.out.println(info);

        //More inefficient (efektyvesnis budas prideti texta)
        StringBuilder sb=new StringBuilder("");
        sb.append("My name is Sue.");
        sb.append(" ");
        sb.append("I am a lion tamer");
        System.out.println(sb.toString());

        StringBuilder s = new StringBuilder();
        s.append("My name is Roger")
                .append(" ")
                .append("I am a skydiver");
        System.out.println(s.toString());

        //-------------------Formatting---------------------
        System.out.println("Here is some text. \tThat ir a tab.\nThat was a newline");

        System.out.printf("Total cost %10d; Quantity is %d", 5, 120);

        //Formatting integers
        for (int i = 0; i <20 ; i++) {
            System.out.printf("%2d: some text here\n", i);
        }

        for (int i = 0; i <20 ; i++) {
            System.out.printf("%-2d: %s\n", i, "here is some text");
        }
        //Formatting floating values
        System.out.printf("Total value: %.8f\n", 5.6); //8 nurodo kiek skaiciu po kablekio ir tuo paciu apvalina
        System.out.printf("Total value: %5.1f\n", 365.59648); //5.1 - 5 nurodo kiek isviso skaiciu bus iskaitan kableli (sibloliu) o 1 nurodo kiek po kabelio
    }
}
