package com.upondev.udemy.JavaTutorialForCompletreBeginners.UdemyJavaTutorial.L25_PublicPrivateProtectedAccessModifier;

public class Field {
    Plant plant = new Plant("oo", "000", "kkk");
    public Field(){
        //protected size kintamasis yra pasiekiamas, nes yra pakete kur Plant
        System.out.println(plant.size);
    }

}
