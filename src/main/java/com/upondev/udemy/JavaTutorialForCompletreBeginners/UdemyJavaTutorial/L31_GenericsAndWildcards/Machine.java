package com.upondev.udemy.JavaTutorialForCompletreBeginners.UdemyJavaTutorial.L31_GenericsAndWildcards;

public class Machine {
    @Override
    public String toString(){
        return "I am a machine";
    }

    public void start(){
        System.out.println("Machine starting.");
    }
}
