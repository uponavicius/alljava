package com.upondev.udemy.JavaTutorialForCompletreBeginners.UdemyJavaTutorial.L47_TheTransientKeywordAndMoreSerialization;

import java.io.Serializable;

public class Person47 implements Serializable {
    private transient int id;
    private String name;
    private static int count;

    public Person47(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public static int getCount() {
        return count;
    }

    public static void setCount(int count) {
        Person47.count = count;
    }

    @Override
    public String toString() {
        return "Peron [id=" + id + ", name=" + name + "] Count is: " + count;
    }
}
