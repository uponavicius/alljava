package com.upondev.udemy.JavaTutorialForCompletreBeginners.UdemyJavaTutorial.L43_EnumTypes_BasicAndAdvancedUsage;

public class App {

    // ENUM reprezentuoja fiksuotas reiksmes.
    public static void main(String[] args) {
        Animal animal = Animal.CAT;

        switch (animal) {

            case CAT:
                System.out.println("Cat");
                break;
            case DOG:
                System.out.println("Dog");
                break;
            case MOUSE:
                System.out.println("Mouse");
                break;
            default:
                System.out.println("This animal not found");
                break;
        }

        System.out.println(Animal.DOG);
        System.out.println("Enum name as a string: " +Animal.DOG.name());
        System.out.println(Animal.DOG.getClass());
        System.out.println(Animal.MOUSE instanceof Enum);
        System.out.println(Animal.MOUSE.getName());

        Animal animal2 = Animal.valueOf("CAT");
        System.out.println(animal2);

    }
}
