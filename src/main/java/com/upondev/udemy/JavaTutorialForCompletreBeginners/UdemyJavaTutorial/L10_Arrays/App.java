package com.upondev.udemy.JavaTutorialForCompletreBeginners.UdemyJavaTutorial.L10_Arrays;

public class App {
    public static void main(String[] args) {
        int value = 7; // value (verte)
        int[] values; //values - reference (nuoroda, adresas i atmini)
        values = new int[3]; //3 vetu masyvas, bet reiksmiu dar mazybe nera pristirta

        values[0] = 10;
        values[1] = 20;
        values[2] = 30;

        for (int i = 0; i < values.length; i++) {
            System.out.println(values[i]);

        }

        int[] numbers = {5, 6, 7};
        for (int i = 0; i <numbers.length ; i++) {
            System.out.println(numbers[i]);
        }

    }
}
