package com.upondev.udemy.JavaTutorialForCompletreBeginners.UdemyJavaTutorial.L3_WorkingWithText;

public class App {
    public static void main(String[] args) {
        int myInt = 7;
        String text = "Hello"; //tai ne primityvusis tipas, o klase. Objekto tipas.
        String blank = " ";
        String name = "Bob";
        String greeting = text + blank + name;
        System.out.println(greeting);
        System.out.println("Hello" + " " + "Bob");
        System.out.println(text + blank + name);
        System.out.println("My integer is: " + myInt); //skirtingu tipu sudetis vadinama concatenation

    }
}
