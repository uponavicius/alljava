package com.upondev.udemy.JavaTutorialForCompletreBeginners.UdemyJavaTutorial.L36_RuntimeVsCheckedExceptions;

public class App {
    public static void main(String[] args) {
        /*
        Daznai uzduodamas klausimas kokie yra excepsinu tipai.
        Bazines 2 rusys - Runtime(unchecked) ir Checked Exceptions
        https://docs.oracle.com/javase/8/docs/api/java/lang/RuntimeException.html
         */
        // int value = 7;
        // value = value / 0; // Runtime Exceptions

        String[] texts = {"One", "Two", "Three"};

        try {
            System.out.println(texts[3]); // Runtime Exceptions
        } catch (Exception e){
            System.out.println(e.toString());
        }



    }
}
