package com.upondev.udemy.JavaTutorialForCompletreBeginners.UdemyJavaTutorial.L47_TheTransientKeywordAndMoreSerialization;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class WriteObjects {
    public static void main(String[] args) {
        System.out.println("Writing objects...");

        try (FileOutputStream fs = new FileOutputStream("/Users/pro/IdeaProjects/allsdainfo/src/UdemyJavaTutorial/L47_TheTransientKeywordAndMoreSerialization/people.txt");
             ObjectOutputStream os = new ObjectOutputStream(fs)) {
            //ObjectOutputStream os = new ObjectOutputStream(fs);

            Person47 person47 = new Person47(77, "Bob");
            person47.setCount(88);
            os.writeObject(person47);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
