package com.upondev.udemy.JavaTutorialForCompletreBeginners.UdemyJavaTutorial.L35_MultipleExceptions;


import java.io.IOException;

public class App3 {
    public static void main(String[] args) {
        Test test = new Test();
        try {
            test.run();
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            test.input();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
