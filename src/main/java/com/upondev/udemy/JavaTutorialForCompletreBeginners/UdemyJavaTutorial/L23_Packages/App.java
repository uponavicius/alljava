// jei klase yra pakete, tai pirma eilute klaseje visada yra paketas
//paketaai visada mazosiomis raidemis. klase camelCase

package com.upondev.udemy.JavaTutorialForCompletreBeginners.UdemyJavaTutorial.L23_Packages;


import com.upondev.udemy.JavaTutorialForCompletreBeginners.UdemyJavaTutorial.L23_Packages.ocean.Fish;
import com.upondev.udemy.JavaTutorialForCompletreBeginners.UdemyJavaTutorial.L23_Packages.ocean.Seaweed;
import com.upondev.udemy.JavaTutorialForCompletreBeginners.UdemyJavaTutorial.L23_Packages.ocean.plants.Algae;

public class App {
    public static void main(String[] args) {
        Fish fish = new Fish();
        Seaweed seaweed = new Seaweed();
        Algae algae = new Algae();

    }
}
