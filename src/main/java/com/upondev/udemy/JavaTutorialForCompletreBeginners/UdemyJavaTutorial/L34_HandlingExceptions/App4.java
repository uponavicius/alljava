package com.upondev.udemy.JavaTutorialForCompletreBeginners.UdemyJavaTutorial.L34_HandlingExceptions;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class App4 {
    public static void main(String[] args) {
        try {
            openFile();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }
    public static void openFile() throws FileNotFoundException {
        File file = new File("/Users/pro/IdeaProjects/allsdainfo/UdemyJavaTutorial/L34_HandlingExceptions/Info.txt");
        FileReader fr = new FileReader(file);
    }
}
