package com.upondev.udemy.JavaTutorialForCompletreBeginners.UdemyJavaTutorial.L6_IfStatement;

public class App {
    public static void main(String[] args) {
        int myInt = 30;

        //Eina iki pirmos salygos kuri yra teisinga. Jei pirma salyga teisinga, tai kitu nebetikrina

        if (myInt < 10) {
            System.out.println("Yes, it's true!");
        } else if (myInt > 20) {
            System.out.println("No, it's false!");
        }


        int loop = 0;

        while (loop < 5) {
            System.out.println("Lopping: " + loop);
            if (loop == 5) {
                break; //iseina is lupos
            }
            loop++;
            System.out.println("Running ");
        }
    }
}
