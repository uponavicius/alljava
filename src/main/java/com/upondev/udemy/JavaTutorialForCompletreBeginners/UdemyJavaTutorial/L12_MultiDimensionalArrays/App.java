package com.upondev.udemy.JavaTutorialForCompletreBeginners.UdemyJavaTutorial.L12_MultiDimensionalArrays;

public class    App {
public static void main(String[] args) {
    int[] values = {2, 3, 225}; // vienos dimensijos array

    int[][] grid = { //multi dimensinis arejus - array of arrays
            {3, 5, 6},
            {2,5},
            {8,9,9,8,7,8,9}
    };

    //System.out.println(grid[1][1]); //antra eilute, antra vieta

    String[][] texts = new String[2][3]; //2 eilute, 3 stulpeliai

    for (int row = 0; row <grid.length ; row++) {
        for (int col = 0; col < grid[row].length; col++) {
            System.out.print(grid[row][col] + "\t");
        }
        System.out.println();
    }

}
}
