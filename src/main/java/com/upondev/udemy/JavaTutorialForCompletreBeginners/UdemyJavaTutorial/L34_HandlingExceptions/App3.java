package com.upondev.udemy.JavaTutorialForCompletreBeginners.UdemyJavaTutorial.L34_HandlingExceptions;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class App3 {
    public static void main(String[] args) {
        openFile();
    }

    public static void openFile() {
        File file = new File("/Users/pro/IdeaProjects/allsdainfo/UdemyJavaTutorial/L34_HandlingExceptions/Info.txt");
        try {
            FileReader fr = new FileReader(file);

            System.out.println("File was found: " + file);
        } catch (FileNotFoundException e) {
            //e.printStackTrace();
            System.out.println("File not found: " + file.toString());
        }
    }
}
