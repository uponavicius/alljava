package com.upondev.udemy.JavaTutorialForCompletreBeginners.UdemyJavaTutorial.L17_SettersAndThis;

public class App {
    public static void main(String[] args) {
        Frog frog1 = new Frog();
        frog1.setName("Berties");
        frog1.setAge(1);

        System.out.println(frog1.getName());
        frog1.setName("Laura");
        System.out.println(frog1.getName());


    }
}
