package com.upondev.udemy.JavaTutorialForCompletreBeginners.UdemyJavaTutorial.L28_CastingNumericalValues;

public class App {
    public static void main(String[] args) {
        byte myteValue = 20;
        short shortVakue = 55;
        int intValue =888;
        long longValue = 23355;

        float floatValue = 8384.8f;
        double doubleValue = 32.4;

        //cast
        intValue = (int) longValue;
        System.out.println(intValue);

        doubleValue=intValue;
        System.out.println(doubleValue);

        intValue = (int) floatValue; //nukerpa po kablekio, bet neapvalina
        System.out.println(intValue);
    }
}
