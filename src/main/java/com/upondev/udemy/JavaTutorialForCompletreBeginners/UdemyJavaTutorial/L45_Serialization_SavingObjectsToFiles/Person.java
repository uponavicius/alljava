package com.upondev.udemy.JavaTutorialForCompletreBeginners.UdemyJavaTutorial.L45_Serialization_SavingObjectsToFiles;

import java.io.Serializable;

public class Person implements Serializable{
    private int id;
    private String name;

    public Person(int id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public String toString() {
        return "Peron [id=" + id + ", name=" + name + "]";
    }
}
