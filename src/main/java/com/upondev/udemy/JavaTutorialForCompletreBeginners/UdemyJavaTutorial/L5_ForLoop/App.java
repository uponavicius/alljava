package com.upondev.udemy.JavaTutorialForCompletreBeginners.UdemyJavaTutorial.L5_ForLoop;

public class App {
    public static void main(String[] args) {
        for (int i = 0; i < 5; i++) {
            System.out.println("Hello " + i);
        }
    }

    public static class L1_HelloWorld {
        public static void main(String[] args) {
            System.out.println("Hello World!");
        }
    }
}
