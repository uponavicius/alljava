package com.upondev.udemy.JavaTutorialForCompletreBeginners.UdemyJavaTutorial.L42_NestedInnerClasses;



public class App {
    public static void main(String[] args) {
        Robot robot = new Robot(7);
        robot.start();

        //jei padariu klase Publin Brain, tai tada tokia galima sintakse
        // Robot.Brain brain = robot.new Brain();
        // brain.think();

        Robot.Battery battery = new Robot.Battery();
        battery.charge();

    }
}
