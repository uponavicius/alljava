package com.upondev.udemy.JavaTutorialForCompletreBeginners.UdemyJavaTutorial.L33_ReadingFilesUsingScanner;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class App {
    public static void main(String[] args) throws FileNotFoundException {
        String fileName = "/Users/pro/IdeaProjects/allsdainfo/UdemyJavaTutorial/L33_ReadingFilesUsingScanner/Info.txt";

        File textFile = new File(fileName);
        Scanner in = new Scanner(textFile);

        while (in.hasNextLine()){
            String line = in.nextLine();
            System.out.println(line     );
        }

        in.close();


    }


}
