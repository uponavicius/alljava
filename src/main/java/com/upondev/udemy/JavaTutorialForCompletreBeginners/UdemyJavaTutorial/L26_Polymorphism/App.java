package com.upondev.udemy.JavaTutorialForCompletreBeginners.UdemyJavaTutorial.L26_Polymorphism;

public class App {
    public static void main(String[] args) {
        Plant plant1 = new Plant();
        Tree tree = new Tree();

        //Plant plant2 = plant1; // 2 refercai i viena objeta.

        //Polymorphism reiskia, kad galite naudoti child klase kur naudota tevine klase.
        Plant plant2 = tree;
        plant2.grow(); //spausdints Tree growing, nes plant2 = tree;

        tree.shedLeaves();

        Plant plant3;


    }
    public static void doGrow(Plant plant){
        plant.grow();
    }

}
