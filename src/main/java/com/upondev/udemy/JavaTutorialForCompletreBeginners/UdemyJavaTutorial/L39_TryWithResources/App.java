package com.upondev.udemy.JavaTutorialForCompletreBeginners.UdemyJavaTutorial.L39_TryWithResources;


public class App {
    public static void main(String[] args) {
        // nuo Java 7

        try (Temp temp = new Temp()) {
        } catch (Exception e) {
            e.printStackTrace();
        } ;


    }
}
