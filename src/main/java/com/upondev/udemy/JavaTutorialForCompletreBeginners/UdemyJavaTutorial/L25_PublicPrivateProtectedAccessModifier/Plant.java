package com.upondev.udemy.JavaTutorialForCompletreBeginners.UdemyJavaTutorial.L25_PublicPrivateProtectedAccessModifier;

public class Plant {
    //public galima pasiekti is bet kur, bet tai yra bloga praktika.
    //kintamuosius visadas stengiames paslepti nuo pasaulio.
    public String name;

    //final static - konstana kuri yra nekeiciama. Priimtina praktika
    public final static int ID = 3;

    //private - prieinamas/pasiekiamas tik is sios klases. Child klase irgi nemato
    //Norint pasiekti reikalingi getter ir setter
    private String type;

    //protected - pasiekimas visame pakete
    protected String size;

    //pasiekiamas tame paciame pakete
    int height;

    public Plant(String name, String type, String size) {
        this.name = name;
        this.type = type;
        this.size = size;
    }


}
