package com.upondev.udemy.JavaTutorialForCompletreBeginners.UdemyJavaTutorial.L15_GettersAndReturnValues;

public class Person {
    String name;
    int age;

    void speak() {
        System.out.println("My name is " + name);
    }

    int calculateYearsToRetirement() {
        int yearsLeft = 100 - age;
        return yearsLeft;

    }

    int getAge(){
        return age;
    }


}
