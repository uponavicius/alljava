package com.upondev.udemy.JavaTutorialForCompletreBeginners.UdemyJavaTutorial.L4_WhileLoop;

public class App {
    public static void main(String[] args) {
        int value = 0;
        // boolean loop = true;
        // boolean otherLoop = 4 < 5;
        //
        // System.out.println(loop);
        // System.out.println(otherLoop);

        while (value < 10) {
            System.out.println("Hello" + value);
            value = value + 1;

        }

    }
}
