package com.upondev.udemy.JavaTutorialForCompletreBeginners.UdemyJavaTutorial.L24_Interface;

// turime dvi ksirtingas klases Person ir Machine
// ir jeigu nusprendziate, kad abiems reikia meto showInfo()
// tokiu atveju galima panaudoti interface ir jame sukurti abstraktu metoda  public void showInfo();
// metodas yra kuraimas be {}, nes jame nieko nedarome.
// kai klasese Person ir Machine implementuosime Info interface
// tada @Override padarysime ir pasirasysime ka metodas turi daryti
// • Sąsajos negali apimti laukų.
// • Numatytieji metodai yra vieši ir abstraktūs.
// • Gali apimti statinius metodus ir laukus.
// • Klasė gali įgyvendinti daugybę sąsajų.
// • Gali būti numatytasis įgyvendinimas („Java 8“).
// • Sąsajas galima išplėsti.

public interface Info {
    public void showInfo();
}
