package com.upondev.udemy.JavaTutorialForCompletreBeginners.UdemyJavaTutorial.L7_GettingUserInput;

import java.util.Scanner;

public class App {
    public static void main(String[] args) {
        //per new kuriami objektai. Siuo atveju Scanneer objektas
        Scanner input = new Scanner(System.in);

        //textas isvedamas i konsole
        System.out.println("Enter a line of text: ");

        //nuskaitomas tekstas ka ivede vartotojas
        String line = input.nextLine();

        //isspausdinima ka ivede vartotojas
        System.out.println("You entered: " + line);



    }
}
