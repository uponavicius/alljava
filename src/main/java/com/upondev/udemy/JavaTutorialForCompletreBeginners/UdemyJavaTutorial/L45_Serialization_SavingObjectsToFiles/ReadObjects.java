package com.upondev.udemy.JavaTutorialForCompletreBeginners.UdemyJavaTutorial.L45_Serialization_SavingObjectsToFiles;

import java.io.*;

public class ReadObjects {
    public static void main(String[] args) {
        System.out.println("Reading objects...");

        File file;
        try (FileInputStream fi = new FileInputStream("/Users/pro/IdeaProjects/allsdainfo/src/UdemyJavaTutorial/L45_Serialization_SavingObjectsToFiles/people.txt")){

            ObjectInputStream  os = new ObjectInputStream(fi);

            Person person1 = (Person) os.readObject();
            Person person2 = (Person) os.readObject();
            os.close();

            System.out.println(person1);
            System.out.println(person2);


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }
}
