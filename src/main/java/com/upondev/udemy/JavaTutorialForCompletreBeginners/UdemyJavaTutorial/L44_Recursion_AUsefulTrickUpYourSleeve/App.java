package com.upondev.udemy.JavaTutorialForCompletreBeginners.UdemyJavaTutorial.L44_Recursion_AUsefulTrickUpYourSleeve;

public class App {
    public static void main(String[] args) {
        int value = 4;
        //pvz. faktorijalas 4 t.y. 4!=4*3*2*1
        System.out.println(factorial(value));


    }

    private static int factorial(int value) {
        System.out.println(value);
        if (value == 1) {
            return 1;
        }
        return factorial(value - 1) * value;
    }
}
