package com.upondev.udemy.JavaTutorialForCompletreBeginners.UdemyJavaTutorial.L29_UpcastingAndDowncasting;

public class App {
    public static void main(String[] args) {
        Machine machine1 = new Machine();
        Camera camera1 = new Camera();
        machine1.start();
        camera1.start();
        camera1.snap();

        Machine machine2 = new Camera(); //Polymorphism

        //Upcasting - saugu.
        Machine machine3 = camera1; //Polymorphism. camera1 objetui yra upcasting, nes machine3 aukstene klase
        machine3.start(); // spausdins Camera started.
        //error: machine3.snap();

        //Downcasting - yra nesaugu
        Machine machine4 = new Camera();
        Camera camera2 = (Camera) machine4;
        camera2.start();
        camera2.snap();

        //Dos't work--- run time error.
        Machine machine5 = new Machine();
        //Camera camera3=(Camera) machine5;
        //camera3.start();
        //camera3.snap();



    }
}
