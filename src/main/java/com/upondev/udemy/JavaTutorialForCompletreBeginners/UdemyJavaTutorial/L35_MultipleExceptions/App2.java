package com.upondev.udemy.JavaTutorialForCompletreBeginners.UdemyJavaTutorial.L35_MultipleExceptions;

import java.io.IOException;
import java.text.ParseException;

public class App2 {
    public static void main(String[] args) {
        Test test = new Test();

        //Multiple Exceptions
        try {
            test.run();
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }

    }
}
