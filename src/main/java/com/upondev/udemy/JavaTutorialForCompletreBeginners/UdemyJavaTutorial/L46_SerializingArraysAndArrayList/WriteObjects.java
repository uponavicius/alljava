package com.upondev.udemy.JavaTutorialForCompletreBeginners.UdemyJavaTutorial.L46_SerializingArraysAndArrayList;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;

public class WriteObjects {
    public static void main(String[] args) {
        System.out.println("Writing objects...");
        Person46[] people = {
                new Person46(1, "Sue"),
                new Person46(99, "Mike"),
                new Person46(7, "Bob")
        };

        ArrayList<Person46> peopleList = new ArrayList<>(Arrays.asList(people));

        try (FileOutputStream fs = new FileOutputStream("/Users/pro/IdeaProjects/allsdainfo/src/UdemyJavaTutorial/L46_SerializingArraysAndArrayList/people.txt");
             ObjectOutputStream os = new ObjectOutputStream(fs);) {
            //ObjectOutputStream os = new ObjectOutputStream(fs);
            os.writeObject(people);
            os.writeObject(peopleList);

            os.writeInt(peopleList.size());

            for (Person46 person : peopleList) {
                os.writeObject(people);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
