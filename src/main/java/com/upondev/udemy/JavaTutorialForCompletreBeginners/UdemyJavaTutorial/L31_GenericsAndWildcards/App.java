package com.upondev.udemy.JavaTutorialForCompletreBeginners.UdemyJavaTutorial.L31_GenericsAndWildcards;

import java.util.ArrayList;

public class App {
    public static void main(String[] args) {
        //pvz 1
        ArrayList<String> list = new ArrayList<>();
        list.add("One");
        list.add("Two");
        showList(list);

        //pvz 2
        ArrayList<Machine> machineList = new ArrayList<>();
        machineList.add(new Machine());
        machineList.add(new Machine());
        showMachineList(machineList);

        //pvz3
        ArrayList<Camera> cameraList = new ArrayList<>();
        cameraList.add(new Camera());
        cameraList.add(new Camera());
        showCameraList(cameraList);
        showCameraListOtherMethod(cameraList);
        showCameraListOtherMethod2(cameraList);


    }

    public static void showList(ArrayList<String> list) {
        System.out.println("Method - showList");
        for (String value : list) {
            System.out.println(value);
        }
    }

    public static void showMachineList(ArrayList<Machine> list) {
        System.out.println("Method - showMachineList");
        for (Machine value : list) {
            System.out.println(value);
        }
    }

    //universalesnis variantas
    public static void showCameraList(ArrayList<?> list) { //? - wildcard
        System.out.println("Method - showCameraList ");
        for (Object value : list) {
            System.out.println(value);
        }
    }

    public static void showCameraListOtherMethod(ArrayList<? extends Machine> list) { //? - wildcard
        System.out.println("Method - showCameraListOtherMethod");
        for (Machine value : list) {
            System.out.println(value);
            value.start();

            // value.snap(); negaliu iskviesti
        }
    }

    public static void showCameraListOtherMethod2(ArrayList<? super Camera> list) { //? - wildcard
        System.out.println("Method - showCameraListOtherMethod2");
        for (Object value : list) {
            System.out.println(value);

        }
    }
}
