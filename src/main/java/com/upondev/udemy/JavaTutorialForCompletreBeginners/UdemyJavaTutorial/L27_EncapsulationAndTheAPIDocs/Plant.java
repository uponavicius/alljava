package com.upondev.udemy.JavaTutorialForCompletreBeginners.UdemyJavaTutorial.L27_EncapsulationAndTheAPIDocs;

public class Plant {
    //Encapsulation emse neleisti kitpoms klasems pasiekti elementu.
    private String name;

    //galima pasiekti private informacija is kitu klasiu
    public String getName() {
        return name;
    }

    //galima keisti private kintamojo reiksme is kitu klasiu.
    public void setName(String name) {
        this.name = name;
    }
}
