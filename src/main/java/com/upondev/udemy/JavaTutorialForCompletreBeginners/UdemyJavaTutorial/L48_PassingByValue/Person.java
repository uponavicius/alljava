package com.upondev.udemy.JavaTutorialForCompletreBeginners.UdemyJavaTutorial.L48_PassingByValue;

public class Person {
    private String name;

    public Person(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Name: " + name;
    }
}

