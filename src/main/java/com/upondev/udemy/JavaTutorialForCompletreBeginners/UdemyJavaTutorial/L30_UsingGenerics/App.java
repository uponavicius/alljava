package com.upondev.udemy.JavaTutorialForCompletreBeginners.UdemyJavaTutorial.L30_UsingGenerics;


// Generic class is class that ca work with other objects and you specified what type of object it can work with you
// instance a class then you create the object in the class

import java.util.ArrayList;
import java.util.HashMap;

//Bendroji klasė yra klasė, kuri gali dirbti su kitais objektais, o jūs nurodėte, kokio tipo objektą ji gali
// naudoti kartu su jumis, pavyzdžiui, klasę, tada sukuriate objektą klasėje.
public class App {
    public static void main(String[] args) {

        //-------------------BEFORE JAVA 5-------------------
        ArrayList list = new ArrayList();
        list.add("Apple");
        list.add("Orange");
        list.add("Banana");

        String fruit = (String)list.get(1); //donwcast
        System.out.println(fruit);

        //-------------------FROM JAVA 5-------------------
        ArrayList <String> strings = new ArrayList<String>();
        strings.add("Cat");
        strings.add("Dog");
        strings.add("Alligator");

        String animal = strings.get(1);
        System.out.println(animal);

        //-------------------There can be more than one type argument-------------------
        HashMap<Integer, String> map = new HashMap<>();

        //-------------------From Java 7 style-------------------
        ArrayList <Animal> someList = new ArrayList<>();

    }
}
