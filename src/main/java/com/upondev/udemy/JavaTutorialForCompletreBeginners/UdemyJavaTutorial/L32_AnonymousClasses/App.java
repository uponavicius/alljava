package com.upondev.udemy.JavaTutorialForCompletreBeginners.UdemyJavaTutorial.L32_AnonymousClasses;

public class App {
    public static void main(String[] args) {
        Machnine machnine1 = new Machnine(){
            //child klase machine - kitaip vadinama Anonymous Classes
            @Override
            public void start() {
                System.out.println("Camera snapping....");
            }
        };
        machnine1.start();

        Plant plant1 = new Plant(){
            //Anonymous Classes
            @Override
            public void grow() {
                System.out.println("Plant growing.");
            }
        };

        plant1.grow();


    }
}
