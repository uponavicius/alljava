package com.upondev.udemy.JavaTutorialForCompletreBeginners.UdemyJavaTutorial.L37_AbstractClasses;

public class App {
    public static void main(String[] args) {
        //Abstract Classes nieko negali daryti pati, tai yra baze kitoms klasems.
        //Klase gali turite tik viena parent klase, bet daug intercafe
        //Interface metodai yra tusti, o abstrakt klaseje jei gali buti aprasyti

        Camera cam1 = new Camera();
        cam1.setId(5);

        Car car1 = new Car();
        car1.setId(4);

        //Machine klase yra abstrakti klase, todel neveikia objekto kurimas
        //Machine machine1 = new Machine();

        car1.run();

    }
}
