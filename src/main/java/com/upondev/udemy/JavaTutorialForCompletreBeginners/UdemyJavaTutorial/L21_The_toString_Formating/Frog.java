package com.upondev.udemy.JavaTutorialForCompletreBeginners.UdemyJavaTutorial.L21_The_toString_Formating;

public class Frog {
    private int id;
    private String name;

    public Frog(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(); //programose geriau naudoti StringBuilder
        sb.append(id).append(": ").append(name);
        return sb.toString();

        //return id + ": " + name; //alternatyva
        // return String.format("%4d: %s", id, name); //alternatyva
    }

}
