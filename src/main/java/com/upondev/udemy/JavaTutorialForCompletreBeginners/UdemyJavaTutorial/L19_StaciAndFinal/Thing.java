package com.upondev.udemy.JavaTutorialForCompletreBeginners.UdemyJavaTutorial.L19_StaciAndFinal;

public class Thing {
    // final constance kintamieji yr anekeiciami, tokie kaip Math.PI pi=3.14 matemtiskai
    public final static int LUCKY_NUMBER = 7;

    //instance kintamasis. gali tureti kopijas
    public String name;

    //klases kintamasis ir jo gali buti tik viena kopija, nes viena klase.
    public static String description;

    public static int count = 0;

    public int id;

    public Thing() {
        id = count;
        count++;
    }

    public void showName() {
        System.out.println(name);
        System.out.println("Object id: " + id + ". " + description + ": " + name); //instance metodai gali prieiti prie statiniu duomenu.
    }

    public static void showInfo() {
        System.out.println(description); //statiniai metodai turi preiga prie statiniu duomenu.
        //System.out.println(name); // bet negali prieiti prie nestatiniu duomenu.
    }


}
