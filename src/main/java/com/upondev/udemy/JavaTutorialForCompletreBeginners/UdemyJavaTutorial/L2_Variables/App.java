package com.upondev.udemy.JavaTutorialForCompletreBeginners.UdemyJavaTutorial.L2_Variables;

public class App {

    public static void main(String[] args) {
        //-------------Variables (Kintamieji)-------------------------------
        //primitivieji kitamuju tipai
        int myNumber; //deklaruojamas kintamasis. Sukuriamas
        myNumber = 88; //inicijuojamas kintamasis. Priskiriam reiksme.
        int number = 88; //deklaruojama ir iskarto inicijuojama.

        int myInt = 88;
        short myShort = 88;
        long myLong = 88;

        double myDouble = 3.14;
        float myFloat = 324.584F;

        char myChar = 'v';
        boolean myBoolean = true;

        byte myByte = 127;

        //------------------working with text-----------------------------------------------
        String myString = "text";

    }
}
