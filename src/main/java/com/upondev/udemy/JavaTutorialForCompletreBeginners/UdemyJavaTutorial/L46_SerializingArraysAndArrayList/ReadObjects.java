package com.upondev.udemy.JavaTutorialForCompletreBeginners.UdemyJavaTutorial.L46_SerializingArraysAndArrayList;

import java.io.*;
import java.util.ArrayList;

public class ReadObjects {
    public static void main(String[] args) {
        System.out.println("Reading objects...");


        try (FileInputStream fi = new FileInputStream("/Users/pro/IdeaProjects/allsdainfo/src/UdemyJavaTutorial/L46_SerializingArraysAndArrayList/people.txt")) {

            ObjectInputStream os = new ObjectInputStream(fi);
            Person46[] people = (Person46[]) os.readObject();

            @SuppressWarnings("unchecked")
            ArrayList<Person46> peopleList = (ArrayList<Person46>) os.readObject();

            for (Person46 person : people) {
                System.out.println(person);
            }

            for (Person46 person : peopleList) {
                System.out.println(person);
            }

            int num = os.readInt();
            for (int i = 0; i < num; i++) {
                Person46 person = (Person46) os.readObject();
                System.out.println(person);
            }

            os.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }
}
