package com.upondev.udemy.JavaTutorialForCompletreBeginners.UdemyJavaTutorial.L40_CreatingAndWritingTextFiles;

import java.io.*;

public class App {
    public static void main(String[] args) {

        File file = new File("/Users/pro/IdeaProjects/allsdainfo/src/UdemyJavaTutorial/L40_CreatingAndWritingTextFiles/Info.txt");
        try (BufferedWriter bufferedReader = new BufferedWriter(new FileWriter(file))) {
            bufferedReader.write("This is line one");
            bufferedReader.newLine();
            bufferedReader.write("This is line two");
            bufferedReader.newLine();
            bufferedReader.write("Last line");
        } catch (IOException e) {
            System.out.println("Unable to read file: " + file.toString());
        }

    }
}
