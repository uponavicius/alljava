package com.upondev.udemy.JavaTutorialForCompletreBeginners.UdemyJavaTutorial.L24_Interface;

public class App {
    public static void main(String[] args) {
        Machine machine1 = new Machine();
        machine1.start();

        Person person1 = new Person("Bob");
        person1.greet();

        //taip galima daryti, nes Machine yra implements Info
        Info info1 = new Machine();
        info1.showInfo();
        //taip galima daryti, nes Person yra implements Info
        Info info2 = person1;
        info2.showInfo();
        System.out.println("--------------");

        outputInfo(machine1);
        outputInfo(person1);

    }

    private static void outputInfo(Info info){ //galima ir taip panaudoti
        info.showInfo();
    }
}
