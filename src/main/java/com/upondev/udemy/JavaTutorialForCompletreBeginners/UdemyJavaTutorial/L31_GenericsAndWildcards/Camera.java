package com.upondev.udemy.JavaTutorialForCompletreBeginners.UdemyJavaTutorial.L31_GenericsAndWildcards;

public class Camera extends Machine {
    @Override
    public String toString(){
        return "I am a camera";
    }

    public void snap(){
        System.out.println("Snap!");
    }
}

