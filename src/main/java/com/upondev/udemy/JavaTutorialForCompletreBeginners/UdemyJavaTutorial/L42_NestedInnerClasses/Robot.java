package com.upondev.udemy.JavaTutorialForCompletreBeginners.UdemyJavaTutorial.L42_NestedInnerClasses;

public class Robot {
    private int id;

    //nestatines nested klases naudojamos tada kai norime sugrupuoti funkcionaluma
    //kai reikia, kad klase turetu priemjima i instans kintamuosius uz klases
    private class Brain {
        public void think() {
            System.out.println("Robot " + id + " is thinking");
        }
    }

    //kai norime normalios klases ir kai norime uz klases ribu sugrupuoti
    public static class Battery {
        public void charge() {
            System.out.println("Battery charging...");
        }
    }

    public Robot(int id) {
        this.id = id;
    }

    public void start() {
        System.out.println("Starting robot " + id);
        Brain brain = new Brain();
        brain.think();

        final String name = "Robot";
        class Temp {
            public void doSomething() {
                System.out.println("ID is: " + id);
                System.out.println("My name is " + name);
            }
        }
        Temp temp = new Temp();
        temp.doSomething();
    }
}
