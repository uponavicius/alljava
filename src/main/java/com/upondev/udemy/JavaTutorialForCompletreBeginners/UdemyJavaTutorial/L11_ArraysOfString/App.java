package com.upondev.udemy.JavaTutorialForCompletreBeginners.UdemyJavaTutorial.L11_ArraysOfString;

public class App {
    public static void main(String[] args) {
        String[] words = new String[3];
        words[0] = "Hello";
        words[1] = "to";
        words[2] = "you";

        String[] fruits = {"apple", "banana", "pear", "kiwi"};
        for (String fruit : fruits) { //vietojo fori loop galima taip naudoti (foreach)
            System.out.println(fruit);
        }

        String[] texts=new String[2]; //usima atmintis 2 elementams, bet reiksmiu

    }
}
