package com.upondev.udemy.JavaTutorialForCompletreBeginners.UdemyJavaTutorial.L41_TheEqualsMethod;

public class App {
    public static void main(String[] args) {
        Person person1 = new Person(5, "Bob");
        Person person2 = new Person(5, "Bob");
        //== geriau nenaudoti su objektais. geriau naudoti .equels();
        //== naudoti su primitiviais kintamaisiais.

        System.out.println(person1.equals(person2));

        Double value1 = 7.2;
        Double value2 = 7.2;
        System.out.println(value1 == value2);
        System.out.println(value1.equals(value2));

        Integer number1 = 6;
        Integer number2 = 6;
        System.out.println(number1.equals(number2));

        String text1 = "Hello";
        String text2 = "Hello";
        System.out.println(text1 == text2);
        System.out.println(text1.equals(text2));

    }
}
