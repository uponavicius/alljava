package com.upondev.udemy.JavaTutorialForCompletreBeginners.UdemyJavaTutorial.L17_SettersAndThis;

public class Frog {
    //encapsulation - informacijos spepimas. Siuo atveju panaudojant private access modifier
    private String name;
    private int age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setInfo(String name, int age) {
        setName(name); //tas pats kas this.name = name;
        setAge(age); //tas pats kas this.age = age;
    }
}
