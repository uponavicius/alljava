package com.upondev.udemy.JavaTutorialForCompletreBeginners.UdemyJavaTutorial.L47_TheTransientKeywordAndMoreSerialization;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;


public class ReadObjects {
    public static void main(String[] args) {
        System.out.println("Reading objects...");


        try (FileInputStream fi = new FileInputStream("/Users/pro/IdeaProjects/allsdainfo/src/UdemyJavaTutorial/L47_TheTransientKeywordAndMoreSerialization/people.txt");
             ObjectInputStream os = new ObjectInputStream(fi)) {
            Person47 person47 = (Person47) os.readObject();

            System.out.println(person47); //id ir count yra 0, nes panaudotas transient

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }
}
