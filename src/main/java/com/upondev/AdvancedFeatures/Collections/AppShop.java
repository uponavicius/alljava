package com.upondev.AdvancedFeatures.Collections;

import java.util.*;

public class AppShop {
    // public static void main(String[] args) {
    //    // List<String> shopItems = new ArrayList<>();
    //     Set<String> shopItems = new HashSet<>(); //Set neleidzia tureti dublikatu
    //     Scanner input = new Scanner(System.in);
    //
    //     while (true) {
    //         System.out.println("Enter product or q, d");
    //         String userEnter = input.nextLine();
    //         if (userEnter.equalsIgnoreCase("q")) {
    //             break;
    //         } else {
    //             shopItems.add(userEnter);
    //         }
    //     }
    //     for (String item : shopItems) {
    //         System.out.println(item + " ");
    //     }
    //
    // }

    public static void main(String[] args) {
        Set<ShopItem> shopItems = new HashSet<ShopItem>();
        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.println("Enter a to product or r to remove,   or enter q");
            String userEnter = scanner.nextLine();
            if (userEnter.equalsIgnoreCase("q")) {
                break;
            } else if (userEnter.equalsIgnoreCase("a")) {
                addProduct(shopItems, scanner);
            } else if (userEnter.equalsIgnoreCase("r")) {
                removeProduct(shopItems, scanner);
            } else if (userEnter.equalsIgnoreCase("c")) {
                modifyRating(shopItems, scanner);
            }
        }
        printAllItems(shopItems);
        // Iterator<String> iterator = shopItems.iterator();
        // String last = ""
        // while (iterator.hasNext()) {
        //     if (iterator.next().startsWith("m") & !last.equalsIgnoreCase("")) {
        //         System.out.println(last);
        //     }
        //     last = iterator.next();
        // }
    }

    private static void modifyRating(Set<ShopItem> shopItems, Scanner scanner) {
        String userEnter;
        while (true) {
            userEnter = scanner.nextLine();
            if (userEnter.equalsIgnoreCase("q")) {
                break;
            } else {
                findShopItem(shopItems, userEnter);
            }
        }
    }

    private static void findShopItem(Set<ShopItem> shopItems, String userEnter) {
        String rating = userEnter.substring(userEnter.indexOf(" "));
        String name = userEnter.substring(0, userEnter.indexOf(" "));

        for (ShopItem item : shopItems) {
            if (item.getName().equalsIgnoreCase(name)) {
                item.setRating(Integer.valueOf(rating.trim()));
            }
        }
    }

    //---------------------------- METODAI ---------------------------------

    private static void printAllItems(Set<ShopItem> shopItems) {
        int average = 0;
        for (ShopItem item : shopItems) {
            average = average + item.getRating();
            System.out.println(item + " ");
        }
        System.out.println("Avarange of rating :" + ((double)average / shopItems.size()));
    }


    private static void removeProduct(Set<ShopItem> shopItems, Scanner scanner) {
        String userEnter;
        while (true) {
            userEnter = scanner.nextLine();
            if (userEnter.equalsIgnoreCase("q")) {
                break;
            } else {
                shopItems.remove(new ShopItem(userEnter));
            }
        }
    }

    private static void addProduct(Set<ShopItem> shopItems, Scanner scanner) {
        String userEnter;
        while (true) {
            userEnter = scanner.nextLine();
            if (userEnter.equalsIgnoreCase("q")) {
                break;
            } else {
                shopItems.add(new ShopItem(userEnter));
            }
        }
    }
}
