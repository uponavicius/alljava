package com.upondev.AdvancedFeatures.Collections;

import java.util.*;

/**
 * 1. Create a set consisting of colors - given from the user.
 * 2. Present the removal of individual elements from the set.
 * 3. Display the collection before and after sorting.
 */

public class AppNew {
    static Set<String> colors = new HashSet<String>();

    public static void main(String[] args) {


        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.println("enter color or q for exit pgm");
            String userEnter = scanner.nextLine();
            if (userEnter.startsWith("q")) {
                break;
            } else if (userEnter.startsWith("a")) {
                String color = userEnter.substring(userEnter.indexOf(" ")).trim();
                colors.add(color);
            } else if (userEnter.startsWith("r")) {
                String color = userEnter.substring(userEnter.indexOf(" ")).trim();
                printAllColors();
                colors.remove(color);
                System.out.println();
                printAllColors();
            }
        }

        printAllColors();
        List<String> sortedList = new ArrayList<String>(colors);
        Collections.sort(sortedList);
        printSortedList(sortedList);
        System.out.println();
    }

    private static void printSortedList(List<String> sortedList) {
        for (String color : sortedList) {
            System.out.println(" " + color);
        }
    }

    public static void printAllColors() {
        for (String color : colors) {
            System.out.println(" " + color);
        }
    }
}
