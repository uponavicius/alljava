package com.upondev.AdvancedFeatures.Collections;


public class ShopItem {
    private String name;
    private int rating;

    public ShopItem(String name) {
        this.name = name;
        this.rating = 0;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRating() {

        return rating;
    }

    public void setRating(int rating) {
        if (validRating(rating)) {
            this.rating = rating;
        }
    }

    private boolean validRating(int rating) {
        if (rating < 1 || rating > 6) {
            System.out.println("Bad rating");
            return false;
        } else {
            return true;
        }
    }

    @Override
    public String toString() {
        return name + " " + rating;
    }

}
