package com.upondev.AdvancedFeatures.SchoolByMindaugasDest;

import java.util.ArrayList;
import java.util.List;

public class Subject {
    private String name;
    private List<Integer> grades;

    public Subject(String name) {
        this.name = name;
        this.grades = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Integer> getGrades() {
        return grades;
    }

    public void setGrades(List<Integer> grades) {
        this.grades = grades;
    }

    public void addGrade(Integer grade) {
        grades.add(grade);
    }

    @Override
    public String toString() {
        return name + " " + grades;
    }
}
