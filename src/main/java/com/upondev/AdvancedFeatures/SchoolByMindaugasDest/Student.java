package com.upondev.AdvancedFeatures.SchoolByMindaugasDest;


import java.util.ArrayList;
import java.util.List;

public class Student {
    private String school;
    private String name;
    private List<Subject> subjects;

    public Student(String school, String name) {
        this.school = school;
        this.name = name;
        this.subjects = new ArrayList<>();
    }

    public String getSchool() {
        return school;
    }


    public void setSchool(String school) {
        this.school = school;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Subject> getSubjects() {
        return subjects;
    }

    public void setSubjects(List<Subject> subjects) {
        this.subjects = subjects;
    }

    public void addSubject(Subject subject) {
        subjects.add(subject);
    }

    @Override
    public String toString() {
        return school + " " + name + " " + subjects;
    }
}
