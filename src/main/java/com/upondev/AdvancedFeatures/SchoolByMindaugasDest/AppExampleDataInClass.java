package com.upondev.AdvancedFeatures.SchoolByMindaugasDest;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

public class AppExampleDataInClass {
    public static void main(String[] args) {
        Path path = Paths.get("/Users/pro/IdeaProjects/Red_Project/src/VladasU/Advanced/Features/school/input.txt");
        List<String> rawData = new ArrayList<>(); // pasitalpiname duomenis i arraylista
        List<String> spilitedValues = new ArrayList<>(); //i ji sudeseime reikmes tarp kabletaskiu
        Map<String, Student> studentsMap = new HashMap<>(); //mapas studentu, kuriuos atskirsime pagal varda
        Scanner scanner = new Scanner(System.in);

        try {
            rawData = Files.readAllLines(path); //nusiskaitome faila. nuskaitome visas eilutes
            for (String unslipitedLine : rawData) { //nepadalintos eilutes failo viduje
                spilitedValues.addAll(Arrays.asList(unslipitedLine.split(";"))); //atskiriame duomenis kas ; su sudetame i lista
            }

            for (int i = 0; i < spilitedValues.size(); i = i + 4) { //zinome, kad duomenys eina duomenis ka 4-ta.
                // System.out.println(getSchoolName(spilitedValues, i) + " "
                //         + getStudentName(spilitedValues, i) + " "
                //         + getSubjectName(spilitedValues, i) + " "
                //         + getGrade(spilitedValues, i));

                Student tempStudent = new Student(getSchoolName(spilitedValues, i), getStudentName(spilitedValues, i)); //zinoma, kad studetas, bet nezimone kuris


                Student foundStudent = studentsMap.get(tempStudent.getName()); //bandome surasti sutdenta, ar jis yra mape ar jo nera. key mape yra studento vardas

                if (foundStudent == null) { //nerado studento
                    Subject subject = new Subject(getSubjectName(spilitedValues, i));
                    subject.addGrade(Integer.valueOf(getGrade(spilitedValues, i))); //nauajs dalykas ir pazimys
                    tempStudent.addSubject(subject);

                    studentsMap.put(tempStudent.getName(), tempStudent); //idejome nauja studenta, paskaita ir pazimi


                } else {
                    Subject tempSubject = new Subject(getSubjectName(spilitedValues, i)); //bandome surasti ar yra ar jo nera
                    boolean isNewSubject = true;
                    for (Subject foundSubject : foundStudent.getSubjects()) {
                        if (foundSubject.getName().equalsIgnoreCase(tempSubject.getName())) { //jei rado paskaitos
                            foundSubject.addGrade(Integer.valueOf(getGrade(spilitedValues, i))); // ir pazimi
                            isNewSubject = false;
                            break;
                        }
                    }
                    if (isNewSubject) { //true
                        tempSubject.addGrade(Integer.valueOf(getGrade(spilitedValues, i)));
                        foundStudent.addSubject(tempSubject);
                    }

                }

            }

            while (true) {
                System.out.println("Enter student name or 'Q' - to quit from program");
                String userEnter = scanner.nextLine();

                if (userEnter.equalsIgnoreCase("q")) {
                    break;
                } else {
                    Student whatUserWants = studentsMap.get(userEnter);
                    int totalEntries = 0;
                    //System.out.println(studentsMap.get(whatUserWants));

                    for (Subject subject : whatUserWants.getSubjects()) {
                        totalEntries = totalEntries + subject.getGrades().size();
                        System.out.println(subject);

                    }
                    System.out.println("Total entries: " + totalEntries);

                }
            }


        } catch (Exception e) {
            System.out.println(e.toString());
        }


    }

    private static String getGrade(List<String> spilitedValues, int i) {
        return spilitedValues.get(i + 3);
    }

    private static String getSubjectName(List<String> spilitedValues, int i) {
        return spilitedValues.get(i + 2);
    }

    private static String getStudentName(List<String> spilitedValues, int i) {
        return spilitedValues.get(i + 1);
    }

    private static String getSchoolName(List<String> spilitedValues, int i) {
        return spilitedValues.get(i);
    }

}
