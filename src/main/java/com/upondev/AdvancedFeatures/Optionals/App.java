package com.upondev.AdvancedFeatures.Optionals;

import java.util.Optional;
import java.util.Scanner;

public class App {
    public static void main(String[] args) {
        // of method does not allow null value
        Optional<String> uncertainValue;


        while (true) {
            System.out.println("Enter name, or Q to quit");
            Scanner scanner = new Scanner(System.in);
            String userEnter = scanner.nextLine();

            if (userEnter.equalsIgnoreCase("q")) {
                break;
            } else if (userEnter.isEmpty()) {
                uncertainValue = Optional.ofNullable(null);

            } else {
                uncertainValue = Optional.of(userEnter);
            }
            if (uncertainValue.isPresent()) {
                System.out.println(uncertainValue.get().split(" ")[0]);
            } else {
                System.out.println("no name entered");
            }
        }


    }
}
