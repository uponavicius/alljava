package com.upondev.AdvancedFeatures.Encapsulation;

public class Planet {
    private PlanetName name;
    private RelativeSize size;

    public Planet(PlanetName name, RelativeSize size) {
        this.name = name;
        this.size = size;
    }

    public PlanetName getName() {
        return name;
    }

    public void setName(PlanetName name) {
        this.name = name;
    }

    public RelativeSize getSize() {
        return size;
    }

    public void setSize(RelativeSize size) {
        this.size = size;
    }

    @Override
    public String toString(){
        return size + " " + name;
    }
}
