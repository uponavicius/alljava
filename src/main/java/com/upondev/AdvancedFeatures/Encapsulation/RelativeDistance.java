package com.upondev.AdvancedFeatures.Encapsulation;

public enum RelativeDistance {
    ZERO(0),
    CLOSE(1000),
    REMOTE(100000);


    int value;

    RelativeDistance(int value) {
        this.value = value;
    }

    public static RelativeDistance distanceFromEarth(PlanetName name) {
        switch (name) {
            case EARTH:
                return ZERO;
            case PLUTO:
                return CLOSE;
            case JUPITER:
                return REMOTE;
            default:
                return ZERO;
        }
    }


}
