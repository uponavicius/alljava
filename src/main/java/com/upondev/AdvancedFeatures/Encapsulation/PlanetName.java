package com.upondev.AdvancedFeatures.Encapsulation;

public enum PlanetName {
    JUPITER,
    PLUTO,
    EARTH;
}
