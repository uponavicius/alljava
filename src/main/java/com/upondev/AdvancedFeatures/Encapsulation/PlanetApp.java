package com.upondev.AdvancedFeatures.Encapsulation;

public class PlanetApp {
    public static void main(String[] args) {
        Planet jupiter = new Planet(PlanetName.JUPITER, RelativeSize.HUGE);
        Planet pluto = new Planet(PlanetName.PLUTO, RelativeSize.SMALL);
        Planet earth = new Planet(PlanetName.EARTH, RelativeSize.SMALL);

        System.out.println(jupiter.toString());
        System.out.println(pluto.toString());
        System.out.println(earth.toString());

        //System.out.println(RelativeDistance.distanceFromEarth(jupiter.getName()));
        //System.out.println(RelativeDistance.distanceFromEarth(pluto.getName()));
        //System.out.println(RelativeDistance.distanceFromEarth(earth.getName()));

    }
}
