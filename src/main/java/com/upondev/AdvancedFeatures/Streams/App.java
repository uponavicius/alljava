package com.upondev.AdvancedFeatures.Streams;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class App {
    public static void main(String[] args) {
        Path path = Paths.get("/Users/pro/IdeaProjects/Red_Project/src/VladasU/Advanced/Features/Streams/100000SalesRecords.csv");
        List<String> rawData = new ArrayList<>();
        List<String> orderData = new ArrayList<>();

        try {
            rawData = Files.readAllLines(path);
            // for (String unslipitedLine : rawData) { // ta pati padareme su stream
            //     orderData.addAll(Arrays.asList(unslipitedLine.split(",")));
            //
            // }
            rawData.stream()
                    .map(s -> s.split(","))
                    .forEach(a->orderData.addAll(Arrays.asList(a)));

            for (int i = 14; i < orderData.size(); i = i + 14) {
                System.out.println("Region: " + orderData.get(i)
                        + "Country: " + orderData.get(i + 1)
                        + "Item Type: " + orderData.get(i + 2)
                        + "Sales Channel: " + orderData.get(i + 3)
                        + "Order Priority: " + orderData.get(i + 4)
                        + "Order Date: " + orderData.get(i + 5)
                        + "Order ID: " + orderData.get(i + 6)
                        + "Ship Date: " + orderData.get(i + 7)
                        + "Units Sold: " + orderData.get(i + 8)
                        + "Unit Price: " + orderData.get(i + 9)
                        + "Unit Cost: " + orderData.get(i + 10)
                        + "Total Revenue: " + orderData.get(i + 11)
                        + "Total Cost: " + orderData.get(i + 12)
                        + "Total Profit: " + orderData.get(i + 13)
                );

            }




        } catch (Exception e) {
            System.out.println(e.toString());
        }


    }
}
