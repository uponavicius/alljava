package com.upondev.AdvancedFeatures.Inheritance;

public class Circle extends Shape {
    private int radius;

    public Circle(String color, int size, int radius) {
        super(color, size);
        this.radius = radius;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        if (radius <= 0) {
            throw new RuntimeException("Bad value " + radius);
        }
        this.radius = radius;
    }
}
