package com.upondev.AdvancedFeatures.Inheritance;

public class Rectangle extends Shape {
    private int height;
    private int width;

    public Rectangle(String color, int size, int height, int width) {
        super(color, size);
        this.height = height;
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        if (height <= 0) {
            throw new RuntimeException("Bad value " + height);
        }
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        if (width <= 0) {
            throw new RuntimeException("Bad Value " + width);
        }
        this.width = width;
    }
}
