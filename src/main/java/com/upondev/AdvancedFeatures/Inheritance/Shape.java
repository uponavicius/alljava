package com.upondev.AdvancedFeatures.Inheritance;

public class Shape {
    // a) Add fields, create constructor, getters and setters.
    // b) Create classes Rectangle and Circle. Both of them should inherit class Shape. Which fields and methods are common?

    private String color;
    private int size;

    public Shape(String color, int size) {
        this.color = color;
        this.size = size;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
