package com.upondev.AdvancedFeatures.HW;

/**
 * Create enum Gender with values MALE, FEMALE.
 */

public enum Gender {
    MALE,
    FEMALE;

}
