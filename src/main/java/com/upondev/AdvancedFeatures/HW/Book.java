package com.upondev.AdvancedFeatures.HW;

/**
 * Create class Book with String parameter name and Author parameter author.
 * All parameters should be used in Book constructor and have public get/set methods.
 */
public class Book extends Author {
    private String bookName = "unknown book name";

    public Book(String authorName, Gender gender, String email, String bookName) {
        super(authorName, gender, email);
        this.bookName = bookName;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    @Override
    public String toString() {
        return "Author name: " + getAuthorName()
                + ". Gender: " + getGender()
                + ". E mail: " + getEmail()
                + ". Book name: " + bookName;
    }
}
