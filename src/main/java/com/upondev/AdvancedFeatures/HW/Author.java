package com.upondev.AdvancedFeatures.HW;

/**
 * Create class Author with String parameters name, email and Gender parameter gender.
 * All parameters should be used in Author constructor and have public get/set methods.
 */
public class Author {
    private String authorName = "unknown";
    private String email = "unknown";
    private Gender gender;


    public Author(String authorName, Gender gender, String email) {
        this.authorName = authorName;
        this.email = email;
        this.gender = gender;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

//    @Override
////    public String toString() {
////        return "Author name: " + authorName + "\n"
////                + "Author email: " + email;
////    }
}
