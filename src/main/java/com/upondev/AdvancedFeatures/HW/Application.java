package com.upondev.AdvancedFeatures.HW;

/**
 * In your Application class with main method create Book object and try printing the values of its parameters.
 */
public class Application {
    public static void main(String[] args) {

        Book bookPetraitis = new Book("Petras Petraitis", Gender.MALE, "P.Petraitis@JavaIsSuper.com", "Java tests with Encapsulation, Inheritance, Composition and Enums");
        System.out.println(bookPetraitis);
        Book bookGina = new Book("Ieva Gina", Gender.FEMALE, "H.Balnys@JavaIsSuper.com", "Java is perfect");
        System.out.println(bookGina);

    }
}
