package com.upondev.AdvancedFeatures.school;

import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

/**
 * Reikia i console isvesti vieno mokinio rezultatus visose dalykose.
 * Kuriam mokiniu reikia isvesti rezultatus nurodoma per console.
 */
public class App {
    public static void main(String[] args) {
        Map<String, Student> studentMap = new HashMap<>(); //auditorija
        Path absolutePath = Paths.get("/Users/pro/IdeaProjects/Red_Project/src/VladasU/Advanced/Features/school/input.txt");
        Charset charset = Charset.forName("UTF-8");
        Scanner scanner = new Scanner(System.in);

        try {
            //nuskaitomas failas
            List<String> lines = Files.readAllLines(absolutePath, charset);
            for (String line : lines) {
                //kiekviena eilute nuskaitoma ir verciama i objekta
                String[] arr = line.split(";");
                String schoolName = arr[0];
                String surnameN = arr[1];
                String lecture = arr[2];
                int rating = Integer.parseInt(arr[3]);

                //sukurti objektus ir patalpinti i map <name, naujas
                if (studentMap.containsKey(surnameN.toLowerCase())) { //esamiems objektams pridedamas pazimys
                    studentMap.get(surnameN.toLowerCase()).addLecture(lecture, rating);

                } else { //jei nera sukuriamas naujas
                    Student student = new Student(surnameN, schoolName);
                    student.addLecture(lecture, rating);//
                    studentMap.put(surnameN.toLowerCase(), student); //uzpildau auditorija
                }

            }
        } catch (Exception e) {
            System.out.println(e);
        }

        while (true) {
            System.out.println("R - to know results");
            System.out.println("Q - to quit from program");
            System.out.println(studentMap.keySet());
            String userEnter = scanner.nextLine();

            if (userEnter.equalsIgnoreCase("q")) {
                System.out.println("Have a nice day!");
                break;
            } else if (userEnter.equalsIgnoreCase("r")) {


                //Uzklausiame pavardes ir isspausdiname auditorijoje esame jeigu jis yra
                System.out.println("Enter surname and name first letter");
                userEnter = scanner.nextLine().toLowerCase();

                if (studentMap.containsKey(userEnter)) {
                    System.out.println(studentMap.get(userEnter).printInfo());
                    System.out.println("");
                    System.out.println(studentMap.get(userEnter).printInfoAverage());
                } else {
                    System.out.println("There is no such student");
                }


            }
        }


    }
}
