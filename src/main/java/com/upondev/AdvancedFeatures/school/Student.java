package com.upondev.AdvancedFeatures.school;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class Student {
    private String schoolName;
    private String surnameN;

    Map<String, LinkedList<Integer>> allLectures = new HashMap<>(); //pazymiu knygele   String lecture, int grade


    public Student(String schoolName, String surnameN) {
        this.schoolName = schoolName;
        this.surnameN = surnameN;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getSurnameN() {
        return surnameN;
    }

    public void setSurnameN(String surnameN) {
        this.surnameN = surnameN;
    }

    public Map<String, LinkedList<Integer>> getAllLectures() {
        return allLectures;
    }

    public void setAllLectures(Map<String, LinkedList<Integer>> allLectures) {
        this.allLectures = allLectures;
    }

    public void addLecture(String lecture, Integer grade) {
        if (allLectures.containsKey(lecture)) {
            addGrade(allLectures.get(lecture), grade);
        } else {
            allLectures.put(lecture, new LinkedList<>());
            addGrade(allLectures.get(lecture), grade);
        }

    }

    public void addGrade(LinkedList<Integer> lectureGrades, int grade) { //naudojamas listas
        lectureGrades.add(grade);
    }

    public String printInfo() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(schoolName);
        stringBuilder.append("\n");
        stringBuilder.append(surnameN);
        stringBuilder.append("\n");
        int counter = 0;
        for (String lect : allLectures.keySet()) {
            stringBuilder.append(lect);
            stringBuilder.append(": ");
            stringBuilder.append(allLectures.get(lect));
            stringBuilder.append("\n");
            counter = counter + allLectures.get(lect).size();

        }
        stringBuilder.append("\n");
        stringBuilder.append("Lecture count: ");
        stringBuilder.append(allLectures.size());
        stringBuilder.append("\n");
        stringBuilder.append("Total grades number: ");
        stringBuilder.append(counter);
        return stringBuilder.toString();
    }

    public String printInfoAverage() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(schoolName);
        stringBuilder.append("\n");
        stringBuilder.append(surnameN);
        stringBuilder.append("\n");
        int counter = 0;
        for (String lect : allLectures.keySet()) {
            stringBuilder.append(lect);
            stringBuilder.append(": ");
            stringBuilder.append(getAverage(allLectures.get(lect)));
            stringBuilder.append("\n");
            counter = counter + allLectures.get(lect).size();


        }
        stringBuilder.append("\n");
        stringBuilder.append("Lecture count: ");
        stringBuilder.append(allLectures.size());
        stringBuilder.append("\n");
        stringBuilder.append("Total grades number: ");
        stringBuilder.append(counter);
        return stringBuilder.toString();
    }

    public double getAverage(LinkedList<Integer> gradeList) {
        int total = 0;
        for (int number : gradeList) {
            total += number;

        }
        double doubleRoundedToTwo = (double) total / gradeList.size();
        //susiapvalinti iki dvieju #.##
        doubleRoundedToTwo = (double) Math.round(doubleRoundedToTwo * 100) / 100;
        return doubleRoundedToTwo;
    }

    @Override
    public String toString() {
        return schoolName + " " + surnameN + " " + allLectures;
    }
}
