package com.upondev.AdvancedFeatures.Concurrency;

public class MultiThreading implements Runnable {
    private Extra extra;
    private String prefix;

    public MultiThreading(Extra extra, String prefix) {
        this.extra = extra;
        this.prefix = prefix;

    }

    // @Override
    public void run() {
        // System.out.println("Hello");
        try {
            int i = 3;
            Runnable target;
            Thread thread = new Thread();
            while (i > 0) {
                i--;
                if (i == 1 && prefix.equalsIgnoreCase("SW1")) {
                    thread.stop();
                } else {
                    thread.sleep(1000);

                }
                extra.checkMethod(prefix);
            }

        } catch (Exception e) {
            System.out.println(e.toString());
        }


    }


}
