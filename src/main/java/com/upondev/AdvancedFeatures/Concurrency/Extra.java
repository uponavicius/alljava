package com.upondev.AdvancedFeatures.Concurrency;

public class Extra {

    private int value;

    public Extra(int value) {
        this.value = value;
    }


    public void checkMethod(String prefix) {
        System.out.println("Incoming1: " + prefix + " " + value);
        value--;
        System.out.println("Incoming2: " + prefix + " " + value);
    }

}
