package com.upondev.AdvancedFeatures.Concurrency;

public class AppThreading {
    public static void main(String[] args) throws InterruptedException {
        Extra extra = new Extra(10);
        // MultiThreading thread1 = new MultiThreading(new Extra(), "SW1");
        // MultiThreading thread2 = new MultiThreading(new Extra(), "SW2");
        MultiThreading thread1 = new MultiThreading(extra, "SW1");
        MultiThreading thread2 = new MultiThreading(extra, "SW2");
        thread1.run();
        thread2.run();



    }
}
