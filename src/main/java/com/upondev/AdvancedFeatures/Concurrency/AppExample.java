package com.upondev.AdvancedFeatures.Concurrency;

public class AppExample {

    // public static void main(String[] args) throws InterruptedException {
    //     // System.out.println("Main thread starts");
    //     // Thread.sleep(5000);
    //     // System.out.println("Main thread is still running");
    //     // Thread.sleep(5000);
    //     // System.out.println("Main thread ends");
    //
    //     // StopWatchThread thread1 = new StopWatchThread("SW1");
    //     // StopWatchThread thread2 = new StopWatchThread("SW2");
    //     // thread1.run();
    //     // thread2.run();
    //
    //     Runnable target;
    //     Thread thread = new Thread(new StopWatchThread("Sw"));
    //     thread.start();



        public static void main (String[]args) throws InterruptedException {

            Bench bench = new Bench(1); // creating bench with one free seat
            SeatTakerThread seatTaker1 = new SeatTakerThread(bench);
            SeatTakerThread seatTaker2 = new SeatTakerThread(bench);
            seatTaker1.start();
            seatTaker2.start();


        }

    public void methodWithSyncedCodeBlock () {
        System.out.println("Unsynced part");
        synchronized (this) {
            System.out.println("Synced part"); //...
        }
    }


}

