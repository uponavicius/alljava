package com.upondev.AdvancedFeatures.GenericTypes;

public interface Comparable<T> {
    public int compareTo(T o);
}
