package com.upondev.AdvancedFeatures.GenericTypes;

public class App {
    /**
     * 1. Create a Person class that will implement a Comparable interface.
     * Person class should implement compareTo method, that will verify if one person is taller than another.
     * 2. Create a simple Generic class, that will give a possibility,
     * to store any kind of value within. Add object of type String,
     * Integer and Double to array of that Generic type. Print all values of the array within a loop.
     */
    public static void main(String[] args) {
        Person vladas = new Person("Vladas", 35);
        Person rokas = new Person("Rokas", 26);

        if (vladas.compareTo(rokas)>0){
            System.out.println("Vladas is older");
        } else {
            System.out.println("Rokas is older");
        }
    }
}
