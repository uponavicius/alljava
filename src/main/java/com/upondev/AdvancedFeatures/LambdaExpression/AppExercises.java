package com.upondev.AdvancedFeatures.LambdaExpression;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.UnaryOperator;

public class AppExercises {
    public static void main(String[] args) {
        UnaryOperator<Double> addition = d -> d + d;
        System.out.println(addition.apply(2.2));

        UnaryOperator<Double> subtraction = s -> s - s;
        System.out.println(subtraction.apply(4.4));

        UnaryOperator<Double> multiplication = m -> m * m;
        System.out.println(multiplication.apply(3.3));

        UnaryOperator<Double> division = d -> d / d;
        System.out.println(division.apply(3.3));


        List<Integer> numbers = new ArrayList<>();
        numbers.add(1);
        numbers.add(4);
        numbers.add(5);

        Function<List<Integer>, Integer> sumup = b->{ //siuo atbeju b yra tas musu listas numbers
            int sum = 0;
            for (Integer numer: b){
                sum = sum +numer;
            }
            return sum;
        };
        System.out.println(sumup.apply(numbers));


        String inputText = "asd sfdfdsf fgfdgf dfgdfg dfgfdfdg";
        Consumer<String> countWords = t->{
            System.out.println(t.split(" ").length);
        };
        countWords.accept(inputText); //inpuText yra t

    }
}