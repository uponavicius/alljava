package com.upondev.AdvancedFeatures.LambdaExpression;

import java.util.Random;
import java.util.function.*;

public class App {
    public static void main(String[] args) {

        Runnable myRunnable = () -> System.out.println("Running a runnable");
        myRunnable.run();
        Predicate<String> startsWithABCTest = s -> s.startsWith("ABC");
        System.out.println(startsWithABCTest.test("ABCDEF"));


        // Method apply returns Integer and the type of the parameter is String
        Function<String, Integer> stringLengthFunction = s -> s.length();
        System.out.println(stringLengthFunction.apply("ABCDE"));

        // Method apply returns String and the type of the parameter is String
        Function<String, String> replaceCommasWithDotsFunction = s -> s.replace(',', '.');
        System.out.println(replaceCommasWithDotsFunction.apply("a,b,c"));


        Person johnSmith = new Person("John", "Smith", 15);
        Predicate<Person> adultPersonTest = Person::isAdult; //nurodo, kad ir persona klases isAdult metoda
        System.out.println(adultPersonTest.test(johnSmith));


        Supplier<Integer> randomNumberSupplier = () -> new Random().nextInt(); //ka grazinsime
        int randomNumber = randomNumberSupplier.get();
        System.out.println(randomNumber);


        Consumer<Double> printWithPrefixConsumer = d -> System.out.println("Value: " + d); //ka paimsime
        printWithPrefixConsumer.accept(10.5);

        UnaryOperator<Integer> toSquareUnaryOperator = i -> i * i;
        System.out.println(toSquareUnaryOperator.apply(5));


        UnaryOperator<Integer> toSquareUnaryOperator2 = i -> {
            int result = i * i;
            System.out.println("Result: " + result);
            return result;
        };
        toSquareUnaryOperator2.apply(4);


    }

}
