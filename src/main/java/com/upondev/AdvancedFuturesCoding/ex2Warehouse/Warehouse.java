package com.upondev.AdvancedFuturesCoding.ex2Warehouse;

import java.util.ArrayList;
import java.util.List;

public class Warehouse {

    private List<Item> items = new ArrayList<>();

    public void add(Item item) { //metodas prides i listas
        items.add(item);
    }

    public void removeItem(Item item) { //metodas pasalins is listo
        try {
            items.remove(item);
        } catch (Exception e) {
            System.out.println("No item found");
        }
    }

    public void printItemList() {
        try {
            for (int i = 0; i < items.size(); i++) {
                System.out.println(items.get(i));
            }
        } catch (Exception e) {
            System.out.println("No item found");
        }
    }

    public void updateItem(String itemName, int itemQuantity, int itemPrice) {
        items.stream().filter(i -> i.getItemName().equalsIgnoreCase(itemName)).forEach(i -> i.setQuantity(itemQuantity));
        items.stream().filter(i -> i.getItemName().equalsIgnoreCase(itemName)).forEach(i -> i.setPrice(itemPrice));
    }

    public void itemsPriceSum() {
        // int sum = 0;
        // for (int i = 2; i < items.size(); i = i + 3) {
        //     sum = sum + items.get(i).getPrice();
        // }

        Integer sum = items.stream().map(i -> i.getPrice()).reduce(0, (a, b) -> a + b);
        System.out.println(sum);

    }
}
