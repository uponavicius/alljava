package com.upondev.AdvancedFuturesCoding.ex2Warehouse;

import java.util.Scanner;

/**
 * a. User should be able to: add, display all of the details, update, delete an item
 * b. Use composition and collections (The warehouse has products/items)
 * c. Add possibility to display summaries, like sum of all of the products, their prices.
 * d. *Add possibility to update number of items in a specific way, e.g.:
 * “pliers:30”
 * “scissors:+4”
 */
public class App {
    public static void main(String[] args) {

        Warehouse warehouse = new Warehouse();
        Scanner scanner = new Scanner(System.in);
        while (true) {
            meniu();

            String userInput = scanner.nextLine();
            if (userInput.equalsIgnoreCase("Q")) {
                break;
            } else if (userInput.equalsIgnoreCase("A")) {
                addItemWithMeniu(warehouse, scanner);
            } else if (userInput.equalsIgnoreCase("D")) {
                warehouse.printItemList();
            } else if (userInput.equalsIgnoreCase("U")) {
                updateItemWithMeniu(warehouse, scanner);
            } else if (userInput.equalsIgnoreCase("R")) {
                removeItemWithMeniu(warehouse, scanner);
            } else if (userInput.equalsIgnoreCase("S")) {
                warehouse.itemsPriceSum();

            } else {
                System.out.println("Please enter one of menu option");
            }
        }

    }

    //---------------------methods---------------------------------------------------
    private static void removeItemWithMeniu(Warehouse warehouse, Scanner scanner) {
        String itemName = itemName(scanner, "Please enter item name: ");
        int itemQuantity = itemQuantity(scanner, "Please enter quantity: ");
        int itemPrice = itemPrice(scanner, "Please enter item price: ");
        Item tempItem = new Item(itemName, itemQuantity, itemPrice);
        warehouse.removeItem(tempItem);
    }

    private static void updateItemWithMeniu(Warehouse warehouse, Scanner scanner) {
        String itemName = itemName(scanner, "Please enter item name: ");
        int itemQuantity = itemQuantity(scanner, "Please enter quantity: ");
        int itemPrice = itemPrice(scanner, "Please enter item price: ");
        warehouse.updateItem(itemName, itemQuantity, itemPrice);
    }

    private static void addItemWithMeniu(Warehouse warehouse, Scanner scanner) {
        String itemName = itemName(scanner, "Please enter item name: ");
        int itemQuantity = itemQuantity(scanner, "Please enter quantity: ");
        int itemPrice = itemPrice(scanner, "Please enter item price: ");
        warehouse.add(new Item(itemName, itemQuantity, itemPrice));
        System.out.println("Item successfully item ");
    }

    private static void meniu() {
        System.out.println("-----------------------------");
        System.out.println("Enter text or q to quit.");
        System.out.println("A - to add product");
        System.out.println("D - display details");
        System.out.println("U - to update item");
        System.out.println("R - delete item");
        System.out.println("S - all items price sum");
    }


    private static int itemPrice(Scanner scanner, String s) {
        System.out.println(s);
        return Integer.parseInt(scanner.nextLine());
    }

    private static int itemQuantity(Scanner scanner, String s) {
        System.out.println(s);
        return Integer.parseInt(scanner.nextLine());
    }

    private static String itemName(Scanner scanner, String s) {
        System.out.println(s);
        return scanner.nextLine();
    }


}
