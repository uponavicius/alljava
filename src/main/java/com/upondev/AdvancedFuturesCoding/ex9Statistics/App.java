package com.upondev.AdvancedFuturesCoding.ex9Statistics;

import java.io.*;
import java.util.Map;
import java.util.TreeMap;

/**
 * 9. * Statistics
 * a. Create a file that will contain any text. It can be generated using lorem ipsum library.
 * b. Create statistics of words contained in the text.
 * c. Display the number of occurrences of individual words in the form
 * <word>: <number of occurrences>
 * d. * as above sorted
 */

public class App {
    static final String fileName = "/Users/pro/IdeaProjects/AllCourse/src/main/java/com/upondev/Exercises9Statistics/text.txt";
    public static void main(String[] args) throws IOException {

        //File file = new File("/Users/pro/IdeaProjects/AllCourse/src/main/java/com/upondev/Exercises9Statistics/text.txt");
        //FileInputStream fileStream = new FileInputStream(new File("/Users/pro/IdeaProjects/AllCourse/src/main/java/com/upondev/Exercises9Statistics/text.txt"));
        //InputStreamReader input = new InputStreamReader(new FileInputStream(new File("/Users/pro/IdeaProjects/AllCourse/src/main/java/com/upondev/Exercises9Statistics/text.txt")));
        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(new File(fileName))));

        String line;

        // Initializing counters
        int countWord = 0;
        int sentenceCount = 0;
        int characterCount = 0;
        int paragraphCount = 1;
        int whitespaceCount = 0;
        Map<String, Integer> frequencyData = new TreeMap<>();

        // Reading line by line from the
        // file until a null is returned
        while((line = reader.readLine()) != null)
        {
            if(line.equals(""))
            {
                paragraphCount++;
            }
            if(!(line.equals("")))
            {

                characterCount += line.length();

                // \\s+ is the space delimiter in java
                String[] wordList = line.split("\\s+");

                for (String word: wordList){
                    if (frequencyData.containsKey(word)){ //jeigu yra toks zofis
                       // int value = frequencyData.get(word)+1 //
                        frequencyData.put(word, frequencyData.get(word)+1);
                    } else{
                        frequencyData.put(word, 1);
                    }
                }

                countWord += wordList.length;
                whitespaceCount += countWord -1;

                // [!?.:]+ is the sentence delimiter in java
                String[] sentenceList = line.split("[!?.:]+");

                sentenceCount += sentenceList.length;
            }
        }

        System.out.println("Total word count = " + countWord);
        System.out.println("Total number of sentences = " + sentenceCount);
        System.out.println("Total number of characters = " + characterCount);
        System.out.println("Number of paragraphs = " + paragraphCount);
        System.out.println("Total number of whitespaces = " + whitespaceCount);
        //System.out.println(frequencyData);
        frequencyData.forEach((key, value) -> System.out.println(key + ":" + value));

    }

}
