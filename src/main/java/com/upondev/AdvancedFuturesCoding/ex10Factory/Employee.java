package com.upondev.AdvancedFuturesCoding.ex10Factory;

public abstract class Employee {
    private String name;
    private String surname;
    private WorkTool workTool;
    private String workStatus;

    public Employee(String name, String surname) {
        this.name = name;
        this.surname = surname;
        setWorkTool(); //automatiskai nustaoto darbo iranki
        this.workStatus = "Waiting work....";
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setWorkTool(WorkTool workTool) {
        this.workTool = workTool;
    }

    public void setWorkStatus(String workStatus) {
        this.workStatus = workStatus;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public WorkTool getWorkTool() {
        return workTool;
    }

    public String getWorkStatus() {
        return workStatus;
    }

    public void setWorkTool() { //automatiskai nustaoto darbo iranki
        if (this instanceof Director) {
            this.workTool = WorkTool.POINTING_FINGER;
        } else if (this instanceof Manager) {
            this.workTool = WorkTool.LAPTOP;
        } else if (this instanceof Worker) {
            this.workTool = WorkTool.HAMMER;
        }
    }

    public abstract void doWork(); //abstraktus metodas

    public void rest() {
        setWorkStatus("Resting...");
    }

    public abstract void finishJob();


    @Override
    public String toString() {
        return "Name Surname: " + name + " " + surname + ". Working tool: " + workTool + ". Working status: " + workStatus;
    }
}
