package com.upondev.AdvancedFuturesCoding.ex10Factory;

import java.util.Scanner;

/**
 * *Create a factory that includes Manager, employees (Worker) and *Director.
 * a. loops, conditions - adding employees, displaying currently working (what they do), getting orders from the user.
 * b. OOP - all employees (including the director), inherit from the common class (Employee). Each employee may have a work tool (e.g. Hammer, Laptop, PointingFinger).
 * c. collections - appropriate grouping of data
 * d. * threads - use the while loop only to receive orders from the user, use the threads to display status / modify each object.
 * e. ** additional functionalities – e.g. displaying information about who and how much has earned since the beginning
 * of work (counting every few seconds), "deduct in succession" - sorted list of employees (by name), employee data can be loaded from the file at the beginning.
 * f. ** support for the Director class - displaying information "everybody works - great!", "where is <name> ?!" (e.g. if the employee has left earlier).
 */
public class App {
    public static void main(String[] args) {
        Director director = new Director("Vladas", "Uponavicius");

        Scanner scanner = new Scanner(System.in);
        while (true) {
            menu();

            String userInput = scanner.nextLine();
            if (userInput.equalsIgnoreCase("A")) {
                addEmployee(director, scanner);

            } else if (userInput.equalsIgnoreCase("S")) {
                director.showWorkingStatus();
            } else if (userInput.equalsIgnoreCase("O")) {

            } else if (userInput.equalsIgnoreCase("Q")) {
                break;
            } else {
                System.out.println("Bad command");
            }

        }
    }

    private static void menu() {
        System.out.println("A - add");
        System.out.println("S - show");
        System.out.println("O - order");
        System.out.println("Q - quit");
    }

    private static void addEmployee(Director director, Scanner scanner) {
        System.out.println("M - worker");
        System.out.println("W - manager");
        String employeeType = scanner.nextLine();
        if (employeeType.equalsIgnoreCase("M")) {
            director.addManager();
        } else if (employeeType.equalsIgnoreCase("W")) {
            director.addWorkerToManager();
        } else {
            System.out.println("Bad command");
        }
    }


}
