package com.upondev.AdvancedFuturesCoding.ex10Factory;

public enum WorkTool {
    HAMMER("Hammer"),
    LAPTOP("Laptop"),
    POINTING_FINGER("Pointing Finger");

    String workToolName;

    WorkTool(String workToolName) {
        this.workToolName = workToolName;
    }

    // public WorkTool getWorkTool(Employee employee) {
    //     if (employee instanceof Director) {
    //         return WorkTool.POINTING_FINGER;
    //     } else if (employee instanceof Manager) {
    //         return WorkTool.LAPTOP;
    //     } else if (employee instanceof Worker) {
    //         return WorkTool.HAMMER;
    //     }
    //     return null;
    // }

    @Override
    public String toString() {
        return workToolName;
    }
}
