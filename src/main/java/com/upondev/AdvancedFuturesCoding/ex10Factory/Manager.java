package com.upondev.AdvancedFuturesCoding.ex10Factory;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Manager extends Employee {

    private Map<String, Worker> workers = new HashMap<>();

    public Map<String, Worker> getWorkers() {
        return workers;
    }

    public void setWorkers(Map<String, Worker> workers) {
        this.workers = workers;
    }

    public Manager(String name, String surname, String[] workers) {
        super(name, surname);
        loadWorkers(workers);
    }

    public Manager(String name, String surname) {
        super(name, surname);
    }

    public void addWorker() {
        System.out.println("Enter  manager name and surname:");
        String input = new Scanner(System.in).nextLine();
        if (input.contains(" ")) {
            String name = input.split("")[0];
            String surname = input.split("")[1];
            String key = name + surname;
            if (workers.containsKey(key)) {
                System.out.println("Manager already exist");
            } else {
                workers.put(key, new Worker(name, surname));
                System.out.println("Manager added");
            }

        }
    }

    public void loadWorkers(String[] workers) {
        for (int i = 0; i < workers.length; i += 2) {
            String name = workers[i];
            String surname = workers[i + 1];
            this.workers.put(name + surname, new Worker(name, surname));
        }
    }

    @Override
    public void doWork() {
        System.out.println("Manager working.");
    }

    @Override
    public void finishJob() {
        setWorkStatus("Manager finished job.");
    }


}
