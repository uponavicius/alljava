package com.upondev.AdvancedFuturesCoding.ex10Factory;

public class Worker extends Employee {


    public Worker(String name, String surname) {
        super(name, surname);
    }

    @Override
    public void doWork() {
        System.out.println("Worker working.");
    }

    @Override
    public void finishJob() {
        setWorkStatus("Worker finished job.");
    }


}
