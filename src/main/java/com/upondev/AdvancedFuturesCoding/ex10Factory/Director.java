package com.upondev.AdvancedFuturesCoding.ex10Factory;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Director extends Employee {

    private Map<String, Manager> managers = new HashMap<>();


    public Director(String name, String surname) {
        super(name, surname);
        loadManagers();
    }

    public void addManager() {
        System.out.println("Enter  manager name and surname:");
        String input = new Scanner(System.in).nextLine();
        if (input.contains(" ")) {
            String name = input.split("")[0];
            String surname = input.split("")[1];
            String key = name + surname;
            if (managers.containsKey(key)) {
                System.out.println("Manager already exist");
            } else {
                managers.put(key, new Manager(name, surname));
                System.out.println("Manager added");
            }

        }

    }

    public void addWorkerToManager() {
        showManagersNames();
        System.out.println("Enter  manager name and surname:");
        String input = new Scanner(System.in).nextLine();
        if (input.contains(" ")) {
            String name = input.split("")[0];
            String surname = input.split("")[1];
            String key = name + surname;
            if (managers.containsKey(key)) {
                managers.get(key).addWorker();

            } else {
                System.out.printf("Manager %s %s not working here. \n", name, surname);
            }

        }
    }

    public void loadManagers() {

        String[][] workers = {
                {"Simantas", "Simaitis", "Jonas", "Jablonskis"},
                {"Linas", "Linaitis", "Germanas", "Germanauskas"}
        };
        managers.put("PetrasPeraitis", new Manager("Petras", "Petraitis", workers[0]));
        managers.put("JonatasJonaitis", new Manager("Jonas", "Jonaitis", workers[1]));

    }

    public void showWorkingStatus() {
        System.out.println("Director:");
        System.out.println(getName() + " " + getSurname() + " " + getWorkStatus());
        System.out.println("Manager");
        managers.values().stream()
                .map(manager -> manager.getName() + " " + manager.getSurname() + ": " + getWorkStatus())
                .forEach(System.out::println);
        System.out.println("Worker");
    /////////////////////////// pas Gerarda :D

    }

    public void showManagersNames() {
        managers.forEach((key, value) -> System.out.println(value.getName() + " " + value.getSurname()));
    }

    @Override
    public void doWork() {
        System.out.println("Director working... or not :D");
    }

    @Override
    public void finishJob() {
        setWorkStatus("Director finished job.....");
    }


}
