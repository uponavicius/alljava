package com.upondev.AdvancedFuturesCoding.WithGerardas;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class App {
    public static void main(String[] args) {
        Map<String, Map<String, Map<String, List<Employee>>>> maxima = new LinkedHashMap<>();

        Employee employee1 = new Employee("Vladas");
        Employee employee2 = new Employee("Petras");
        Employee employee3 = new Employee("Jurate");

        //maxima.put("Vilniaus regionas", (Map<String, Map<String, List<Employee>>>) new LinkedHashMap<>().put("Vilnius", new LinkedHashMap<>().put("Administracija", new LinkedList<>().add(employee1))));

        List<Employee> tempEmployeeList = new LinkedList<>(); //
        tempEmployeeList.add(employee1);
        Map<String, List<Employee>> tempDepartmentMap = new LinkedHashMap<>();
        tempDepartmentMap.put("Administracija", tempEmployeeList);
        Map<String, Map<String, List<Employee>>> tempCityMap = new LinkedHashMap<>();
        tempCityMap.put("Vilnius", tempDepartmentMap);
        maxima.put("Vilniaus regionas", tempCityMap);


        System.out.println(employee1);
        System.out.println(maxima);
        String employee1Name;
        employee1Name = maxima.get("Vilniaus regionas").get("Vilnius").get("Administracija").get(0).getName();
        System.out.println(employee1Name);

    }
}
