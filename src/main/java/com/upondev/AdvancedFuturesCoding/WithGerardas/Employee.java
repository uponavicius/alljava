package com.upondev.AdvancedFuturesCoding.WithGerardas;

public class Employee {
    private String name;
    private static int employeeCount = 1;
    private int id;

    public Employee(String name) {
        this.name = name;
        this.id = employeeCount++;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static int getEmployeeCount() {
        return employeeCount;
    }

    public static void setEmployeeCount(int employeeCount) {
        Employee.employeeCount = employeeCount;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Employee name: " + name + " id: " + this.id;
    }
}
