package com.upondev.AdvancedFuturesCoding.ex7PlanningVacations;


import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

/**
 * 7. We’re
 * a. User provides information about the country and cities that he is going to visit. You
 * can use a nested while loop to gather information and a HashMap to store it.
 * b. Display the created plan.
 * i. *if city occurs on the list twice it should be displayed as “back and forth” c. Store created plan using JSON.
 */


public class App {
    public static void main(java.lang.String[] args) {
        System.out.println("Vocations planing");
        System.out.println("==================================");
        Map<java.lang.String, List<String>> countryCity = new LinkedHashMap<>();
        java.lang.String country;
        List<String> cities;


        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.println("P - vocations planing");
            System.out.println("L - to see vocation plan");
            System.out.println("Q - quit from vocation planing");
            System.out.println("J - store in JSON");
            java.lang.String userInput = scanner.nextLine();
            if (userInput.equalsIgnoreCase("P")) {
                System.out.println("Enter Country");
                country = scanner.nextLine();


                if (!countryCity.containsKey(country)) { //jeigu nera salis jau yra map (kaip key),  tai tada kuria suscia sarasa (lapa)
                    cities = new LinkedList<>();
                } else {
                    cities = countryCity.get(country); //jei yra, tai i tapati lista (lapa) surasome
                }

                while (true) {
                    System.out.println("Enter city or Q to quit from city enter");
                    java.lang.String userEnterCity = scanner.nextLine();

                    if (userEnterCity.equalsIgnoreCase("Q")) {
                        break;
                    } else {

                        cities.add(userEnterCity);
                    }

                }
                countryCity.put(country, cities);

            } else if (userInput.equalsIgnoreCase("L")) {
                for (Map.Entry<java.lang.String, List<String>> entry : countryCity.entrySet()) {
                    java.lang.String key = entry.getKey();
                    List<String> value = entry.getValue();
                    System.out.println("Country: " + key + ". Cities: " + value);

                }


            } else if (userInput.equalsIgnoreCase("Q")) {
                break;
            } else if (userInput.equalsIgnoreCase("J")) {
                JSONObject obj=new JSONObject();
                for (Map.Entry<java.lang.String, List<String>> entry : countryCity.entrySet()) {
                    java.lang.String key = entry.getKey();
                    List<String> value = entry.getValue();
                    JSONArray jsonArray = new JSONArray();
                    jsonArray.addAll(value); //https://www.tutorialspoint.com/json_simple/json_simple_quick_guide.htm

                    obj.put(key, jsonArray);
                   // obj.put("City", value);

                    System.out.println(obj);



                }

                FileWriter file;
                try {
                    file = new FileWriter("/Users/pro/IdeaProjects/AllCourse/src/main/java/com/upondev/Exercises7PlanningVacations/CountryCity.json");
                    file.write(obj.toJSONString());
                    System.out.println("Successfully copied JSON Object ti file...");
                    System.out.println("\nJSON Object: " + obj);

                    file.flush();

                } catch (IOException e) {
                    e.printStackTrace();
                }

            } else {
                System.out.println("Bad command");
            }


        }
    }


}


