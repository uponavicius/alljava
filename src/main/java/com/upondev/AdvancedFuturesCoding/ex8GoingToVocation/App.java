package com.upondev.AdvancedFuturesCoding.ex8GoingToVocation;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/**
 * 8. We’re going on vacation
 * a. Load created in the previous exercise plan (JSON). Display it.
 * b. Create a simple interaction with a user. Every time, when user will press Enter (or write “next”) next city (or country) should be displayed.
 * c. Add a possibility to remove a city/country from the list (user was not able to visit it)
 */
public class App {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        Map<String, List<String>> countryCities = new LinkedHashMap<>();

        while (true) {
            String userEnter = menu(scanner);
            if (userEnter.equalsIgnoreCase("L")) {
                loadInfo(countryCities);
                System.out.println(countryCities);
            } else if (userEnter.equalsIgnoreCase("N")) {
                printNextLocation(scanner, countryCities);
            } else if (userEnter.equalsIgnoreCase("R")) {
                removeCountryOrCity(scanner, countryCities);
            } else if (userEnter.equalsIgnoreCase("Q")) {
                break;
            } else {
                System.out.println("Bad command");
            }
        }
    }

    private static void removeCountryOrCity(Scanner scanner, Map<String, List<String>> countryCities) {
        while (true) {
            String removeCountryOrCity = removeMenu(scanner);
            if (removeCountryOrCity.equalsIgnoreCase("CO")) {
                removeCountry(scanner, countryCities);
                break;
            } else if (removeCountryOrCity.equalsIgnoreCase("CI")) {
                removeCity(scanner, countryCities);
            } else if (removeCountryOrCity.equalsIgnoreCase("Q")) {
                break;
            } else {
                System.out.println("Bad command");
            }
        }
    }


    private static void printNextLocation(Scanner scanner, Map<String, List<String>> countryCities) {
        Iterator<String> iteratorCountries = countryCities.keySet().iterator(); // susikuriu iteratoriu saliai
        while (iteratorCountries.hasNext()) { //eina per salis
            String countyName = iteratorCountries.next(); //issitraukiu salies varda
            // System.out.println("Country:" + countyName);
            Iterator<String> iteratorCities = countryCities.get(countyName).iterator(); //istsiraukiu salies varda pagal sali
            while (iteratorCities.hasNext()) { //eina per miestus
                String cityName = iteratorCities.next();
                System.out.println("Country:" + countyName);
                System.out.println("Next city: " + cityName);

                while (true) {
                    System.out.println("Press enter to go or skip to skip");
                    String userEnterAction = scanner.nextLine();
                    if (userEnterAction.equalsIgnoreCase("")) {
                        System.out.println("You are visiting: " + cityName);
                        break;
                    } else if (userEnterAction.equalsIgnoreCase("skip")) {
                        if (iteratorCities.hasNext()) {
                            iteratorCities.next();
                        } else {
                            break;
                        }

                        System.out.println("City skipped");
                        break;
                    } else {
                        System.out.println("Bad command");
                    }
                }

            }
        }
        System.out.println("Have a nice day. Trip ended!");
    }

    private static String removeMenu(Scanner scanner) {
        System.out.println("CO - remove country");
        System.out.println("CI - remove city");
        System.out.println("Q - quit");
        return scanner.nextLine();
    }

    private static void removeCity(Scanner scanner, Map<String, List<String>> countryCities) {
        System.out.println(countryCities);
        System.out.println("Enter city");
        String removeCity = scanner.nextLine();
        //=================================================== o jeigu nera tokio miesto????

        for (Map.Entry<String, List<String>> entry : countryCities.entrySet()) {
            entry.getValue().remove(removeCity);
        }

        System.out.println(countryCities);
    }

    private static void removeCountry(Scanner scanner, Map<String, List<String>> countryCities) {
        System.out.println(countryCities);
        System.out.println("Enter country");
        String removeCountry = scanner.nextLine();
        countryCities.remove(removeCountry);
        System.out.println(countryCities);
        return;

        //countryCities.entrySet().removeIf(e -> e.getKey().equalsIgnoreCase(removeCountry));

        //=================================================== o jeigu nera tokios valstybes????
    }

    private static String menu(Scanner scanner) {
        System.out.println("L - load file");
        System.out.println("N - print next location");
        System.out.println("R - remove city or country");
        System.out.println("Q - quit");
        return scanner.nextLine();
    }

    private static void loadInfo(Map<String, List<String>> countryCities) {
        //JSON parser object to parse read file
        JSONParser jsonParser = new JSONParser();
        try (FileReader fileReader = new FileReader("/Users/pro/IdeaProjects/AllCourse/src/main/java/com/upondev/Exercises8GoingToVocation/CountryCity.json")) {
            Object obj = jsonParser.parse(fileReader);
            JSONObject objJSON = (JSONObject) obj;
            Set<String> keySet = objJSON.keySet();
            // Su iteratoriumi
            Iterator<String> iterator = keySet.iterator();
            while (iterator.hasNext()) {
                List<String> cities = new LinkedList<>();
                String key = iterator.next();
                cities.addAll((JSONArray) objJSON.get(key));
                countryCities.put(key, cities);
            }
            //For each ciklas
            // for (String key:keySet ) {
            //     List<String> cities = new LinkedList<>();
            //     cities.addAll((JSONArray) objJSON.get(key));
            //     countryCities.put(key, cities);
            // }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

}
