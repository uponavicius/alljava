package com.upondev.AdvancedFuturesCoding.ex5ListDraw;

import java.util.ArrayList;
import java.util.List;

public class Draw {


    public void horizontalLine() {
        List<String> asterisks = new ArrayList<>();
        asterisks.add("*");
        for (int i = 0; i < 10; i++) {
            System.out.print(asterisks.get(0));

        }
        System.out.println();

    }

    public void verticalLine() {
        List<String> asterisks = new ArrayList<>();
        asterisks.add("*");
        for (int i = 0; i < 10; i++) {
            System.out.println(asterisks.get(0));

        }
    }

    public void squareFullOfAsterisks() {
        List<String> asterisks = new ArrayList<>();
        asterisks.add("*");

        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                System.out.print(asterisks.get(0));
            }
            System.out.println();
        }
    }

    public void addRow() {
        List<String> asterisks = new ArrayList<>();
        asterisks.add("*");

        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < 5; j++) {
                System.out.print(asterisks.get(0));
            }
            System.out.println();
        }
    }

    public void addColumn() {
        List<String> asterisks = new ArrayList<>();
        asterisks.add("*");

        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 6; j++) {
                System.out.print(asterisks.get(0));
            }
            System.out.println();
        }
    }

    public void removeRow() {
        List<String> asterisks = new ArrayList<>();
        asterisks.add("*");

        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 5; j++) {
                System.out.print(asterisks.get(0));
            }
            System.out.println();
        }
    }

    public void removeColumn() {
        List<String> asterisks = new ArrayList<>();
        asterisks.add("*");

        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 4; j++) {
                System.out.print(asterisks.get(0));
            }
            System.out.println();
        }
    }
}
