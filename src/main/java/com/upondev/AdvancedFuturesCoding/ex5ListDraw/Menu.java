package com.upondev.AdvancedFuturesCoding.ex5ListDraw;

public class Menu {

    public void menuList() {
        System.out.println("H - draw a horizontal line");
        System.out.println("V - draw a vertical line");
        System.out.println("S - draw a square full of asterisks");
        System.out.println("AR - add row");
        System.out.println("RR - remove row");
        System.out.println("AC - add column");
        System.out.println("RC - remove column");
        System.out.println("Q - quit");
    }
}
