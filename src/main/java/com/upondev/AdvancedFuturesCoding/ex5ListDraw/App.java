package com.upondev.AdvancedFuturesCoding.ex5ListDraw;

import java.util.Scanner;

public class App {
    /**
     * Create a List that stores the character, e.g. "*".
     * a. Use the list to draw a horizontal line.
     * b. Draw a vertical line
     * c. Draw a square full of asterisks.
     * d. * Inside the loop, allow the user to select "add" / "delete" "row" / "column" - display
     * the effect after each selection.
     */
    public static void main(String[] args) {
        Draw draw = new Draw();

        Scanner scanner = new Scanner(System.in);
        while (true) {
            Menu menu = new Menu();
            menu.menuList();
            String userInput = scanner.nextLine();
            if (userInput.equalsIgnoreCase("H")) {
                draw.horizontalLine();
            } else if (userInput.equalsIgnoreCase("V")) {
                draw.verticalLine();
            } else if (userInput.equalsIgnoreCase("S")) {
                draw.squareFullOfAsterisks();
            } else if (userInput.equalsIgnoreCase("Q")) {
                break;
            } else if (userInput.equalsIgnoreCase("AR")) {
                draw.addRow();
            } else if (userInput.equalsIgnoreCase("RR")) {
                draw.removeRow();
            } else if (userInput.equalsIgnoreCase("AC")) {
                draw.addColumn();
            } else if (userInput.equalsIgnoreCase("RC")) {
                draw.addColumn();
            } else {
                System.out.println("Bad command");


            }

        }
    }


}