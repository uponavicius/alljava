package com.upondev.AdvancedFuturesCoding.ex3PersonalInformation;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * a. Create a file containing any personal data (name, surname, phone number). Data of individual persons should be in the following lines.
 * b. Download data from a file and create objects of people based on them (in any way - Regex, String.split ...).
 * c. Enter the created objects into ArrayList or Map (<line number>: <Person>).
 * d. Present the obtained data.
 */
public class App {
    public static void main(String[] args) {
        Path path = Paths.get("/Users/pro/IdeaProjects/allsdainfo/src/AdvancedFuturesCoding/Exercises3PersonalInformation/Info.txt");
        List<String> rawData = new ArrayList<>(); //pasitapliname neapdorotus duomenis i aray
        List<String> splittedValue = new ArrayList<>(); //susidesime reikmes tarp kabletaskiu
        List<Person> persons = new ArrayList<>(); //sudesime kiekviena asmeni i lista (i ta pati)

        try {
            rawData = Files.readAllLines(path);
            for (String unsplitedLine : rawData) {
                splittedValue.addAll(Arrays.asList(unsplitedLine.split(";")));
            }

            for (int i = 0; i < splittedValue.size(); i = i + 3) {
                //sukuriamas zmogus
               // Person tempPerson = new Person(splittedValue.get(i), splittedValue.get(i + 1), splittedValue.get(i + 2)); //alternatyva zemiau
                persons.add(new Person(splittedValue.get(i), splittedValue.get(i + 1), splittedValue.get(i + 2))); //sudedu kiekviena asmeni i lista (i ta pati)

            }
        } catch (Exception e) {
            System.out.println(e.toString());
        }

        for (Person person : persons ) {
            System.out.println(person);

        }
    }
}
