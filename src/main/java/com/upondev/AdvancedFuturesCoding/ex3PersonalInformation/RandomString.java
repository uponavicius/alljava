package com.upondev.AdvancedFuturesCoding.ex3PersonalInformation;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

// Java program generate a random AlphaNumeric String
// using Math.random() method
public class RandomString {
    public static void main(String[] args) throws IOException {
        // Our example data
        List<List<String>> rows = Arrays.asList(
                Arrays.asList("Jean;", "author", "Java"),
                Arrays.asList("David", "editor", "Python"),
                Arrays.asList("Scott", "editor", "Node.js")
        );

        FileWriter csvWriter = new FileWriter("new.csv");
        // csvWriter.append("Name");
        // csvWriter.append(";");
        // csvWriter.append("Role");
        // csvWriter.append(";");
        // csvWriter.append("Topic");
        // csvWriter.append(";");
        // csvWriter.append("\n");

        for (List<String> rowData : rows) {
            csvWriter.append(String.join(",", rowData));
            csvWriter.append("\n");
        }

        csvWriter.flush();
        csvWriter.close();

    }
}
