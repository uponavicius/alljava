package com.upondev.AdvancedFuturesCoding.ex6EmployeeManager;


import java.util.Objects;

public class Manager {
    private String managerName;

    public Manager(String managerName) {
        this.managerName = managerName;
    }

    public String getManagerName() {
        return managerName;
    }

    public void setManagerName(String managerName) {
        this.managerName = managerName;
    }

    @Override
    public String toString() {
        return "Manager: " + managerName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Manager manager = (Manager) o;
        return Objects.equals(managerName, manager.managerName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(managerName);
    }
}
