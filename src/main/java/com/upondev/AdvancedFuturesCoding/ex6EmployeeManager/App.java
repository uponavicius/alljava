package com.upondev.AdvancedFuturesCoding.ex6EmployeeManager;

import java.util.*;

public class App {
    public static void main(String[] args) {
        Map<Manager, List<Employee>> managerEmployee = new LinkedHashMap<>();
        Manager manager = new Manager("Vladas Uponavicius");

        List<Employee> employeesVlado = new LinkedList<>(); //darbuotoju sarasas
        employeesVlado.add(new Employee("Jonas Jonaitis"));
        employeesVlado.add(new Employee("Petras Pertraitis"));
        employeesVlado.add(new Employee("Mikas Mikaitis"));
        employeesVlado.add(new Employee("Simas Trumpulis"));
        employeesVlado.add(new Employee("Simona Kernyte"));
        employeesVlado.add(new Employee("Laura Izaura"));
        employeesVlado.add(new Employee("Lina Katalina"));
        employeesVlado.add(new Employee("Julija Manaida"));
        //employeesVlado.add(new Employee("Simas"));

        Manager manager2 = new Manager("Gerardas Balnius");

        List<Employee> employeesGerardo = new LinkedList<>(); //darbuotoju sarasas
        employeesGerardo.add(new Employee("Jonas Jonaitis"));
        employeesGerardo.add(new Employee("Petras Pertraitis"));
        employeesGerardo.add(new Employee("Mikas Mikaitis"));




        managerEmployee.put(manager, employeesVlado); //menegeriui pirskiriame darbuotojus
        managerEmployee.put(manager2, employeesGerardo);

        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("-----------------------");
            System.out.println("N - employ new employee");
            System.out.println("D - dismissed employee");
            System.out.println("L - show list of employee");
            System.out.println("Q - quit from program");
            System.out.println("-----------------------");
            String userInput = scanner.nextLine();

            if (userInput.equalsIgnoreCase("N")) {
                System.out.println("Enter new employ Name and Surname");
                String newEmploy = scanner.nextLine();
                employeesVlado.add(new Employee(newEmploy));

            } else if (userInput.equalsIgnoreCase("D")) {
                System.out.println("Enter employ Name and Surname to dismiss");
                String dismissEmploy = scanner.nextLine();
                // for(Iterator<Employee> iter = employeesVlado.iterator(); iter.hasNext();) {
                //     Employee data = iter.next();
                //     if (data.getEmployeeName().equalsIgnoreCase(dismissEmploy)) {
                //         iter.remove();
                //     }
                // }

                employeesVlado.removeIf(data -> data.getEmployeeName().equalsIgnoreCase(dismissEmploy));

            } else if (userInput.equalsIgnoreCase("L")) {
                for (Map.Entry<Manager, List<Employee>> entry : managerEmployee.entrySet()) {
                    Manager key = entry.getKey();
                    List<Employee> value = entry.getValue();
                    System.out.println(key + ". Employees: " + value);
                }

                // tik vlado darbuotojai
                String printManager = "Vladas Uponavicius";
                System.out.println(managerEmployee.get(new Manager(printManager)));
                System.out.println("-------------");

                for (Manager managerOne :managerEmployee.keySet() ) { //key gaunasi managerOne
                    //managerOne.getManagerName(); //managerio vardas

                    if (managerOne.getManagerName().equalsIgnoreCase(printManager)){
                        System.out.println(managerEmployee.get(managerOne));
                    }

                }



            } else if (userInput.equalsIgnoreCase("Q")) {
                System.out.println("Have a nice day!");
                break;
            } else {
                System.out.println("---Bad command---");
            }

        }
    }
}
