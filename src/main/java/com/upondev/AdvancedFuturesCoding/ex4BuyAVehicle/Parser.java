package com.upondev.AdvancedFuturesCoding.ex4BuyAVehicle;

public class Parser {
    private String buyerInfoFromScanner;

    public Person splitBuyerInfo(String buyerInfoFromScanner) {
        Person person = new Person();
        person.setName(buyerInfoFromScanner.split(" ")[0]);
        person.setSurname(buyerInfoFromScanner.split(" ")[1]);
        person.setBornDate(buyerInfoFromScanner.split(" ")[2]);
        return person;

    }
}


