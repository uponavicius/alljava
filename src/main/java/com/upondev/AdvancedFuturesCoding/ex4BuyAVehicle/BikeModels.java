package com.upondev.AdvancedFuturesCoding.ex4BuyAVehicle;

public enum BikeModels {
    MOUNTAIN_BIKE("Mountain bike", 2000),
    ROAD_BIKE("Road bike", 1800),
    BMX_BIKE("BMX bike", 1500);

    private String name;
    private int price;

    BikeModels(String name, int price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }


    @Override
    public String toString() {
        return "Bike: " + name + ". Price: " + price;
    }
}
