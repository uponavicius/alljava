package com.upondev.AdvancedFuturesCoding.ex4BuyAVehicle;

import java.util.Date;

public class Bike implements Vehicle {

    @Override
    public void buy() {
        BikeModels bikeModels = BikeModels.MOUNTAIN_BIKE;
        System.out.println(bikeModels);
        Date date1 = new Date();
        System.out.println("Bike bought: " + date1.toString());

    }
}
