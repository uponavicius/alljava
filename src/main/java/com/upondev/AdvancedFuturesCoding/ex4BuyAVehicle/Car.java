package com.upondev.AdvancedFuturesCoding.ex4BuyAVehicle;

import java.util.Date;

public class Car implements Vehicle {

    @Override
    public void buy() {
        CarModels carModels = CarModels.VOLVO_XC90;
        System.out.println(carModels);
        Date date1 = new Date();
        System.out.println("Car bought: " + date1.toString());

    }
}
