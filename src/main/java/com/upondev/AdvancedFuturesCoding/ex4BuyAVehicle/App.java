package com.upondev.AdvancedFuturesCoding.ex4BuyAVehicle;

import java.util.Scanner;

public class App {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println();
        Person person = null;


        while (true) {
            System.out.println("N - new buyer");
            System.out.println("L - print sold cars and bikes list");
            System.out.println("Q - quit from vehicle shop");
            String userInput = scanner.nextLine();

            if (userInput.equalsIgnoreCase("N")) {
                System.out.println("Enter buyer name, surname and bord date");
                String userEnterNewBuyer = scanner.nextLine();
                Parser newBuyerInfo = new Parser();
                person = newBuyerInfo.splitBuyerInfo(userEnterNewBuyer);
            } else if (userInput.equalsIgnoreCase("L")) {
                System.out.println("------------------------------------------------------------");
                person.buy(new Car());
                System.out.println("------------------------------------------------------------");
                person.buy(new Bike());
                System.out.println("------------------------------------------------------------");

            } else if (userInput.equalsIgnoreCase("Q")) {
                break;
            } else {
                System.out.println("Bad command");
            }


        }


    }
}
