package com.upondev.AdvancedFuturesCoding.ex4BuyAVehicle;

public interface Vehicle {

    public void buy();
}
