package com.upondev.AdvancedFuturesCoding.ex4BuyAVehicle;

public enum CarModels {
    VOLVO_XC70("Volvo XC70", 70000),
    VOLVO_XC90("Volvo XC90", 90000),
    VOLVO_S90("Volvo S90", 60000),
    VOLVO_V90("Volvo V90", 60000),
    VOLVO_S80("Volvo S80", 50000),
    VOLVO_V70("Volvo V70", 50000),
    VOLVO_S60("Volvo S60", 45000),
    VOLVO_V60("Volvo V60", 45000),
    VOLVO_XC60("Volvo XC60", 55000);

    private String name;
    private int price;

    CarModels(String name, int price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "Car: " + name + ". Price: " + price;
    }
}
