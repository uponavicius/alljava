package com.upondev.AdvancedFuturesCoding.ex4BuyAVehicle;

public class Person {
    private String name = "unknown name";
    private String surname = "unknown surname";
    private String bornDate = "unknown bord date";

    public Person() {
    }

    public Person(String name, String surname, String bornDate) {
        this.name = name;
        this.surname = surname;
        this.bornDate = bornDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getBornDate() {
        return bornDate;
    }

    public void setBornDate(String bornDate) {
        this.bornDate = bornDate;
    }

    public void buy(Vehicle purchases) { //interface Purchases
        System.out.println("Buyer: " + name + " " + surname + ". Born date: " + bornDate);
        purchases.buy();

    }

    @Override
    public String toString() {
        return "Name: " + name + ". Surname: " + surname + ". Born date: " + bornDate;
    }
}
