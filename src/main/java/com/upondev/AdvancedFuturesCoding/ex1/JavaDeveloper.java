package com.upondev.AdvancedFuturesCoding.ex1;

public class JavaDeveloper extends Developer {

    //Create constructor for every class that will call constructor of the super class. Each constructor should display an information, that it has been called.
    public JavaDeveloper() {
        super();
        System.out.println("JavaDeveloper constructor");
    }

    //*Overload method from the Person class in JavaDeveloper class to accept additional parameters.
    public String adInfo(String name, String surname, int age) {
        System.out.println(name + " " + surname + " " + age);
        return name + " " + name + " " + age;
    }

}
