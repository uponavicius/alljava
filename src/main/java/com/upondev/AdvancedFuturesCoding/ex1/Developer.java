package com.upondev.AdvancedFuturesCoding.ex1;

public class Developer extends Person {

    //Create constructor for every class that will call constructor of the super class. Each constructor should display an information, that it has been called.
    public Developer() {
        super();
        System.out.println("Developer constructor");
    }

    //Using an object of type JavaDeveloper call a method that is defined in Developer class. What access modifier should it have?
    public void printText() {
        System.out.println("Printed text for fun :)");
    }

}
