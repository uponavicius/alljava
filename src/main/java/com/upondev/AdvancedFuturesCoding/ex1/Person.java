package com.upondev.AdvancedFuturesCoding.ex1;

public class Person {

    //Create constructor for every class that will call constructor of the super class. Each constructor should display an information, that it has been called.
    public Person() {
        System.out.println("Person constructor");
    }

    //*Overload method from the Person class in JavaDeveloper class to accept additional parameters.
    public String adInfo(String name, String surname) {
        System.out.println(name + " " + surname);
        return name + " " + name;

    }

}
