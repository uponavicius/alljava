package com.upondev.FundCoding.tests.unittesting;

import org.junit.Assert;
import org.junit.Test;

public class SimplestLogicTest {
    // public static void main(String[] args) {
    //     // arrange / given (pre-conditions)
    //     String input = "Hello world";
    //     // act / when
    //     String output = SimplestLogic.addExclamationToString(input);
    //     //assert / then
    //     // if (output.endsWith("!"))
    //     //     System.out.println("PASSED");
    //     if (output.equals(input + "!!!"))
    //         System.out.println("PASSED");
    //     else
    //         System.out.println("FAILED");
    //     //teardown / cleanup
    //     // none for this simple case
    // }
    @Test
    public void test_addExclamationToString_given_hw_returns_hw_with_exclamation () {

        //given
        String input = "Hello world";
        //when
        String output = SimplestLogic.addExclamationToString(input);
        //then
        Assert.assertEquals("Hello world!!!", output);
        //teardown

    }
}
