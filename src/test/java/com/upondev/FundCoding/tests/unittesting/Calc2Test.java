package com.upondev.FundCoding.tests.unittesting;


import com.upondev.FundCoding.tests.Calculator.Calc2;
import org.junit.Assert;
import org.junit.Test;

public class Calc2Test {
    @Test
    public void test_divide_givenTwoPositiveDoubles_returnsCorrectResult() {
        // given
        Calc2 calc2 = new Calc2();
        // when
        double result = calc2.divide(10.0, 2.0);
        // then
        Assert.assertEquals(5.0, result, 0.0001);

    }


    // public void test__sudetiesVeiksmas__givenTwoPositiveInts_returnsCorrectSum(){
    //     // given
    //     Calc calculator = new Calc(5, 3);
    //     // when
    //     double result = calculator.sudetiesVeiksmas();
    //     // then
    //     Assert.assertEquals(8.0, result, 0.0001);
    // }

}
