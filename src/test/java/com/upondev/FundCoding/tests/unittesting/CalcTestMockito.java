package com.upondev.FundCoding.tests.unittesting;

    import org.junit.Test;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

    public class CalcTestMockito {

        @Test
        public void add_mockTest(){
            // given
            Calc mockCalculator = mock(Calc.class);

            // when
            when(mockCalculator.sudetiesVeiksmas()).thenReturn(123.1);

            // then
            System.out.println(mockCalculator.sudetiesVeiksmas());
            System.out.println(mockCalculator.sudetiesVeiksmas());

            // assert
            verify(mockCalculator, times(2)).sudetiesVeiksmas();
        }

        //Bandymas, nežinau ar geras 2019-12-02
        // @Test
        // public void subtraction_mockTest(){
        //     //given
        //     Calc mockCalculator = mock(Calc.class);
        //
        //     //when
        //     when(mockCalculator.dalybosVeiksmas()).thenReturn(2.0);
        //
        //     //then
        //     System.out.println(mockCalculator.dalybosVeiksmas());
        //
        //     //assert
        //     verify(mockCalculator, times(1)).dalybosVeiksmas();
        //
        // }
    }

