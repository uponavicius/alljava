package com.upondev.FundCoding.tests.unittesting;

import org.junit.*;
import org.junit.rules.ExpectedException;

import java.math.BigDecimal;

public class CalcTest {
    @BeforeClass
    public static void beforeClass () {
        System.out.println("beforeClass()");
    }
    @Before
    public void before () {
        System.out.println("before ()");
    }
    @Test
    public void test__sudetiesVeiksmas__givenTwoPositiveInts_returnsCorrectSum(){
        // given
        Calc calculator = new Calc(5, 3);
        // when
        double result = calculator.sudetiesVeiksmas();
        // then
        Assert.assertEquals(8.0, result, 0.0001);
    }

    @Test
    public void test__sudetiesVeiksmas__givenTwoPositiveDoubles_returnsCorrectSum(){
        // given
        Calc calculator = new Calc(5.000001, 3.5);
        // when
        double result = calculator.sudetiesVeiksmas();
        // then
        Assert.assertEquals(8.500001, result, 0.0001);
    }

    @Test
    public void test__sudetiesVeiksmas__givenOnePositiveOneNegative_returnsCorrectSum(){
        // given
        Calc calculator = new Calc(-5.000001, 3.5);
        // when
        double result = calculator.sudetiesVeiksmas();
        // then
        Assert.assertEquals(-1.500001, result, 0.0001);
    }

    @Test
    public void test__sudetiesVeiksmas__givenTwoIntMax_returnsCorrectSum(){
        // given
        Calc calculator = new Calc(Integer.MAX_VALUE, Integer.MAX_VALUE);
        // when
        double result = calculator.sudetiesVeiksmas();
        // then
        Assert.assertEquals(new BigDecimal("4294967294"), new BigDecimal(result));
    }

    @Test
    public void test__sudetiesVeiksmas__givenTwoDoubleMax_returnsInfinity(){
        // given
        Calc calculator = new Calc(Double.MAX_VALUE, Double.MAX_VALUE);
        // when
        double result = calculator.sudetiesVeiksmas();
        // then
        Assert.assertEquals(Double.POSITIVE_INFINITY, result, 0);
    }
    @Test
    public void test_dalybosVeiksmas_givenTwoPositiveInts_returnsCorrectSum () {
        // given
        Calc calculator = new Calc(15, 5);
        // when
        double result = calculator.dalybosVeiksmas();
        // then
        Assert.assertEquals(3, result, 0);
    }
    @Test
    public void test_dalybosVeiksmas_givenPositiveAndNegativeInts_returnsCorrectSum () {
        // given
        Calc calculator = new Calc(15, -5);
        // when
        double result = calculator.dalybosVeiksmas();
        // then
        Assert.assertEquals(-3, result, 0);
    }
    @Test
    public void test_dalybosVeiksmas_givenTwoIntMax_returnsCorrectSum () {
        // given
        Calc calculator = new Calc(Integer.MAX_VALUE, Integer.MAX_VALUE);
        // when
        double result = calculator.dalybosVeiksmas();
        // then
        Assert.assertEquals(1, result, 0);
    }
    // @Test
    // public void test_dalybosVeiksmas_givenDivisionFromZero_returnsInfinity () {
    //     // given
    //     Calc calculator = new Calc(5000, 0);
    //     // when
    //     double result = calculator.dalybosVeiksmas();
    //     // then
    //     Assert.assertEquals(Double.POSITIVE_INFINITY, result, 0);
    // }
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void test_dalybosVeiksmas_givenDivisionFromZero_throwsAritmeticException () {
        // given
        expectedException.expect(ArithmeticException.class);
        expectedException.expectMessage("/ by zero");

        Calc calculator = new Calc(5000, 0);
        // when
        double result = calculator.dalybosVeiksmas();
        // then ... expect exception
        //Assert.assertEquals(2.5, result, 0);
    }
    @After
    public void after () {
        System.out.println("after ()");
    }
    @AfterClass
    public static void afterClass() {
        System.out.println("afterClass ()");
    }
}
