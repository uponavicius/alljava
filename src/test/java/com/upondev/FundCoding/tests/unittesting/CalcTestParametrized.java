package com.upondev.FundCoding.tests.unittesting;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.runners.Parameterized.*;

@RunWith(Parameterized.class)
public class CalcTestParametrized {

    @Parameters
    public static Collection<Integer[]> parameters() {
        return Arrays.asList(new Integer[][] {
                {1, 1, 2}, // 1 st testcase
                {2, 4, 6}, // 2 nd testcase
                {1, 6, 7},
                {4, 1, 5}
                //   0, 1, 2
        });
    }

    @Parameter(0)
    public int argument1;

    @Parameter(1)
    public int argument2;

    @Parameter(2)
    public int expectedResult;

    @Test
    public void testAdd() {
        // given
        Calc calculator = new Calc(argument1, argument2);
        // when
        double result = calculator.sudetiesVeiksmas();
        // then
        Assert.assertEquals(expectedResult, result, 0.01);
    }
}
